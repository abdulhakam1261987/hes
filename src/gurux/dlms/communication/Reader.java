package gurux.dlms.communication;

import gurux.common.GXCommon;
import gurux.dlms.GXDateTime;
import java.util.List;
import gurux.dlms.client.Global.Obis;
import gurux.dlms.client.Global.util.Kalendar;
import gurux.dlms.client.Global.util.PrintUtil;
import gurux.dlms.client.Global.util.ObisDescriptor;
import static gurux.dlms.communication.Communicator.logger;
import static gurux.dlms.communication.Communicator.read;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.object.meter.DCUTIDEMeterListObj;
import gurux.dlms.objects.GXDLMSActionSchedule;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSExtendedRegister;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.GXDLMSRegisterActivation;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.GXDLMSSecuritySetup;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;

/**
 * Class untuk interface pembacaan meter.
 *
 * @author Tab Solutions Team
 */
public class Reader extends Read {

    /**
     * Membaca nomor meter dengan Standard OBIS (0.0.96.1.0.255).
     */
    public void readNomorMeter() {
        readData("0.0.96.1.0.255");
    }

    /**
     * Membaca Waktu meter dengan Standard OBIS (0.0.1.0.0.255) index ke 2.
     */
    public void readMeterTime() {
        logger.log(Level.INFO, "Read meter time (0.0.1.0.0.255 index 2)");
        try {
            GXDLMSClock data = new GXDLMSClock("0.0.1.0.0.255");
            data.setVersion(0);
            read(data, 2);
            System.out.println("Waktu Meter : " + data.getTime());
            System.out.println("Waktu Meter Local : " + data.getTime().getLocalCalendar().getTime());
            System.out.println("Waktu Meter Meter : " + data.getTime().getMeterCalendar().getTime());
            GXDateTime dateTime = new GXDateTime(Calendar.getInstance());
            System.out.println("Waktu saat ini : " + dateTime);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("Waktu Meter : N/A");
        }
        System.out.println("===========================================================================");
    }

    /**
     * Membaca setting profile generic. Versi yang digunakan adalah 1.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca settingnya.
     */
    public void readProfileGenericSetting(String logicalName) {
        readProfileGenericSetting(logicalName, 1);
    }

    /**
     * Membaca setting profile generic.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca settingnya.
     * @param version Versi object GXDLMSProfileGeneric yang akan dibaca.
     */
    public void readProfileGenericSetting(String logicalName, int version) {
        GXDLMSProfileGeneric generic = new GXDLMSProfileGeneric(logicalName);
        generic.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        generic.setVersion(version);
        readProfileGenericSetting(generic);
    }

    /**
     * Membaca load profile dengan standard OBIS (1.0.99.1.0.255 version 1).
     * Kecuali EDMI Mk10M (0.0.99.1.0.255 version 1)
     */
    public void readLoadProfile() {
        readProfileGeneric(Obis.getLoadProfileObject());
    }

    /**
     * Membaca data load profile hari ini.
     */
    public void readTodaySLoadProfile() {
        readProfileGenericByDateRange(
                Obis.getLoadProfileObject(),
                Kalendar.getStartDay(),
                Kalendar.getTheEndOfToday());
    }

    /**
     * Membaca load profile dengan selective access yaitu range descriptor.
     *
     * @param start Tanggal mulai baca.
     * @param end Tanggal akhir baca.
     */
    public void readLoadProfileByRange(Calendar start, Calendar end) {
        readProfileGenericByDateRange(
                Obis.getLoadProfileObject(), start, end);
    }

    /**
     * Membaca profile generic Object. Versi yang digunakan adalah 1.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     */
    public void readProfileGeneric(String logicalName) {
        readProfileGeneric(logicalName, 1);
    }

    /**
     * Membaca profile generic Object.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     * @param version Versi object GXDLMSProfileGeneric yang akan dibaca.
     */
    public void readProfileGeneric(String logicalName, int version) {
        GXDLMSProfileGeneric generic = new GXDLMSProfileGeneric(logicalName);
        generic.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        generic.setVersion(version);
        readProfileGeneric(generic);
    }

    /**
     * Membaca profile generic Object dengan selective access yaitu range
     * descriptor. Versi yang digunakan adalah 1.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     * @param start Start date as java.util.Calendar class (Tanggal awal baca).
     * @param end End date as java.util.Calendar class (Tanggal akhir baca).
     */
    public void readProfileGenericByDateRange(String logicalName, Calendar start, Calendar end) {
        readProfileGenericByDateRange(logicalName, 1, start, end);
    }

    /**
     * Membaca profile generic Object dengan selective access yaitu range
     * descriptor.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     * @param version Versi object GXDLMSProfileGeneric yang akan dibaca.
     * @param start Start date as java.util.Calendar class (Tanggal awal baca).
     * @param end End date as java.util.Calendar class (Tanggal akhir baca).
     */
    public void readProfileGenericByDateRange(String logicalName, int version, Calendar start, Calendar end) {
        GXDLMSProfileGeneric generic = new GXDLMSProfileGeneric(logicalName);
        generic.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        generic.setVersion(version);
        readProfileGenericByDateRange(generic, start, end);
    }

    /**
     * Membaca profile generic Object dengan selective access yaitu entry
     * descriptor. Versi yang digunakan adalah 1.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     * @param startIndex Index awal dari row yang akan dibaca.
     * @param jumlahRow Jumlah row yang akan dibaca.
     */
    public void readProfileGenericByEntry(String logicalName, int startIndex, int jumlahRow) {
        readProfileGenericByEntry(logicalName, 1, startIndex, jumlahRow);
    }

    /**
     * Membaca profile generic Object dengan selective access yaitu entry
     * descriptor.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     * @param version Versi object GXDLMSProfileGeneric yang akan dibaca.
     * @param startIndex Index awal dari row yang akan dibaca.
     * @param jumlahRow Jumlah row yang akan dibaca.
     */
    public void readProfileGenericByEntry(String logicalName, int version, int startIndex, int jumlahRow) {
        GXDLMSProfileGeneric generic = new GXDLMSProfileGeneric(logicalName);
        generic.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        generic.setVersion(version);
        readProfileGenericByEntry(generic, startIndex, jumlahRow);
    }

    /**
     * Membaca capture object. Versi yang digunakan dari
     * GXDLMSProfileGeneric-nya adalah 1.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca capture objectnya.
     */
    public void readCaptureObject(String logicalName) {
        readCaptureObject(logicalName, 1);
    }

    /**
     * Membaca capture object.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca capture objectnya.
     * @param version Versi object GXDLMSProfileGeneric yang akan dibaca capture
     * objectnya.
     */
    public void readCaptureObject(String logicalName, int version) {
        GXDLMSProfileGeneric generic = new GXDLMSProfileGeneric(logicalName);
        generic.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        generic.setVersion(version);
        readCaptureObject(generic);
    }

    /**
     * Membaca object Data.
     *
     * @param logicalName Logical name data yang ingin dibaca.
     */
    public void readData(String logicalName) {
        GXDLMSData data = new GXDLMSData(logicalName);
        data.setVersion(0);
        data.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        readData(data);
    }

    /**
     * Membaca object register.
     *
     * @param logicalName Logical name register yang ingin dibaca.
     */
    public void readRegister(String logicalName) {
        GXDLMSRegister register = new GXDLMSRegister(logicalName);
        register.setVersion(0);
        register.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        readRegister(register);
    }

    /**
     * Membaca object extended register.
     *
     * @param logicalName Logical name extended register yang ingin dibaca.
     */
    public void readExtendedRegister(String logicalName) {
        GXDLMSExtendedRegister register = new GXDLMSExtendedRegister(logicalName);
        register.setVersion(0);
        register.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        readExtendedRegister(register);
    }

    /**
     * Membaca object demand register.
     *
     * @param logicalName Logical name extended register yang ingin dibaca.
     */
    public void readDemandRegister(String logicalName) {
        GXDLMSDemandRegister demandRegister = new GXDLMSDemandRegister(logicalName);
        demandRegister.setVersion(0);
        demandRegister.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        readDemandRegister(demandRegister);
    }

    /**
     * Membaca object Disconnect Control dengan standard OBIS (0.0.96.3.10.255)
     * version 0.
     */
    public void readDisconnectControl() {
        readDisconnectControl("0.0.96.3.10.255");
    }

    /**
     * Membaca object Disconnect Control.
     *
     * @param logicalName Logical name object Disconnect Control yang ingin
     * dibaca.
     */
    public void readDisconnectControl(String logicalName) {
        GXDLMSDisconnectControl control = new GXDLMSDisconnectControl(logicalName);
        control.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        control.setVersion(0);
        readDisconnectControl(control);
    }

    /**
     * Membaca object active calendar dengan standard OBIS (0.0.13.0.0.255)
     * version 0.
     */
    public void readActivityCalendar() {
        readActivityCalendar("0.0.13.0.0.255");
    }

    /**
     * Membaca object active calendar.
     *
     * @param logicalName Logical name object GXDLMSActivityCalendar yang akan
     * dibaca.
     */
    public void readActivityCalendar(String logicalName) {
        GXDLMSActivityCalendar gxdlmsac = new GXDLMSActivityCalendar();
        gxdlmsac.setLogicalName(logicalName);
        gxdlmsac.setVersion(0);
        gxdlmsac.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        readActivityCalendar(gxdlmsac);
    }

    /**
     * Membaca object limiter dengan standard OBIS (0.0.17.0.0.255) version 0.
     * Perhatian : Jika belum membaca Association View, kemungkinan besar index
     * ke 2 dan 11 tidak dapat diketahui objectnya
     */
    public void readLimiter() {
        readLimiter("0.0.17.0.0.255");
    }

    /**
     * Membaca object limiter version 0. Perhatian : Jika belum membaca
     * Association View, kemungkinan besar index ke 2 dan 11 tidak dapat
     * diketahui objectnya
     *
     * @param logicalName Logical name dari object yang akan dibaca
     */
    public void readLimiter(String logicalName) {
        GXDLMSLimiter limiter = new GXDLMSLimiter(logicalName);
        limiter.setVersion(0);
        limiter.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        readLimiter(limiter);
    }

    /**
     * Melakukan pengecekan keaktifan emergency profile object limiter dengan
     * standard OBIS (0.0.17.0.0.255) version 0.
     *
     * @return true jika emergency profilenya sedang aktif.
     */
    public boolean isEmergencyLimiterActive() {
        return isEmergencyLimiterActive("0.0.17.0.0.255");
    }

    /**
     * Melakukan pengecekan keaktifan emergency profile object limiter.
     *
     * @param logicalName Logical name object GXDLMSLimiter yang akan dicek.
     * @return true jika emergency profilenya sedang aktif.
     */
    public boolean isEmergencyLimiterActive(String logicalName) {
        GXDLMSLimiter limiter = new GXDLMSLimiter(logicalName);
        limiter.setVersion(0);
        limiter.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        return isEmergencyLimiterActive(limiter);
    }

    /**
     * Membaca object script table.
     *
     * @param logicalName Logical name dari object yang akan dibaca
     */
    public void readScriptTable(String logicalName) {
        GXDLMSScriptTable scriptTable = new GXDLMSScriptTable(logicalName);
        scriptTable.setVersion(0);
        scriptTable.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        readScriptTable(scriptTable);
    }

    /**
     * Membaca object single action schedule.
     *
     * @param logicalName Logical name dari object yang akan dibaca
     */
    public void readSingleActionSchedule(String logicalName) {
        GXDLMSActionSchedule actionSchedule = new GXDLMSActionSchedule(logicalName);
        actionSchedule.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        actionSchedule.setVersion(0);
        readSingleActionSchedule(actionSchedule);
    }

    /**
     * Membaca object single action schedule.
     *
     * @param objectType Object type dari object yang akan dibaca;
     */
    public void readWaktuRekamBilling(ObjectType objectType) {
        if (objectType.equals(ObjectType.ACTION_SCHEDULE)) {
            GXDLMSActionSchedule actionSchedule = new GXDLMSActionSchedule("0.0.15.0.0.255");
            actionSchedule.setDescription(ObisDescriptor.getDeskripsi("0.0.15.0.0.255"));
            actionSchedule.setVersion(0);
            readWaktuRekamBilling(actionSchedule);
        } else if (objectType.equals(ObjectType.DATA)) {
            GXDLMSData data = new GXDLMSData("1.0.98.1.128.255");
            data.setDescription(ObisDescriptor.getDeskripsi("1.0.98.1.128.255"));
            data.setVersion(0);
            readWaktuRekamBilling(data);
        }
    }

    /**
     * Membaca object clock dengan default OBIS(0.0.1.0.0.255).
     */
    public void readClock() {
        readClock("0.0.1.0.0.255");
    }

    /**
     * Membaca object clock.
     *
     * @param logicalName Logical name dari object yang akan dibaca.
     */
    public void readClock(String logicalName) {
        GXDLMSClock clock = new GXDLMSClock(logicalName);
        clock.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        clock.setVersion(0);
        readClock(clock);
    }

    /**
     * Membaca object register activation dengan default OBIS(0.0.14.0.0.255).
     */
    public void readRegisterActivation() {
        readRegisterActivation("0.0.14.0.0.255");
    }

    /**
     * Membaca object register activation.
     *
     * @param logicalName Logical name dari object yang akan dibaca.
     */
    public void readRegisterActivation(String logicalName) {
        GXDLMSRegisterActivation registerActivation = new GXDLMSRegisterActivation(logicalName);
        registerActivation.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        registerActivation.setVersion(0);
        readRegisterActivation(registerActivation);
    }

    /**
     * Membaca object security setup dengan default OBIS(0.0.43.0.0.255) version
     * 1.
     */
    public void readSecuritySetup() {
        readSecuritySetup("0.0.43.0.0.255", 1);
    }

    /**
     * Membaca object security setup dengan default OBIS(0.0.43.0.0.255) version
     * 0.
     */
    public void readSecuritySetup0() {
        readSecuritySetup("0.0.43.0.0.255", 0);
    }

    /**
     * Membaca object security setup.
     *
     * @param logicalName Logical name dari object yang akan dibaca.
     * @param version Versi object GXDLMSSecuritySetup yang akan dibaca.
     */
    public void readSecuritySetup(String logicalName, int version) {
        GXDLMSSecuritySetup securitySetup = new GXDLMSSecuritySetup(logicalName);
        securitySetup.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        securitySetup.setVersion(version);
        readSecuritySetup(securitySetup);
    }

    /**
     * Membaca data padam pada meter.
     */
    public void readDataPadam() {
        readMultipleObject(Obis.getMeterOffObject(), "Info padam");
    }

    /**
     * Membaca TOU dengan standard OBIS (0.0.13.0.0.255) version 0.
     */
    public void readTOU() {
        readTOU("0.0.13.0.0.255");
    }

    /**
     * Membaca TOU.
     *
     * @param logicalName Logical name object GXDLMSActivityCalendar.
     */
    public void readTOU(String logicalName) {
        GXDLMSActivityCalendar gxdlmsac = new GXDLMSActivityCalendar();
        gxdlmsac.setLogicalName(logicalName);
        gxdlmsac.setVersion(0);
        gxdlmsac.setDescription("TOU");
        readTOU(gxdlmsac);
    }

    /**
     * Membaca Multiple Object tanpa mendefinisikan kesatuan.
     *
     * @param objects List object yang ingin dibaca.
     */
    public void readMultipleObject(List<GXDLMSObject> objects) {
        readMultipleObject(objects, null);
    }

    /**
     * Membaca Multiple Object.
     *
     * @param objects List object yang ingin dibaca.
     * @param multipleObjectDefinition Definisi kesatuan dari multiple object.
     */
    public void readMultipleObject(List<GXDLMSObject> objects, String multipleObjectDefinition) {
        System.out.println("======================================================================================================================================================");
        if (multipleObjectDefinition.equalsIgnoreCase("NONE") || multipleObjectDefinition == null) {
            logger.log(Level.INFO, "Read Multiple Object");
        } else {
            logger.log(Level.INFO, "Read " + multipleObjectDefinition + " Object");
        }
        for (GXDLMSObject object : objects) {
            switch (object.getObjectType()) {
                case DATA:
                    readData(object.getLogicalName());
                    break;
                case REGISTER:
                    readRegister(object.getLogicalName());
                    break;
                case EXTENDED_REGISTER:
                    readExtendedRegister(object.getLogicalName());
                    break;
                case DEMAND_REGISTER:
                    readDemandRegister(object.getLogicalName());
                    break;
                case PROFILE_GENERIC:
                    readProfileGeneric(object.getLogicalName(), object.getVersion());
                    break;
                case LIMITER:
                    readLimiter(object.getLogicalName());
                    break;
                case ACTIVITY_CALENDAR:
                    readActivityCalendar(object.getLogicalName());
                    break;
                case DISCONNECT_CONTROL:
                    readDisconnectControl(object.getLogicalName());
                    break;
                case SCRIPT_TABLE:
                    readScriptTable(object.getLogicalName());
                    break;
                case ACTION_SCHEDULE:
                    readSingleActionSchedule(object.getLogicalName());
                    break;
                case CLOCK:
                    readClock(object.getLogicalName());
                    break;
                default:
                    logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
                        object.getObjectType(), object.getLogicalName(), object.getDescription()});
                    logger.log(Level.WARNING, "Unhandled Object");
                    System.out.println("===========================================================================");
                    break;
            }
        }
        System.out.println(Level.INFO + ": End Read Multiple Object");
        System.out.println("======================================================================================================================================================");
    }

    /**
     * Membaca profile generic Object. Versi yang digunakan adalah 1.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     */
    public void readEventProfileGeneric(String logicalName) {
        readEventProfileGeneric(logicalName, 1);
    }

    /**
     * Membaca profile generic Object.
     *
     * @param logicalName Logical name object GXDLMSProfileGeneric yang akan
     * dibaca.
     * @param version Versi object GXDLMSProfileGeneric yang akan dibaca.
     */
    public void readEventProfileGeneric(String logicalName, int version) {
        GXDLMSProfileGeneric generic = new GXDLMSProfileGeneric(logicalName);
        generic.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        generic.setVersion(version);
        readEventProfileGeneric(generic);
    }

    /**
     * Membaca TIDE meter list.
     */
    public void readTideMeterList() {
        GXDLMSData data = new GXDLMSData("0.1.96.1.130.255");
        data.setVersion(0);
        data.setDescription("TIDE Meter List");
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            data.getObjectType(), data.getLogicalName(), data.getDescription()});
        try {
            read(data, 2);
            String regex = "\n";
            String[] strings = ((String) data.getValue()).split(regex);
            List<DCUTIDEMeterListObj> list = new ArrayList<>();
            for (String string : strings) {
                String[] ses = string.split(",");
                DCUTIDEMeterListObj obj = new DCUTIDEMeterListObj(ses[0], ses[1], ses[2],
                        Integer.parseInt(ses[3]), Integer.parseInt(ses[4]));
                list.add(obj);
            }
            for (DCUTIDEMeterListObj dCUTIDEMeterListObj : list) {
                System.out.println(dCUTIDEMeterListObj.toString());
            }
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }

    }
}
