/*
 * 
 */
package gurux.dlms.communication;

/**
 * Enum Definition
 *
 * @author Tab Solutions Team
 */
public enum SecurityKeyType {
    BLOCK_CIPHER_KEY(0),
    DEDICATED_KEY(1),
    AUTHENTICATION_KEY(2),
    MASTER_KEY(3);

    private final int intValue;

    private static java.util.HashMap<Integer, SecurityKeyType> mappings;

    public static java.util.HashMap<Integer, SecurityKeyType> getMappings() {
        synchronized (SecurityKeyType.class) {
            if (mappings
                    == null) {
                mappings = new java.util.HashMap<>();
            }
        }
        return mappings;
    }

    SecurityKeyType(final int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    /**
     * Get integer value from enumeration.
     *
     * @return Country specific identifiers in integer value.
     */
    public int getValue() {
        return intValue;
    }

    /**
     * Convert integer for enum value.
     *
     * @param value Country specific identifiers in integer value.
     * @return CountrySpesificIdentifier object by integer value.
     */
    public static SecurityKeyType forValue(final int value) {
        SecurityKeyType type = getMappings().get(value);
        if (type == null) {
            throw new IllegalArgumentException(
                    "Invalid key type: " + String.valueOf(value));
        }
        return type;
    }
}
