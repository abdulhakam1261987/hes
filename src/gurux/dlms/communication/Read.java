/*
 * 
 */
package gurux.dlms.communication;

import gurux.dlms.GXDLMSException;
import gurux.dlms.GXReplyData;
import gurux.dlms.client.Global.PhysicalUnit;
import gurux.dlms.client.Global.SkaladanUnit;
import gurux.dlms.client.Global.util.ObisDescriptor;
import gurux.dlms.client.Global.util.PrintUtil;
import static gurux.dlms.communication.Communicator.dlms;
import static gurux.dlms.communication.Communicator.isAssociationViewReaded;
import static gurux.dlms.communication.Communicator.logger;
import static gurux.dlms.communication.Communicator.read;
import static gurux.dlms.communication.Communicator.readDataBlock;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.ErrorCode;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.enums.Unit;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.object.meter.MeterType;
import gurux.dlms.objects.GXDLMSActionSchedule;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSExtendedRegister;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.GXDLMSRegisterActivation;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.GXDLMSSecuritySetup;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

/**
 * Class untuk mengatur pembacaan meter.
 *
 * @author Tab Solutions Team
 */
class Read extends Communicator {

    /**
     * Membaca data Profile Generic berdasarkan range.
     *
     * @deprecated Method ini untuk Load profile EDMI MK7MI gen 1 saja.
     * @param pg Object profile generic yang ingin dibaca.
     * @param start Tanggal awal baca.
     * @param end Tanggal akhir baca.
     * @return Berupa array object yang berisi data sesuai dengan capture
     * objectnya.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    @Deprecated
    Object[] readEDMIRowsByRange(final GXDLMSProfileGeneric pg,
            final Date start, final Date end) throws Exception {
        GXReplyData reply = new GXReplyData();
        byte[][] data = dlms.readEDMIRowsByRange(pg, start, end);
        readDataBlock(data, reply);
        return (Object[]) dlms.updateValue(pg, 2, reply.getValue());
    }

    /**
     * Membaca data Profile Generic berdasarkan range.
     *
     * @deprecated Method ini untuk Load profile EDMI MK7MI gen 2 saja.
     * @param pg Object profile generic yang ingin dibaca.
     * @param start Tanggal awal baca.
     * @param end Tanggal akhir baca.
     * @return Berupa array object yang berisi data sesuai dengan capture
     * objectnya.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    @Deprecated
    Object[] readEDMIGen2RowsByRange(final GXDLMSProfileGeneric pg,
            final Date start, final Date end) throws Exception {
        GXReplyData reply = new GXReplyData();
        byte[][] data = dlms.readEDMIGen2RowsByRange(pg, start, end);
        readDataBlock(data, reply);
        return (Object[]) dlms.updateValue(pg, 2, reply.getValue());
    }

    /**
     * Membaca capture object profile generic;
     *
     * @param generic GXDLMSProfileGeneric object
     */
    void readCaptureObject(GXDLMSProfileGeneric generic) {
        logger.log(Level.INFO, "Read capture object ({0}({1}) index 3)", new Object[]{generic.getLogicalName(), generic.getDescription()});
        try {
            read(generic, 3);
            generic.getCaptureObjects().stream().map((col) -> col.getKey().getLogicalName()).
                    forEachOrdered((logicalName) -> {
                        System.out.println(logicalName + " (" + ObisDescriptor.getDeskripsi(logicalName) + ")");
                    });
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Err! Failed to read columns:{0}", ex.toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Menambahkan capture object profile generic ke dalam Object Collection
     * GXDLMSClient sekaligus scaller dan unitnya. Perhatian : Jangan digunakan
     * bila sudah melakukan pembacaan Association View kecuali meter EDMI MK7MI.
     *
     * @param generic Object profile generic yang sedang dibaca.
     */
    void addToCollection(GXDLMSProfileGeneric generic) {
        logger.log(Level.INFO, "add unit measurement and record unit and scale to collection");
        List<Double> doubles = new ArrayList<>();
        if (Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName())) {
            try {
                doubles = readEDMIMk7MILPScaleList();
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Gagal mengambil skala EDMI Mk7MI karena : {0}", ex.toString());
            }
        }
        try {
            read(generic, 3);
            int count = 0;
            for (Map.Entry<GXDLMSObject, GXDLMSCaptureObject> col : generic
                    .getCaptureObjects()) {
                switch (col.getKey().getObjectType()) {
                    case DEMAND_REGISTER: {
                        GXDLMSDemandRegister demandRegister = new GXDLMSDemandRegister(
                                col.getKey().getLogicalName());
                        demandRegister.setVersion(col.getKey().getVersion());
                        demandRegister.setDescription(ObisDescriptor.getDeskripsi(
                                demandRegister.getLogicalName()));
                        Object val = read(demandRegister, 4);
                        SkaladanUnit su = new SkaladanUnit(PrintUtil.lihatNilai(4, val));
                        demandRegister.setScaler(readLPScale(
                                Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName()), count, doubles,
                                su.getSkala()));
                        demandRegister.setUnit(su.getUnit());
                        dlms.getObjects().add(demandRegister);
                        System.out.println("add " + demandRegister.getLogicalName()
                                + "(" + demandRegister.getDescription() + ")"
                                + " version " + demandRegister.getVersion()
                                + " scaller " + demandRegister.getScaler()
                                + " unit " + demandRegister.getUnit()
                                + " as " + demandRegister.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case REGISTER: {
                        GXDLMSRegister register = new GXDLMSRegister(col.getKey().getLogicalName());
                        register.setVersion(col.getKey().getVersion());
                        register.setDescription(ObisDescriptor.getDeskripsi(register.getLogicalName()));
                        Object val = read(register, 3);
                        SkaladanUnit su = new SkaladanUnit(PrintUtil.lihatNilai(3, val));
                        register.setScaler(readLPScale(Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName()),
                                count, doubles, su.getSkala()));
                        register.setUnit(su.getUnit());
                        dlms.getObjects().add(register);
                        System.out.println("add " + register.getLogicalName()
                                + "(" + register.getDescription() + ")"
                                + " version " + register.getVersion()
                                + " scaller " + register.getScaler()
                                + " unit " + register.getUnit()
                                + " as " + register.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case EXTENDED_REGISTER: {
                        GXDLMSExtendedRegister extendedRegister
                                = new GXDLMSExtendedRegister(col.getKey().getLogicalName());
                        extendedRegister.setVersion(col.getKey().getVersion());
                        extendedRegister.setDescription(ObisDescriptor.getDeskripsi(
                                extendedRegister.getLogicalName()));
                        Object val = read(extendedRegister, 3);
                        SkaladanUnit su = new SkaladanUnit(PrintUtil.lihatNilai(3, val));
                        extendedRegister.setScaler(readLPScale(
                                Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName()), count, doubles,
                                su.getSkala()));
                        extendedRegister.setUnit(su.getUnit());
                        dlms.getObjects().add(extendedRegister);
                        System.out.println("add " + extendedRegister.getLogicalName()
                                + "(" + extendedRegister.getDescription() + ")"
                                + " version " + extendedRegister.getVersion()
                                + " scaller " + extendedRegister.getScaler()
                                + " unit " + extendedRegister.getUnit()
                                + " as " + extendedRegister.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case CLOCK:
                        GXDLMSClock clock = new GXDLMSClock(col.getKey().getLogicalName());
                        clock.setDescription(ObisDescriptor.getDeskripsi(clock.getLogicalName()));
                        clock.setVersion(col.getKey().getVersion());
                        for (int i = 2; i < 10; i++) {
                            read(clock, i);
                        }
                        dlms.getObjects().add(clock);
                        System.out.println("add " + clock.getLogicalName()
                                + "(" + clock.getDescription() + ")"
                                + " version " + clock.getVersion()
                                + " as " + clock.getObjectType()
                                + " to collection ");
                        break;
                    case DATA: {
                        GXDLMSData dt = new GXDLMSData(col.getKey().getLogicalName());
                        dt.setDescription(ObisDescriptor.getDeskripsi(dt.getLogicalName()));
                        dlms.getObjects().add(dt);
                        System.out.println("add " + dt.getLogicalName()
                                + "(" + dt.getDescription() + ")"
                                + " version " + dt.getVersion()
                                + " as " + dt.getObjectType()
                                + " to collection ");
                        break;
                    }
                    default: {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName(col.getKey().getLogicalName());
                        dt.setObjectType(col.getKey().getObjectType());
                        dt.setDescription(ObisDescriptor.getDeskripsi(dt.getLogicalName()));
                        dlms.getObjects().add(dt);
                        System.out.println("add " + dt.getLogicalName()
                                + "(" + dt.getDescription() + ")"
                                + " version " + dt.getVersion()
                                + " as " + dt.getObjectType()
                                + " to collection ");
                        break;
                    }
                }
                count++;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error adding to collection {0}", e.toString());
        }
    }

    /**
     * Membaca setting profile generic.
     *
     * @param object Object GXDLMSProfileGeneric yang akan dibaca.
     */
    void readProfileGenericSetting(GXDLMSProfileGeneric object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                if (pos != 2) {
                    read(object, pos);
                }
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printProfileGenericSetting(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca profile generic Object.
     *
     * @param object Object GXDLMSProfileGeneric yang akan dibaca.
     */
    void readProfileGeneric(GXDLMSProfileGeneric object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        if (!isAssociationViewReaded()) {
            addToCollection(object);
        }
        logger.log(Level.INFO, "Read capture object ({0} index 3)", object.getLogicalName());
        try {
            read(object, 3);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Err! Failed to read columns:{0}", ex.toString());
            System.out.println("===========================================================================");
            return;
        }
        try {
            GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            pg.setLogicalName(object.getLogicalName());
            pg.setObjectType(ObjectType.PROFILE_GENERIC);
            if (Meter.isLoadProfileEDMIMk7MI(object.getLogicalName())) {
                GXDLMSObject dt = new GXDLMSObject();
                dt.setLogicalName("0.0.1.0.0.255");
                dt.setDescription("Capture Time");
                dt.setObjectType(ObjectType.CLOCK);
                dt.setUIDataType(2, DataType.DATETIME);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
                dt = new GXDLMSObject();
                dt.setLogicalName("1.0.0.8.4.255");
                dt.setDescription("Data");
                dt.setObjectType(ObjectType.DATA);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
            }
            object.getCaptureObjects().forEach((itco) -> {
                pg.addCaptureObject(itco.getKey(), itco.getValue().getAttributeIndex(),
                        itco.getValue().getDataIndex());
            });
            logger.log(Level.INFO, "Jumlah Channel {0} : {1}", new Object[]{object.getDescription(),
                pg.getCaptureObjects().size()});
            long entriesInUse = ((Number) read(pg, 7)).longValue();
            long entries = ((Number) read(pg, 8)).longValue();
            logger.log(Level.INFO, "Entries: {0}/{1}", new Object[]{String.valueOf(entriesInUse),
                String.valueOf(entries)});
            if (entriesInUse == 0 || pg.getCaptureObjects().isEmpty()) {
                if (entriesInUse == 0) {
                    logger.log(Level.WARNING, "Entries in use is 0");
                }
                if (pg.getCaptureObjects().isEmpty()) {
                    logger.log(Level.WARNING, "Capture objects is empty");
                }
                System.out.println("===========================================================================");
                return;
            }
            read(pg, 2);
            Object[] cells = pg.getBuffer();
            for (Object rows : cells) {
                System.out.println("---------------------------------------------------------------------------");
                int a = 0;
                for (Object cell : (Object[]) rows) {
                    System.out.print(pg.getCaptureObjects().get(a).getKey().getDescription() + "("
                            + pg.getCaptureObjects().get(a).getKey().getLogicalName() + ") = ");
                    String hasil;
                    if (cell instanceof byte[]) {
                        hasil = new String((byte[]) cell);
                    } else {
                        hasil = cell.toString();
                    }
                    if (pg.getCaptureObjects().get(a).getKey().getObjectType().equals(ObjectType.REGISTER)) {
                        if (PhysicalUnit.getPhysicalUnit(
                                ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                        pg.getCaptureObjects().get(a).getKey().
                                                getLogicalName())).getUnit()).getSatuan().equals(
                                        PhysicalUnit.getPhysicalUnit(Unit.SECOND).getSatuan())) {
                            System.out.println(PhysicalUnit.secondToDuration(Integer.parseInt(hasil)));

                        } else {
                            System.out.println(hasil + " " + PhysicalUnit.getPhysicalUnit(
                                    ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                            pg.getCaptureObjects().get(a).getKey().
                                                    getLogicalName())).getUnit()).getSatuan());
                        }
                    } else {
                        System.out.println(hasil);
                    }
                    a++;
                }
            }
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Membaca profile generic Object dengan selective access yaitu entry
     * descriptor.
     *
     * @param object Object GXDLMSProfileGeneric yang akan dibaca.
     * @param startIndex Index awal dari row yang akan dibaca.
     * @param jumlahRow Jumlah row yang akan dibaca.
     */
    void readProfileGenericByEntry(GXDLMSProfileGeneric object,
            int startIndex, int jumlahRow) {
        logger.log(Level.INFO, "Read Profile Generic Object ({0}|{1}) by the entry from index {2} for {3} rows",
                new Object[]{object.getLogicalName(), object.getDescription(),
                    startIndex, jumlahRow});
        if (!isAssociationViewReaded()) {
            addToCollection(object);
        }
        logger.log(Level.INFO, "Read capture object ({0} index 3)", object.getLogicalName());
        try {
            read(object, 3);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Err! Failed to read columns:{0}", ex.toString());
            System.out.println("===========================================================================");
            return;
        }
        try {
            GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            pg.setLogicalName(object.getLogicalName());
            pg.setDescription(object.getDescription());
            pg.setObjectType(ObjectType.PROFILE_GENERIC);
            if (Meter.isLoadProfileEDMIMk7MI(object.getLogicalName())) {
                GXDLMSObject dt = new GXDLMSObject();
                dt.setLogicalName("0.0.1.0.0.255");
                dt.setDescription("Capture Time");
                dt.setObjectType(ObjectType.CLOCK);
                dt.setUIDataType(2, DataType.DATETIME);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
                dt = new GXDLMSObject();
                dt.setLogicalName("1.0.0.8.4.255");
                dt.setDescription("Data");
                dt.setObjectType(ObjectType.DATA);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
            }
            object.getCaptureObjects().forEach((itco) -> {
                pg.addCaptureObject(itco.getKey(), itco.getValue().getAttributeIndex(),
                        itco.getValue().getDataIndex());
            });
            logger.log(Level.INFO, "Jumlah Channel {0} : {1}", new Object[]{object.getDescription(),
                pg.getCaptureObjects().size()});
            long entriesInUse = ((Number) read(pg, 7)).longValue();
            long entries = ((Number) read(pg, 8)).longValue();
            logger.log(Level.INFO, "Entries: {0}/{1}", new Object[]{String.valueOf(entriesInUse),
                String.valueOf(entries)});
            if (entriesInUse == 0 || pg.getCaptureObjects().isEmpty()) {
                if (entriesInUse == 0) {
                    logger.log(Level.WARNING, "Entries in use is 0");
                }
                if (pg.getCaptureObjects().isEmpty()) {
                    logger.log(Level.WARNING, "Capture objects is empty");
                }
                System.out.println("===========================================================================");
                return;
            }
            readRowsByEntry(pg, startIndex, jumlahRow);
            Object[] cells = pg.getBuffer();
            for (Object rows : cells) {
                System.out.println("---------------------------------------------------------------------------");
                int a = 0;
                for (Object cell : (Object[]) rows) {
                    System.out.print(pg.getCaptureObjects().get(a).getKey().getDescription() + "("
                            + pg.getCaptureObjects().get(a).getKey().getLogicalName() + ") = ");
                    String hasil;
                    if (cell instanceof byte[]) {
                        hasil = new String((byte[]) cell);
                    } else {
                        hasil = cell.toString();
                    }
                    if (pg.getCaptureObjects().get(a).getKey().getObjectType().equals(ObjectType.REGISTER)) {
                        if (PhysicalUnit.getPhysicalUnit(
                                ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                        pg.getCaptureObjects().get(a).getKey().
                                                getLogicalName())).getUnit()).getSatuan().equals(
                                        PhysicalUnit.getPhysicalUnit(Unit.SECOND).getSatuan())) {
                            System.out.println(PhysicalUnit.secondToDuration(Integer.parseInt(hasil)));

                        } else {
                            System.out.println(hasil + " " + PhysicalUnit.getPhysicalUnit(
                                    ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                            pg.getCaptureObjects().get(a).getKey().
                                                    getLogicalName())).getUnit()).getSatuan());
                        }
                    } else {
                        System.out.println(hasil);
                    }
                    a++;
                }
            }
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Membaca profile generic Object dengan selective access yaitu range
     * descriptor.
     *
     * @param object Object GXDLMSProfileGeneric yang akan dibaca.
     * @param start Start date as java.util.Calendar class (Tanggal awal baca).
     * @param end End date as java.util.Calendar class (Tanggal akhir baca).
     */
    void readProfileGenericByDateRange(GXDLMSProfileGeneric object,
            Calendar start, Calendar end) {
        logger.log(Level.INFO, "Read Profile Generic Object ({0}|{1}) by range from {2} until {3}",
                new Object[]{object.getLogicalName(), object.getDescription(),
                    start.getTime().toString(), end.getTime().toString()});
        if (!isAssociationViewReaded()) {
            addToCollection(object);
        }
        logger.log(Level.INFO, "Read capture object ({0} index 3)", object.getLogicalName());
        try {
            read(object, 3);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Err! Failed to read columns:{0}", ex.toString());
            System.out.println("===========================================================================");
            return;
        }
        try {
            GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            pg.setLogicalName(object.getLogicalName());
            pg.setDescription(object.getDescription());
            pg.setObjectType(ObjectType.PROFILE_GENERIC);
            if (Meter.isLoadProfileEDMIMk7MI(object.getLogicalName())) {
                GXDLMSObject dt = new GXDLMSObject();
                dt.setLogicalName("0.0.1.0.0.255");
                dt.setDescription("Capture Time");
                dt.setObjectType(ObjectType.CLOCK);
                dt.setUIDataType(2, DataType.DATETIME);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
                dt = new GXDLMSObject();
                dt.setLogicalName("1.0.0.8.4.255");
                dt.setDescription("Data");
                dt.setObjectType(ObjectType.DATA);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
            }
            object.getCaptureObjects().forEach((itco) -> {
                pg.addCaptureObject(itco.getKey(), itco.getValue().getAttributeIndex(),
                        itco.getValue().getDataIndex());
            });
            logger.log(Level.INFO, "Jumlah Channel {0} : {1}", new Object[]{object.getDescription(),
                pg.getCaptureObjects().size()});
            long entriesInUse = ((Number) read(pg, 7)).longValue();
            long entries = ((Number) read(pg, 8)).longValue();
            logger.log(Level.INFO, "Entries: {0}/{1}", new Object[]{String.valueOf(entriesInUse),
                String.valueOf(entries)});
            if (entriesInUse == 0 || pg.getCaptureObjects().isEmpty()) {
                if (entriesInUse == 0) {
                    logger.log(Level.WARNING, "Entries in use is 0");
                }
                if (pg.getCaptureObjects().isEmpty()) {
                    logger.log(Level.WARNING, "Capture objects is empty");
                }
                System.out.println("===========================================================================");
                return;
            }
            switch (Meter.meterType) {
                case EDMI_Mk7MI_G2:
                    readEDMIGen2RowsByRange(pg, start.getTime(), end.getTime());
                    break;
                case EDMI_Mk7MI:
                    readEDMIRowsByRange(pg, start.getTime(), end.getTime());
                    break;
                default:
                    readRowsByRange(pg, start.getTime(), end.getTime());
                    break;
            }
            Object[] cells = pg.getBuffer();
            for (Object rows : cells) {
                System.out.println("---------------------------------------------------------------------------");
                int a = 0;
                for (Object cell : (Object[]) rows) {
                    System.out.print(pg.getCaptureObjects().get(a).getKey().getDescription() + "("
                            + pg.getCaptureObjects().get(a).getKey().getLogicalName() + ") = ");
                    String hasil;
                    if (cell instanceof byte[]) {
                        hasil = new String((byte[]) cell);
                    } else {
                        hasil = cell.toString();
                    }
                    if (pg.getCaptureObjects().get(a).getKey().getObjectType().equals(ObjectType.REGISTER)) {
                        if (PhysicalUnit.getPhysicalUnit(
                                ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                        pg.getCaptureObjects().get(a).getKey().
                                                getLogicalName())).getUnit()).getSatuan().equals(
                                        PhysicalUnit.getPhysicalUnit(Unit.SECOND).getSatuan())) {
                            System.out.println(PhysicalUnit.secondToDuration(Integer.parseInt(hasil)));

                        } else {
                            System.out.println(hasil + " " + PhysicalUnit.getPhysicalUnit(
                                    ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                            pg.getCaptureObjects().get(a).getKey().
                                                    getLogicalName())).getUnit()).getSatuan());
                        }
                    } else {
                        System.out.println(hasil);
                    }
                    a++;
                }
            }
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Mendapatkan daftar skala untuk masing-masing capture object EDMI Mk7MI.
     *
     * @return List skala dalam variabel bertipe Double.
     */
    List<Double> readEDMIMk7MILPScaleList() throws Exception {
        logger.log(Level.WARNING, "Read EDMI Mk7MI Load Profile Scaller (0.0.136.0.1.255 index 2)");
        List<Double> doubles = new ArrayList<>();
        GXDLMSData data = new GXDLMSData("0.0.136.0.1.255");
        data.setDescription("EDMI Scale");
        data.setVersion(0);
        data.setObjectType(ObjectType.DATA);
        read(data, 2);
        for (Object tmp : (List<?>) data.getValue()) {
            if (tmp instanceof List) {
                String tmp2;
                if (tmp instanceof byte[]) {
                    tmp2 = new String((byte[]) tmp);
                } else {
                    tmp2 = String.valueOf(tmp);
                }
                String[] strings = tmp2.replace("[", "").replace("]", "").split(",");
                doubles.add(Math.pow(10, Integer.parseInt(strings[1].trim())));
            }
        }
        return doubles;
    }

    /**
     * Memberikan skala untuk capture object. skalaObject jika bukan EDMI Mk7MI.
     *
     * @param isLoadProfileEDMIMk7MI True jika membaca Load Profile EDMI Mk7MI
     * karena Load Profile EDMI Mk7MI capture Object dan Buffernya tidak
     * Kongruen.
     * @param count Urutan capture object.
     * @param doubles List skala EDMI Mk7MI. bisa didapat dengan menggunakan
     * method getEDMIMk7MILPScaleList().
     * @param skalaObject Skala object aslinya.
     *
     * @return Skala dalam variabel bertipe Double
     */
    double readLPScale(boolean isLoadProfileEDMIMk7MI, int count, List<Double> doubles, double skalaObject) {
        if (isLoadProfileEDMIMk7MI && (count - 2 >= 0)) {
            return doubles.get(count);
        } else {
            return skalaObject;
        }
    }

    /**
     * Membaca object Data.
     *
     * @param object GXDLMSData yang ingin dibaca.
     */
    void readData(GXDLMSData object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        try {
            read(object, 2);
            PrintUtil.printdata(object);
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Membaca object register.
     *
     * @param object GXDLMSRegister yang ingin dibaca.
     */
    void readRegister(GXDLMSRegister object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printRegister(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca object Extended Register.
     *
     * @param object GXDLMSExtendedRegister yang ingin dibaca.
     */
    void readExtendedRegister(GXDLMSExtendedRegister object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printExtendedRegister(object);
        System.out.println("===========================================================================");

    }

    /**
     * Membaca object demand Register.
     *
     * @param object GXDLMSDemandRegister yang ingin dibaca.
     */
    void readDemandRegister(GXDLMSDemandRegister object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printDemandRegister(object);
        System.out.println("===========================================================================");

    }

    /**
     * Membaca object Disconnect Control.
     *
     * @param object Object Disconnect Control yang ingin dibaca.
     */
    void readDisconnectControl(GXDLMSDisconnectControl object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{object.getObjectType(),
            object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printDisconnectControl(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca object active calendar.
     *
     * @param object Object GXDLMSActivityCalendar yang akan dibaca.
     */
    void readActivityCalendar(GXDLMSActivityCalendar object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{object.getObjectType(),
            object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printActivityCalendar(object);
        System.out.println("===========================================================================");
    }

    /**
     * Menambahkan monitored value ke dalam Object Collection GXDLMSClient.
     * Penambahan ini bersifat statis. Perhatian : Jangan digunakan bila sudah
     * melakukan pembacaan Association View.
     */
    void addMonitoredValueToCollection() {
        logger.log(Level.INFO, "addMonitoredValueToCollection");
        GXDLMSRegister register = new GXDLMSRegister("1.0.21.7.0.255");
        register.setDescription(ObisDescriptor.getDeskripsi(register.getLogicalName()));
        dlms.getObjects().add(register);
        register = new GXDLMSRegister("1.0.32.7.0.255");
        register.setDescription(ObisDescriptor.getDeskripsi(register.getLogicalName()));
        dlms.getObjects().add(register);
        register = new GXDLMSRegister("1.0.31.7.0.255");
        register.setDescription(ObisDescriptor.getDeskripsi(register.getLogicalName()));
        dlms.getObjects().add(register);
        register = new GXDLMSRegister("1.0.15.7.0.255");
        register.setDescription(ObisDescriptor.getDeskripsi(register.getLogicalName()));
        dlms.getObjects().add(register);
    }

    /**
     * Membaca object limiter. Perhatian : Jika belum membaca Association View,
     * kemungkinan besar index ke 2 dan 11 tidak dapat diketahui objectnya.
     *
     * @param object Object GXDLMSLimiter yang akan dibaca.
     */
    void readLimiter(GXDLMSLimiter object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        if (!isAssociationViewReaded()) {
            addMonitoredValueToCollection();
        }
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                if (!(Meter.meterType.equals(MeterType.EDMI_Mk7MI_G2) && pos == 9)) {
                    read(object, pos);
                }
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        SkaladanUnit su = new SkaladanUnit();
        if (object.getMonitoredValue().getObjectType().equals(ObjectType.REGISTER)
                || object.getMonitoredValue().getObjectType().equals(ObjectType.EXTENDED_REGISTER)) {
            try {
                GXDLMSRegister register = new GXDLMSRegister(object.getMonitoredValue().getLogicalName());
                Object val = read(register, 3);
                su = new SkaladanUnit(PrintUtil.lihatNilai(3, val));
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Gagal membaca skala monitored value");
            }
        } else if (object.getMonitoredValue().getObjectType().equals(ObjectType.DEMAND_REGISTER)) {
            try {
                GXDLMSDemandRegister register
                        = new GXDLMSDemandRegister(object.getMonitoredValue().getLogicalName());
                Object val = read(register, 4);
                su = new SkaladanUnit(PrintUtil.lihatNilai(3, val));
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Gagal membaca skala monitored value");
            }
        }
        PrintUtil.printLimiter(object, su);
        System.out.println("===========================================================================");
    }

    /**
     * Melakukan pengecekan keaktifan emergency profile object limiter.
     *
     * @param object Object GXDLMSLimiter yang akan dicek.
     */
    boolean isEmergencyLimiterActive(GXDLMSLimiter object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        try {
            Object val = read(object, 10);
            boolean b = Boolean.getBoolean(PrintUtil.lihatNilai(10, val));
            if (b) {
                logger.log(Level.INFO, "Emergency profile is aktif, returning true");
            } else {
                logger.log(Level.INFO, "Emergency profile is not aktif, returning false");
            }
            System.out.println("===========================================================================");
            return b;
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
            return false;
        }
    }

    /**
     * Membaca object script table.
     *
     * @param object Object GXDLMSScriptTable yang akan dibaca.
     */
    void readScriptTable(GXDLMSScriptTable object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printScriptTable(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca object single action schedule.
     *
     * @param object Object GXDLMSActionSchedule yang akan dibaca.
     */
    void readSingleActionSchedule(GXDLMSActionSchedule object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printSingleActionSchedule(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca object single action schedule.
     *
     * @param object Object GXDLMSActionSchedule yang akan dibaca.
     */
    void readWaktuRekamBilling(GXDLMSActionSchedule object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printWaktuRekamBilling(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca waktu rekam billing SPLN.
     *
     * @param object Object GXDLMSData yang akan dibaca.
     */
    void readWaktuRekamBilling(GXDLMSData object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printWaktuRekamBilling(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca object clock.
     *
     * @param object Object GXDLMSClock yang akan dibaca.
     */
    void readClock(GXDLMSClock object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printClock(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca object register activation.
     *
     * @param object Object GXDLMSRegisterActivation yang akan dibaca.
     */
    void readRegisterActivation(GXDLMSRegisterActivation object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printRegisterActivation(object);
        System.out.println("===========================================================================");
    }

    void readSecuritySetup(GXDLMSSecuritySetup object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        for (int pos : object.getAttributeIndexToRead(true)) {
            try {
                read(object, pos);
            } catch (GXDLMSException ex) {
                switch (ErrorCode.forValue(ex.getErrorCode())) {
                    case UNDEFINED_OBJECT:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                        System.out.println("===========================================================================");
                        return;
                    default:
                        logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                                new Object[]{pos, ex.toString()});
                        PrintUtil.printGXDLMSException(ex);
                        break;
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                        new Object[]{pos, ex.toString()});
            }
        }
        PrintUtil.printSecuritySetup(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca TOU Meter.
     *
     * @param object Object GXDLMSActivityCalendar yang akan dibaca.
     */
    void readTOU(GXDLMSActivityCalendar object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{object.getObjectType(),
            object.getLogicalName(), object.getDescription()});
        try {
            read(object, 5);
        } catch (GXDLMSException ex) {
            switch (ErrorCode.forValue(ex.getErrorCode())) {
                case UNDEFINED_OBJECT:
                    logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                    System.out.println("===========================================================================");
                    return;
                default:
                    logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                            new Object[]{5, ex.toString()});
                    PrintUtil.printGXDLMSException(ex);
                    break;
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_READING_INDEX_LOG,
                    new Object[]{5, ex.toString()});
        }
        PrintUtil.printTOU(object);
        System.out.println("===========================================================================");
    }

    /**
     * Membaca profile generic Object.
     *
     * @param object Object GXDLMSProfileGeneric yang akan dibaca.
     */
    void readEventProfileGeneric(GXDLMSProfileGeneric object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        if (!isAssociationViewReaded()) {
            addToCollection(object);
        }
        logger.log(Level.INFO, "Read capture object ({0} index 3)", object.getLogicalName());
        try {
            read(object, 3);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Err! Failed to read columns:{0}", ex.toString());
            System.out.println("===========================================================================");
            return;
        }
        try {
            GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            pg.setLogicalName(object.getLogicalName());
            pg.setObjectType(ObjectType.PROFILE_GENERIC);
            if (Meter.isLoadProfileEDMIMk7MI(object.getLogicalName())) {
                GXDLMSObject dt = new GXDLMSObject();
                dt.setLogicalName("0.0.1.0.0.255");
                dt.setDescription("Capture Time");
                dt.setObjectType(ObjectType.CLOCK);
                dt.setUIDataType(2, DataType.DATETIME);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
                dt = new GXDLMSObject();
                dt.setLogicalName("1.0.0.8.4.255");
                dt.setDescription("Data");
                dt.setObjectType(ObjectType.DATA);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
            }
            object.getCaptureObjects().forEach((itco) -> {
                pg.addCaptureObject(itco.getKey(), itco.getValue().getAttributeIndex(),
                        itco.getValue().getDataIndex());
            });
            logger.log(Level.INFO, "Jumlah Channel {0} : {1}", new Object[]{object.getDescription(),
                pg.getCaptureObjects().size()});
            long entriesInUse = ((Number) read(pg, 7)).longValue();
            long entries = ((Number) read(pg, 8)).longValue();
            logger.log(Level.INFO, "Entries: {0}/{1}", new Object[]{String.valueOf(entriesInUse),
                String.valueOf(entries)});
            if (entriesInUse == 0 || pg.getCaptureObjects().isEmpty()) {
                if (entriesInUse == 0) {
                    logger.log(Level.WARNING, "Entries in use is 0");
                }
                if (pg.getCaptureObjects().isEmpty()) {
                    logger.log(Level.WARNING, "Capture objects is empty");
                }
                System.out.println("===========================================================================");
                return;
            }
            read(pg, 2);
            Object[] cells = pg.getBuffer();
            for (Map.Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : pg.getCaptureObjects()) {
                System.out.print(captureObject.getKey().getLogicalName() + "(" + 
                        ObisDescriptor.getDeskripsi(captureObject.getKey().getLogicalName())  + ")\t");
            }
            System.out.println("Status");
            System.out.println("---------------------------------------------------------------------------");
            for (Object rows : cells) {
                int a = 0;
                for (Object cell : (Object[]) rows) {
                    String hasil;
                    if (cell instanceof byte[]) {
                        hasil = new String((byte[]) cell);
                    } else {
                        hasil = cell.toString();
                    }
                    if (pg.getCaptureObjects().get(a).getKey().getObjectType().equals(ObjectType.REGISTER)) {
                        if (PhysicalUnit.getPhysicalUnit(
                                ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                        pg.getCaptureObjects().get(a).getKey().
                                                getLogicalName())).getUnit()).getSatuan().equals(
                                        PhysicalUnit.getPhysicalUnit(Unit.SECOND).getSatuan())) {
                            System.out.print(PhysicalUnit.secondToDuration(Integer.parseInt(hasil)) + "\t");

                        } else {
                            System.out.print(hasil + " " + PhysicalUnit.getPhysicalUnit(
                                    ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                            pg.getCaptureObjects().get(a).getKey().
                                                    getLogicalName())).getUnit()).getSatuan() + "\t");
                        }
                    } else {
                        System.out.print(hasil + "\t");
                    }
                    a++;
                }
                try {
                    Object[] objects = (Object[]) rows;
                    int status = (int) objects[1];
                    switch (status) {
                        case 0:
                            System.out.println("Status = disappear");
                            break;
                        case 1:
                            System.out.println("Status = appear");
                            break;
                        case 0x10:
                            System.out.println("Status = Mode prabayar");
                            break;
                        case 0x11:
                            System.out.println("Status = Mode prabayar Export-Import");
                            break;
                        case 0x12:
                            System.out.println("Status = Mode paskabayar");
                            break;
                        case 0x13:
                            System.out.println("Status = Mode paskabayar Export-Import");
                            break;
                        case 0x14:
                            System.out.println("Status = Mode paskabayar Disconnection");
                            break;
                        case 0x15:
                            System.out.println("Status = Mode kWh Lebih");
                            break;
                        case 0x16:
                            System.out.println("Status = Mode Disconnect");
                            break;
                        default:
                            System.out.println("Status = Unknown");
                            break;
                    }
                } catch (Exception e) {
                    System.out.println("Status = Unknown");
                }
            }
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }
}
