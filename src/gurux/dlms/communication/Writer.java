/*
 * 
 */
package gurux.dlms.communication;

import gurux.common.GXCommon;
import gurux.dlms.GXByteBuffer;
import gurux.dlms.GXDLMSException;
import gurux.dlms.GXDateTime;
import gurux.dlms.GXReplyData;
import gurux.dlms.GXSimpleEntry;
import gurux.dlms.GXTime;
import gurux.dlms.client.Global.Obis;
import gurux.dlms.client.Global.util.ObisDescriptor;
import gurux.dlms.client.Global.util.PrintUtil;
import static gurux.dlms.communication.Communicator.dlms;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.object.meter.DCUTIDEMeterListObj;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.object.meter.MeterDCUSMI;
import gurux.dlms.object.meter.MeterType;
import gurux.dlms.objects.GXDLMSActionSchedule;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDayProfile;
import gurux.dlms.objects.GXDLMSDayProfileAction;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSImageTransfer;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.GXDLMSSecuritySetup;
import gurux.dlms.objects.enums.GlobalKeyType;
import gurux.dlms.objects.enums.ImageTransferStatus;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class untuk mengatur instruksi ke meter.
 *
 * @author Tab Solutions
 */
public class Writer extends Communicator {

    /**
     * Menambahkan COSEM object attribute 2 index 0 ke capture object load
     * profile meter (0.0.99.1.0.255 untuk EDMI Mk10M dan 1.0.99.1.0.255 untuk
     * yang lainnya), Perhatian : langkah ini akan otomatis mereset datanya,
     * gunakan method addCaptureObject bila OBIS load profilenya bukan standard
     * electricity. Default attribute 2 index 0 diadakan karena kebanyakan
     * object DLMS menyimpan nilai pada attribute dan index tersebut.
     *
     * @param object COSEM object yang akan ditambahkan.
     */
    public void addLoadProfileCaptureObject(GXDLMSObject object) {
        addLoadProfileCaptureObject(object, 2, 0);
    }

    /**
     * Menambahkan COSEM object attribute 2 index 0 ke capture object load
     * profile meter (0.0.99.1.0.255 untuk EDMI Mk10M dan 1.0.99.1.0.255 untuk
     * yang lainnya), Perhatian : langkah ini akan otomatis mereset datanya,
     * gunakan method addCaptureObject bila OBIS load profilenya bukan standard
     * electricity. Default attribute 2 index 0 diadakan karena kebanyakan
     * object DLMS menyimpan nilai pada attribute dan index tersebut.
     *
     * @param logicalName Logical name dari COSEM object yang akan ditambahkan.
     * @param objectType Object type dari COSEM object yang akan ditambahkan.
     * @param version Versi dari COSEM object yang akan ditambahkan.
     */
    public void addLoadProfileCaptureObject(String logicalName, ObjectType objectType, int version) {
        addLoadProfileCaptureObject(logicalName, objectType, version, 2, 0);
    }

    /**
     * Menambahkan COSEM object ke capture object load profile meter
     * (0.0.99.1.0.255 untuk EDMI Mk10M dan 1.0.99.1.0.255 untuk yang lainnya),
     * Perhatian : langkah ini akan otomatis mereset datanya, gunakan method
     * addCaptureObject bila OBIS load profilenya bukan standard electricity.
     *
     * @param logicalName Logical name dari COSEM object yang akan ditambahkan.
     * @param objectType Object type dari COSEM object yang akan ditambahkan.
     * @param version Versi dari COSEM object yang akan ditambahkan.
     * @param attributeIndex Attribute index (tempat menyimpan nilai) dari COSEM
     * object yang akan ditambahkan.
     * @param dataIndex Data index dari COSEM object yang akan ditambahkan.
     */
    public void addLoadProfileCaptureObject(String logicalName, ObjectType objectType, int version,
            int attributeIndex, int dataIndex) {
        GXDLMSObject object = new GXDLMSObject();
        object.setLogicalName(logicalName);
        object.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        object.setObjectType(objectType);
        object.setVersion(version);
        addLoadProfileCaptureObject(object, attributeIndex, dataIndex);
    }

    /**
     * Menambahkan COSEM object ke capture object load profile meter
     * (0.0.99.1.0.255 untuk EDMI Mk10M dan 1.0.99.1.0.255 untuk yang lainnya),
     * Perhatian : langkah ini akan otomatis mereset datanya, gunakan method
     * addCaptureObject bila OBIS load profilenya bukan standard electricity.
     *
     * @param object COSEM object yang akan ditambahkan.
     * @param attributeIndex Attribute index (tempat menyimpan nilai) dari COSEM
     * object yang akan ditambahkan.
     * @param dataIndex Data index dari COSEM object yang akan ditambahkan.
     */
    public void addLoadProfileCaptureObject(GXDLMSObject object, int attributeIndex, int dataIndex) {
        addCaptureObject(object, attributeIndex, dataIndex, Obis.getLoadProfileObject());
    }

    /**
     * Menambahkan COSEM object attribute 2 index 0 ke capture object dari
     * profile generic object, Perhatian : langkah ini akan otomatis mereset
     * datanya. Default attribute 2 index 0 diadakan karena kebanyakan object
     * DLMS menyimpan nilai pada attribute dan index tersebut.
     *
     * @param object COSEM object yang akan ditambahkan.
     * @param profileGeneric Object profile generic yang akan ditambahkan
     * capture objectnya.
     */
    public void addCaptureObject(GXDLMSObject object, GXDLMSProfileGeneric profileGeneric) {
        addCaptureObject(object, 2, 0, profileGeneric);
    }

    /**
     * Menambahkan COSEM object attribute 2 index 0 ke capture object dari
     * profile generic object, Perhatian : langkah ini akan otomatis mereset
     * datanya. Default attribute 2 index 0 diadakan karena kebanyakan object
     * DLMS menyimpan nilai pada attribute dan index tersebut.
     *
     * @param logicalName Logical name dari COSEM object yang akan ditambahkan.
     * @param objectType Object type dari COSEM object yang akan ditambahkan.
     * @param version Versi dari COSEM object yang akan ditambahkan.
     * @param profileGeneric Object profile generic yang akan ditambahkan
     * capture objectnya.
     */
    public void addCaptureObject(String logicalName, ObjectType objectType,
            int version, GXDLMSProfileGeneric profileGeneric) {
        addCaptureObject(logicalName, objectType, version, 2, 0, profileGeneric);
    }

    /**
     * Menambahkan COSEM object ke capture object dari profile generic object,
     * Perhatian : langkah ini akan otomatis mereset datanya.
     *
     * @param logicalName Logical name dari COSEM object yang akan ditambahkan.
     * @param objectType Object type dari COSEM object yang akan ditambahkan.
     * @param version Versi dari COSEM object yang akan ditambahkan.
     * @param attributeIndex Attribute index (tempat menyimpan nilai) dari COSEM
     * object yang akan ditambahkan.
     * @param dataIndex Data index dari COSEM object yang akan ditambahkan.
     * @param profileGeneric Object profile generic yang akan ditambahkan
     * capture objectnya.
     */
    public void addCaptureObject(String logicalName, ObjectType objectType, int version, int attributeIndex,
            int dataIndex, GXDLMSProfileGeneric profileGeneric) {
        GXDLMSObject object = new GXDLMSObject();
        object.setLogicalName(logicalName);
        object.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        object.setObjectType(objectType);
        object.setVersion(version);
        addCaptureObject(object, attributeIndex, dataIndex, profileGeneric);
    }

    /**
     * Menambahkan COSEM object ke capture object dari profile generic object,
     * Perhatian : langkah ini akan otomatis mereset datanya.
     *
     * @param object COSEM object yang akan ditambahkan.
     * @param attributeIndex Attribute index (tempat menyimpan nilai) dari COSEM
     * object yang akan ditambahkan.
     * @param dataIndex Data index dari COSEM object yang akan ditambahkan.
     * @param profileGeneric Object profile generic yang akan ditambahkan
     * capture objectnya.
     */
    public void addCaptureObject(GXDLMSObject object, int attributeIndex,
            int dataIndex, GXDLMSProfileGeneric profileGeneric) {
        logger.log(Level.WARNING, "Adding {0}({1}) into capture object (attribute 3) of {2}({3})",
                new Object[]{object.getLogicalName(), object.getDescription(),
                    profileGeneric.getLogicalName(),
                    profileGeneric.getDescription()});
        try {
            read(profileGeneric, 3);
            int sizeAwal = profileGeneric.getCaptureObjects().size();
            for (Map.Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : profileGeneric.getCaptureObjects()) {
                if (captureObject.getKey().getLogicalName().equals(object.getLogicalName())) {
                    logger.log(Level.SEVERE, "Object {0}({1}) sudah ada dalam capture object",
                            new Object[]{object.getLogicalName(), object.getDescription()});
                    System.out.println("===========================================================================");
                    return;
                }
            }
            profileGeneric.addCaptureObject(object, attributeIndex, dataIndex);
            writeObject(profileGeneric, 3);
            read(profileGeneric, 3);
            int sizeAkhir = profileGeneric.getCaptureObjects().size();
            logger.log(Level.WARNING, "Reset profile generic buffer");
            if (sizeAkhir > sizeAwal) {
                if (Meter.meterType.equals(MeterType.SMI_1_Phase) && profileGeneric.getLogicalName().equals(
                        Obis.getLoadProfileObject().getLogicalName())) {
                    resetLPSMI();
                } else {
                    readDLMSPacket(profileGeneric.reset(dlms));
                }
                logger.log(Level.INFO, "adding {0}({1}) version {2} with attribute index {3} and "
                        + "data index {4} object into captured object success",
                        new Object[]{object.getLogicalName(), object.getDescription(), object.getVersion(),
                            attributeIndex, dataIndex});
                System.out.println("===========================================================================");
            } else {
                logger.log(Level.SEVERE, "Gagal menambah capture object");
                removeCaptureObject(object.getLogicalName(), profileGeneric);
                System.out.println("===========================================================================");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Gagal membaca capture object cause {0}", ex.toString());
            removeCaptureObject(object.getLogicalName(), profileGeneric);
            System.out.println("===========================================================================");
        }
    }

    /**
     * Menghapus COSEM object dari capture object load profile meter
     * (0.0.99.1.0.255 untuk EDMI Mk10M dan 1.0.99.1.0.255 untuk yang lainnya),
     * Perhatian : langkah ini akan otomatis mereset datanya, gunakan method
     * removeCaptureObject jika OBIS load profilenya bukan standard electricity.
     *
     * @param logicalName Logical name dari COSEM object yang akan dihapus.
     */
    public void removeLoadProfileCaptureObject(String logicalName) {
        removeCaptureObject(logicalName, Obis.getLoadProfileObject());
    }

    /**
     * Menghapus COSEM object dari capture object profile generic meter,
     * Perhatian : langkah ini akan otomatis mereset datanya.
     *
     * @param logicalName Logical name dari COSEM object yang akan dihapus.
     * @param profileGeneric Cosem object yang akan dihapus salah satu capture
     * objectnya.
     */
    public void removeCaptureObject(String logicalName, GXDLMSProfileGeneric profileGeneric) {
        logger.log(Level.WARNING, "Removing {0}({1}) from capture object (attribute 3) of {2}({3})",
                new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName),
                    profileGeneric.getLogicalName(),
                    profileGeneric.getDescription()});
        try {
            read(profileGeneric, 3);
            boolean ada = false;
            int indexObject = 0;
            for (Map.Entry<GXDLMSObject, GXDLMSCaptureObject> captureObject : profileGeneric.getCaptureObjects()) {
                if (captureObject.getKey().getLogicalName().equals(logicalName)) {
                    logger.log(Level.INFO, "Object {0}({1}) will be deleted",
                            new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName)});
                    indexObject = profileGeneric.getCaptureObjects().indexOf(captureObject);
                    ada = true;
                    break;
                }
            }
            if (ada) {
                profileGeneric.getCaptureObjects().remove(indexObject);
                writeObject(profileGeneric, 3);
                if (Meter.meterType.equals(MeterType.SMI_1_Phase) && profileGeneric.getLogicalName().equals(
                        Obis.getLoadProfileObject().getLogicalName())) {
                    resetLPSMI();
                } else {
                    readDLMSPacket(profileGeneric.reset(dlms));
                }
                logger.log(Level.INFO, "deleting {0}({1}) from captured object success",
                        new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName)});
                System.out.println("===========================================================================");
            } else {
                logger.log(Level.INFO, "Object {0}({1}) tidak ada di captured object",
                        new Object[]{logicalName, ObisDescriptor.getDeskripsi(logicalName)});
                System.out.println("===========================================================================");
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Mereset buffer dari load profile SMI.
     */
    void resetLPSMI() {
        try {
            logger.log(Level.WARNING, "Resetting meter load profile buffer");
            GXDLMSScriptTable scriptTable = new GXDLMSScriptTable("0.0.10.0.0.255");
            scriptTable.setVersion(0);
            scriptTable.setDescription(ObisDescriptor.getDeskripsi("0.0.10.0.0.255"));
            readDLMSPacket(scriptTable.execute(dlms, 6));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        }
    }

    /**
     * Mengatur waktu pengaktifan passive calendar.Cosem object yang digunakan
     * adalah standard blue book yaitu "0.0.13.0.0.255"
     *
     * @param calendar Waktu pengaktifan dalam bentuk java.util.Calendar
     */
    public void setActivatePassiveCalendarDate(Calendar calendar) {
        setActivatePassiveCalendarDate("0.0.13.0.0.255", calendar);
    }

    /**
     * Mengatur waktu pengaktifan passive calendar.
     *
     * @param calendar Waktu pengaktifan dalam bentuk java.util.Calendar
     * @param logicalName Logical name dari object activity calendar.
     */
    public void setActivatePassiveCalendarDate(String logicalName, Calendar calendar) {
        GXDLMSActivityCalendar activityCalendar = new GXDLMSActivityCalendar(logicalName);
        activityCalendar.setVersion(0);
        activityCalendar.setDescription(ObisDescriptor.getDeskripsi(activityCalendar.getLogicalName()));
        setActivatePassiveCalendarDate(calendar, activityCalendar);
    }

    /**
     * Mengatur waktu pengaktifan passive calendar.
     *
     * @param calendar Waktu pengaktifan dalam bentuk java.util.Calendar
     * @param activityCalendar Cosem object activity calendar
     */
    void setActivatePassiveCalendarDate(Calendar calendar, GXDLMSActivityCalendar activityCalendar) {
        logger.log(Level.WARNING, PrintUtil.WRITING_LOG, new Object[]{activityCalendar.getObjectType(),
            activityCalendar.getLogicalName(), activityCalendar.getDescription()});
        try {
            GXDateTime dateTime = new GXDateTime(calendar);
            activityCalendar.setTime(dateTime);
            writeObject(activityCalendar, 10);
            logger.log(Level.INFO, "Passive calendar will active at {0}", activityCalendar.getTime());
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Mengaktifkan passive calendar. Cosem object yang digunakan adalah
     * standard blue book yaitu "0.0.13.0.0.255"
     */
    public void activatePassiveCalendar() {
        activatePassiveCalendar("0.0.13.0.0.255");
    }

    /**
     * Mengaktifkan passive calendar.
     *
     * @param logicalName Logical name dari object activity calendar.
     */
    public void activatePassiveCalendar(String logicalName) {
        GXDLMSActivityCalendar activityCalendar = new GXDLMSActivityCalendar(logicalName);
        activityCalendar.setVersion(0);
        activityCalendar.setDescription(ObisDescriptor.getDeskripsi(activityCalendar.getLogicalName()));
        activatePassiveCalendar(activityCalendar);
    }

    /**
     * Mengaktifkan passive calendar.
     */
    void activatePassiveCalendar(GXDLMSActivityCalendar activityCalendar) {
        logger.log(Level.WARNING, "Activating {0}({1}) passive calendar",
                new Object[]{activityCalendar.getLogicalName(), activityCalendar.getDescription()});
        try {
            readDLMSPacket(dlms.method(activityCalendar.getLogicalName(), ObjectType.ACTIVITY_CALENDAR, 1, 0, DataType.INT32));
        } catch (Exception ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, ex.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Mengeksekusi remote disconnect dari object disconnect control dengan
     * standardOBIS yaitu "0.0.96.3.10.255".
     */
    public void remoteDisconnect() {
        remoteDisconnect("0.0.96.3.10.255");
    }

    /**
     * Mengeksekusi remote disconnect dari object disconnect control.
     *
     * @param logicalName Logical name dari object GXDLMSDisconnectControl.
     */
    public void remoteDisconnect(String logicalName) {
        GXDLMSDisconnectControl disconnectControl = new GXDLMSDisconnectControl(logicalName);
        disconnectControl.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        disconnectControl.setVersion(0);
        remoteDisconnect(disconnectControl);
    }

    /**
     * Mengeksekusi remote disconnect dari object disconnect control.
     *
     * @param disconnectControl OBIS tempat melakukan disconnect control.
     */
    void remoteDisconnect(GXDLMSDisconnectControl disconnectControl) {
        logger.log(Level.WARNING, "Disconnecting {0} Meter", getNomorMeter());
        try {
            readDLMSPacket(disconnectControl.remoteDisconnect(dlms));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Mengeksekusi remote reconnect dari object disconnect control dengan
     * standardOBIS yaitu "0.0.96.3.10.255".
     */
    public void remoteReconnect() {
        remoteReconnect("0.0.96.3.10.255");
    }

    /**
     * Mengeksekusi remote reconnect dari object disconnect control.
     *
     * @param logicalName Logical name dari object GXDLMSDisconnectControl.
     */
    public void remoteReconnect(String logicalName) {
        GXDLMSDisconnectControl disconnectControl = new GXDLMSDisconnectControl(logicalName);
        disconnectControl.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        disconnectControl.setVersion(0);
        remoteReconnect(disconnectControl);
    }

    /**
     * Mengeksekusi remote reconnect dari object disconnect control.
     *
     * @param disconnectControl OBIS tempat melakukan disconnect control.
     */
    void remoteReconnect(GXDLMSDisconnectControl disconnectControl) {
        logger.log(Level.WARNING, "Reconnecting {0} Meter", getNomorMeter());
        try {
            readDLMSPacket(disconnectControl.remoteReconnect(dlms));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Mengatur ambang batas limiter pada standard OBIS (0.0.17.0.0.255).
     *
     * @param attributeIndex Attribute index yang akan diatur ambang batasnya.
     * 3(ambang batas aktif), 4(ambang batas normal), 5(ambang batas emergency),
     * 6(minimal over duration), 7 (minimal under duration).
     * @param newValue Nilai value yang akan di set ke attribute index.
     */
    public void writeLimiterThreshold(int attributeIndex, String newValue) {
        writeLimiterThreshold("0.0.17.0.0.255", attributeIndex, newValue);
    }

    /**
     * Mengatur ambang batas.
     *
     * @param logicalName Logical name dari object GXDLMSLimiter.
     * @param attributeIndex Attribute index yang akan diatur ambang batasnya.
     * 3(ambang batas aktif), 4(ambang batas normal), 5(ambang batas emergency),
     * 6(minimal over duration), 7 (minimal under duration).
     * @param newValue Nilai value yang akan di set ke attribute index.
     */
    public void writeLimiterThreshold(String logicalName, int attributeIndex, String newValue) {
        GXDLMSLimiter limiter = new GXDLMSLimiter(logicalName);
        limiter.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        limiter.setVersion(0);
        writeLimiterThreshold(limiter, attributeIndex, newValue);
    }

    /**
     * Mengatur ambang batas.
     *
     * @param limiter OBIS dari limiter yang akan diset.
     * @param attributeIndex Attribute index yang akan diatur ambang batasnya.
     * 3(ambang batas aktif), 4(ambang batas normal), 5(ambang batas emergency),
     * 6(minimal over duration), 7 (minimal under duration).
     * @param newValue Nilai value yang akan di set ke attribute index.
     */
    void writeLimiterThreshold(GXDLMSLimiter limiter, int attributeIndex, String newValue) {
        logger.log(Level.WARNING, "Write object {0}({1}) index {2}",
                new Object[]{limiter.getLogicalName(), limiter.getDescription(), attributeIndex});
        try {
            switch (attributeIndex) {
                case 3:
                case 4:
                case 5:
                    long val = Long.parseLong(newValue);
                    readDLMSPacket(dlms.write(limiter.getLogicalName(), val,
                            DataType.UINT32, ObjectType.LIMITER, attributeIndex));
                    logger.log(Level.INFO, "Write index {0} object {1}({2}) sukses",
                            new Object[]{attributeIndex, limiter.getLogicalName(), limiter.getDescription()});
                    break;
                case 6:
                    limiter.setMinOverThresholdDuration(Integer.parseInt(newValue));
                    writeObject(limiter, 6);
                    logger.log(Level.INFO, "Write index {0} object {1}({2}) sukses",
                            new Object[]{attributeIndex, limiter.getLogicalName(), limiter.getDescription()});
                    break;
                case 7:
                    limiter.setMinUnderThresholdDuration(Integer.parseInt(newValue));
                    writeObject(limiter, 7);
                    logger.log(Level.INFO, "Write index {0} object {1}({2}) sukses",
                            new Object[]{attributeIndex, limiter.getLogicalName(), limiter.getDescription()});
                    break;
                default:
                    logger.log(Level.INFO, "Hanya untuk attribute index 3, 4, 5, 6, 7, 10");
                    break;
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Mengatur waktu pengaktifan emergency profile limiter pada standard OBIS
     * (0.0.17.0.0.255).
     *
     * @param calendar Waktu pengaktifan emergency profile dalam bentuk Calendar
     * java object.
     * @param duration Durasi waktu pengaktifan emergency profile.
     */
    public void setEmergencyProfileLimiter(Calendar calendar, long duration) {
        setEmergencyProfileLimiter("0.0.17.0.0.255", calendar, duration);
    }

    /**
     * Mengatur waktu pengaktifan emergency profile limiter.
     *
     * @param logicalName Logical name dari object GXDLMSLimiter.
     * @param calendar Waktu pengaktifan emergency profile dalam bentuk Calendar
     * java object.
     * @param duration Durasi waktu pengaktifan emergency profile.
     */
    public void setEmergencyProfileLimiter(String logicalName, Calendar calendar, long duration) {
        GXDLMSLimiter limiter = new GXDLMSLimiter(logicalName);
        limiter.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        limiter.setVersion(0);
        setEmergencyProfileLimiter(limiter, calendar, duration);
    }

    /**
     * Mengatur waktu pengaktifan emergency profile limiter.
     *
     * @param limiter OBIS dari limiter yang akan diset emergency profilenya.
     * @param calendar Waktu pengaktifan emergency profile dalam bentuk Calendar
     * java object.
     * @param duration Durasi waktu pengaktifan emergency profile.
     */
    void setEmergencyProfileLimiter(GXDLMSLimiter limiter, Calendar calendar, long duration) {
        logger.log(Level.WARNING, "set emergency profile {0}({1})",
                new Object[]{limiter.getLogicalName(), limiter.getDescription()});
        try {
            limiter.getEmergencyProfile().setID(1);
            limiter.getEmergencyProfile().setDuration(duration);
            GXDateTime dateTime = new GXDateTime(calendar);
            limiter.getEmergencyProfile().setActivationTime(dateTime);
            writeObject(limiter, 8);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Menambah passive day profile action dengan OBIS standard
     * (0.0.13.0.0.255).
     *
     * @param dayID Day ID yang akan ditambahkan day profile actionnya.
     * @param dayProfileLogicalName Logical name script dari day profile.
     * @param scriptSelector Selector dari scrip day profile.
     * @param startTime Waktu mulai day profile.
     */
    public void addPassiveDayProfile(int dayID, String dayProfileLogicalName,
            int scriptSelector, GXTime startTime) {
        addPassiveDayProfile("0.0.13.0.0.255", dayID, dayProfileLogicalName, scriptSelector, startTime);
    }

    /**
     * Mengatur calendar passive name di OBIS standard (0.0.13.0.0.255).
     *
     * @param calendarPassiveName Nama dari kalender pasif.
     */
    public void setCalendarPassiveName(String calendarPassiveName) {
        setCalendarPassiveName("0.0.13.0.0.255", calendarPassiveName);
    }

    /**
     * Menambah day ID di OBIS standard (0.0.13.0.0.255).
     *
     * @param dayID Nama dari kalender pasif.
     */
    public void addDayID(int dayID) {
        addDayID("0.0.13.0.0.255", dayID);
    }

    /**
     * Menambah passive day profile action.
     *
     * @param activityCalendarLogicalName Logical name OBIS activity calendar
     * yang akan ditambahkan day profile actionnya.
     * @param dayID Day ID yang akan ditambahkan day profile actionnya.
     * @param dayProfileLogicalName Logical name script dari day profile.
     * @param scriptSelector Selector dari scrip day profile.
     * @param startTime Waktu mulai day profile.
     */
    public void addPassiveDayProfile(String activityCalendarLogicalName, int dayID, String dayProfileLogicalName,
            int scriptSelector, GXTime startTime) {
        GXDLMSActivityCalendar activityCalendar = new GXDLMSActivityCalendar(activityCalendarLogicalName);
        activityCalendar.setDescription(ObisDescriptor.getDeskripsi(activityCalendarLogicalName));
        activityCalendar.setVersion(0);
        GXDLMSDayProfileAction dayProfileAction = new GXDLMSDayProfileAction();
        dayProfileAction.setScriptLogicalName(dayProfileLogicalName);
        dayProfileAction.setScriptSelector(scriptSelector);
        dayProfileAction.setStartTime(startTime);
        addPassiveDayProfile(activityCalendar, dayID, dayProfileAction);
    }

    /**
     * Mengatur calendar passive name di OBIS standard (0.0.13.0.0.255).
     *
     * @param activityCalendarLogicalName Logical name OBIS activity calendar.
     * @param calendarPassiveName Nama dari kalender pasif.
     */
    public void setCalendarPassiveName(String activityCalendarLogicalName, String calendarPassiveName) {
        GXDLMSActivityCalendar activityCalendar = new GXDLMSActivityCalendar(activityCalendarLogicalName);
        activityCalendar.setDescription(ObisDescriptor.getDeskripsi(activityCalendarLogicalName));
        activityCalendar.setVersion(0);
        setCalendarPassiveName(activityCalendar, calendarPassiveName);
    }

    /**
     * Menambah day ID di OBIS standard (0.0.13.0.0.255).
     *
     * @param activityCalendarLogicalName Logical name OBIS activity calendar.
     * @param dayID Nama dari kalender pasif.
     */
    public void addDayID(String activityCalendarLogicalName, int dayID) {
        GXDLMSActivityCalendar activityCalendar = new GXDLMSActivityCalendar(activityCalendarLogicalName);
        activityCalendar.setDescription(ObisDescriptor.getDeskripsi(activityCalendarLogicalName));
        activityCalendar.setVersion(0);
        logger.log(Level.WARNING, "add day ID {0}({1} with {2})",
                new Object[]{activityCalendar.getLogicalName(), activityCalendar.getDescription(),
                    dayID});
        for (int pos : activityCalendar.getAttributeIndexToRead(true)) {
            try {
                read(activityCalendar, pos);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
                System.out.println("===========================================================================");
                return;
            }
        }
        boolean ada = false;
        for (int i = 0; i < activityCalendar.getDayProfileTablePassive().length; i++) {
            if (activityCalendar.getDayProfileTablePassive()[i].getDayId() == dayID) {
                ada = true;
            }
        }
        if (!ada) {
            try {
                for (int i = 0; i < activityCalendar.getDayProfileTablePassive().length; i++) {
                    activityCalendar.getDayProfileTablePassive()[i].setDayId(dayID);
                }
                GXDLMSDayProfileAction[] actions = {};
                GXDLMSDayProfile dLMSDayProfile = new GXDLMSDayProfile(0, actions);
                GXDLMSDayProfile[] dLMSDayProfiles = {dLMSDayProfile};
                activityCalendar.setDayProfileTablePassive(dLMSDayProfiles);
                writeObject(activityCalendar, 9);
            } catch (Exception e) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, e.toString());
            }
        } else {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "Day ID sudah ada");
        }
        System.out.println("===========================================================================");
    }

    /**
     * Menambah passive day profile action.
     *
     * @param activityCalendar OBIS activity calendar yang akan ditambahkan day
     * profile actionnya.
     * @param dayID Day ID yang akan ditambahkan day profile actionnya.
     * @param dayProfileAction Data day profile action yang akan ditambahkan.
     */
    void addPassiveDayProfile(GXDLMSActivityCalendar activityCalendar, int dayID,
            GXDLMSDayProfileAction dayProfileAction) {
        logger.log(Level.WARNING, "add day profile {0}({1} at day ID {2})",
                new Object[]{activityCalendar.getLogicalName(), activityCalendar.getDescription(), dayID});
        for (int pos : activityCalendar.getAttributeIndexToRead(true)) {
            try {
                read(activityCalendar, pos);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
                System.out.println("===========================================================================");
                return;
            }
        }
        boolean ada = false;
        for (int i = 0; i < activityCalendar.getDayProfileTablePassive().length; i++) {
            if (activityCalendar.getDayProfileTablePassive()[i].getDayId() == dayID) {
                ada = true;
                GXDLMSDayProfileAction[] dayProfileActions = new GXDLMSDayProfileAction[activityCalendar.getDayProfileTablePassive()[i].getDaySchedules().length + 1];
                for (int j = 0; j < dayProfileActions.length; j++) {
                    if (j == activityCalendar.getDayProfileTablePassive()[i].getDaySchedules().length) {
                        dayProfileActions[j] = dayProfileAction;
                    } else {
                        dayProfileActions[j] = activityCalendar.getDayProfileTablePassive()[i].
                                getDaySchedules()[j];
                    }
                }
                activityCalendar.getDayProfileTablePassive()[i].setDaySchedules(dayProfileActions);
                break;
            }
        }
        if (ada) {
            try {
                writeObject(activityCalendar, 9);
            } catch (Exception e) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, e.toString());
            }
        } else {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, "Day ID tidak ada");
        }
        System.out.println("===========================================================================");
    }

    /**
     * Mengatur calendar passive name di OBIS standard (0.0.13.0.0.255).
     *
     * @param activityCalendar OBIS activity calendar yang akan diatur nama
     * kalender pasifnya.
     * @param calendarPassiveName Nama dari kalender pasif.
     */
    public void setCalendarPassiveName(GXDLMSActivityCalendar activityCalendar, String calendarPassiveName) {
        logger.log(Level.WARNING, "set calendar passive name {0}({1} to {2})",
                new Object[]{activityCalendar.getLogicalName(), activityCalendar.getDescription(),
                    calendarPassiveName});
        try {
            writeObject(activityCalendar, 6);
        } catch (Exception e) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, e.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Write ID PLN Meter SMI POC.
     *
     * @param nilai Value ID PLN yang akan di set.
     */
    public void writeIDPLNSMI(String nilai) {
        GXDLMSData data = new GXDLMSData("0.0.96.1.10.255");
        data.setDescription("Kode Unit PLN");
        data.setDataType(2, DataType.OCTET_STRING);
        data.setUIDataType(2, DataType.STRING);
        data.setValue(nilai);
        logger.log(Level.WARNING, "Write object {0}({1}) with value {2}",
                new Object[]{data.getLogicalName(), data.getDescription(), nilai.toString()});
        try {
            writeObject(data, 2);
            logger.log(Level.INFO, "Write index {0} object {1}({2}) sukses",
                    new Object[]{"2", data.getLogicalName(), data.getDescription()});
        } catch (Exception e) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, e.getMessage());
        }
        System.out.println("===========================================================================");
    }

    public void setWakturekamBillingSMI(int tanggal) {
        GXDLMSActionSchedule actionSchedule = new GXDLMSActionSchedule("0.0.15.0.0.255");
        for (int pos : actionSchedule.getAttributeIndexToRead(true)) {
            try {
                read(actionSchedule, pos);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
                System.out.println("===========================================================================");
                return;
            }
        }
        GXDateTime waktuRekamBilling = actionSchedule.getExecutionTime()[0];
        waktuRekamBilling.getMeterCalendar().set(Calendar.DATE, tanggal);
        waktuRekamBilling.getLocalCalendar().set(Calendar.DATE, tanggal);
        GXDateTime[] dateTimes = {waktuRekamBilling};
        actionSchedule.setExecutionTime(dateTimes);
        try {
            writeObject(actionSchedule, 4);
            logger.log(Level.INFO, "Write index {0} object {1}({2}) sukses",
                    new Object[]{"4", actionSchedule.getLogicalName(), actionSchedule.getDescription()});
        } catch (Exception e) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, e.getMessage());
        }
        System.out.println("===========================================================================");
    }

    public void resetProfileGenericBuffer(String logicalName) {
        GXDLMSProfileGeneric profileGeneric = new GXDLMSProfileGeneric(logicalName);
        profileGeneric.setVersion(1);
        profileGeneric.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        resetProfileGenericBuffer(profileGeneric);
    }

    void resetProfileGenericBuffer(GXDLMSProfileGeneric profileGeneric) {
        logger.log(Level.WARNING, "Resetting " + profileGeneric.getDescription() + " buffer");
        try {
            readDLMSPacket(profileGeneric.reset(dlms));
            logger.log(Level.INFO, "Buffer reseted");
        } catch (Exception e) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, e.getMessage());
        }
        System.out.println("===========================================================================");
    }

    public void writeWaktuRekamBilling(int tanggal, int jam, int menit) {
        byte[] bs = {
            (byte) 0xFF, (byte) 0xFF,
            (byte) 0xFF,
            (byte) tanggal,
            (byte) 0XFF,
            (byte) jam,
            (byte) menit,
            (byte) 0x0,
            (byte) 0x0,
            (byte) 0xFF, (byte) 0xFF,
            (byte) 0xFF};
        GXDLMSData data = new GXDLMSData("1.0.98.1.128.255");
        data.setDescription(ObisDescriptor.getDeskripsi("1.0.98.1.128.255"));
        data.setVersion(0);
        for (int pos : data.getAttributeIndexToRead(true)) {
            try {
                read(data, pos);
            } catch (Exception e) {
                System.out.println("Error : " + e.toString());
                System.out.println("===========================================================================");
            }
        }
        byte[] value = (byte[]) data.getValue();
        bs[9] = value[9];
        bs[10] = value[10];
        bs[11] = value[11];
        data.setValue(bs);
        System.out.println(GXCommon.bytesToHex((byte[]) data.getValue()));
        try {
            writeObject(data, 2);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            System.out.println("Write error : " + ex.toString());
            System.out.println("===========================================================================");
        }
    }

    public void deleteMeterFromListSMI(String[] arrayNoMeter) {
        logger.log(Level.WARNING, "delete meter from list DCU SMI");
        try {
            byte[][] arrayOctetString = new byte[arrayNoMeter.length][16];
            for (int i = 0; i < arrayOctetString.length; i++) {
                arrayOctetString[i] = arrayNoMeter[i].getBytes();
            }
            byte[][] bses = dlms.method("0.0.96.81.0.255", ObjectType.DATA, 4,
                    arrayOctetString, DataType.ARRAY);
            readDLMSPacket(bses);
            System.out.println("===========================================================================");
        } catch (Exception e) {
            System.out.println("Delete error : " + e.toString());
            System.out.println("===========================================================================");
        }
    }

    public void writeMeterListDCUSMI(MeterDCUSMI[] meterDCUSMIs) {
        logger.log(Level.WARNING, "Write meter list DCU SMI");
        try {
            GXDLMSData meterList = new GXDLMSData("0.0.96.81.0.255");
            meterList.setVersion(0);
            meterList.setDataType(2, DataType.ARRAY);
            meterList.setValue(MeterDCUSMI.getStructure(meterDCUSMIs));
            byte[][] bses = dlms.method("0.0.96.81.0.255", ObjectType.DATA, 104,
                    MeterDCUSMI.getStructure(meterDCUSMIs), DataType.ARRAY);
            GXByteBuffer buffer = new GXByteBuffer();
            for (int i = 0; i < bses.length; i++) {
                byte[] header = new byte[23];
                for (int j = 0; j < bses[i].length; j++) {
                    if (j <= 21) {
                        header[j] = bses[i][j];
                    } else {
                        break;
                    }
                }
                header[22] = (byte) meterDCUSMIs.length;
                buffer.set(header);
                for (MeterDCUSMI meterDCUSMI : meterDCUSMIs) {
                    buffer.setUInt8(2);
                    buffer.setUInt8(23);
                    buffer.setUInt8(18);
                    buffer.setUInt16(meterDCUSMI.getSerialNumber());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getLogicalDeviceName().length);
                    buffer.set(meterDCUSMI.getLogicalDeviceName());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getPortNo());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getBaudRate());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getCommProtocolType());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getDataBitLength());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getStartBit());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getStopBit());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getCheckBit());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getMeterTariff());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getWiringMethod());
                    buffer.setUInt8(18);
                    buffer.setUInt16(meterDCUSMI.getCommClient());
                    buffer.setUInt8(18);
                    buffer.setUInt16(meterDCUSMI.getRs485LogicalAddr());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getCollectorAddr().length);
                    buffer.set(meterDCUSMI.getCollectorAddr());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getCommEncryption());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getaARQAuthLevel());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getlLSKey().length);
                    buffer.set(meterDCUSMI.getlLSKey());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getAuthKey().length);
                    buffer.set(meterDCUSMI.getAuthKey());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getBroadcastKey().length);
                    buffer.set(meterDCUSMI.getBroadcastKey());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getDedicateKey().length);
                    buffer.set(meterDCUSMI.getDedicateKey());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getEncriptionKey().length);
                    buffer.set(meterDCUSMI.getEncriptionKey());
                    buffer.setUInt8(9);
                    buffer.setUInt8(meterDCUSMI.getMasterKey().length);
                    buffer.set(meterDCUSMI.getMasterKey());
                    buffer.setUInt8(17);
                    buffer.setUInt8(meterDCUSMI.getKeyMeterFlag());
                }
                bses[i] = buffer.getData();
            }
            readDLMSPacket(bses);
            System.out.println("===========================================================================");
        } catch (Exception e) {
            System.out.println("Write error : " + e.toString());
            System.out.println("===========================================================================");
        }
    }

    public void updateFirmware() {
        GXDLMSImageTransfer dLMSImageTransfer = new GXDLMSImageTransfer("0.0.44.0.0.255");
        GXReplyData reply = new GXReplyData();
        try {
            dLMSImageTransfer.setImageTransferStatus(ImageTransferStatus.IMAGE_VERIFICATION_FAILED);
            System.out.println(dLMSImageTransfer.getImageTransferStatus().toString());
            read(dLMSImageTransfer, 6);
            System.out.println(dLMSImageTransfer.getImageTransferStatus().toString());
        } catch (Exception ex) {
            Logger.getLogger(Writer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Mengatur kunci keamanan dengan default OBIS 0.0.43.0.0.255
     *
     * @param keyType Enum dari key tipe(AUTHENTICATION_KEY atau
     * BLOCK_CIPHER_KEY), selain itu akan dianggap BLOCK_CIPHER_KEY
     * @param newKey Kunci kemanan yang baru.
     * @param masterKey Master key meter.
     */
    public void executeKeyTransfer(SecurityKeyType keyType, String newKey, String masterKey) {
        GXDLMSSecuritySetup dLMSSecuritySetup = new GXDLMSSecuritySetup("0.0.43.0.0.255");
        dLMSSecuritySetup.setVersion(1);
        executeKeyTransfer(dLMSSecuritySetup, keyType, newKey, masterKey);
    }

    /**
     * Mengatur kunci keamanan.
     *
     * @param logicalName Logical name dari object security setup dengan version
     * 1;
     * @param keyType Enum dari key tipe(AUTHENTICATION_KEY atau
     * BLOCK_CIPHER_KEY), selain itu akan dianggap BLOCK_CIPHER_KEY
     * @param newKey Kunci kemanan yang baru.
     * @param masterKey Master key meter.
     */
    public void executeKeyTransfer(String logicalName, SecurityKeyType keyType, String newKey, String masterKey) {
        GXDLMSSecuritySetup dLMSSecuritySetup = new GXDLMSSecuritySetup(logicalName);
        dLMSSecuritySetup.setVersion(1);
        executeKeyTransfer(dLMSSecuritySetup, keyType, newKey, masterKey);
    }

    /**
     * Mengatur kunci keamanan.
     *
     * @param logicalName Logical name dari object security setup;
     * @param version Versi dari object security setup;
     * @param keyType Enum dari key tipe(AUTHENTICATION_KEY atau
     * BLOCK_CIPHER_KEY), selain itu akan dianggap BLOCK_CIPHER_KEY
     * @param newKey Kunci kemanan yang baru.
     * @param masterKey Master key meter.
     */
    public void executeKeyTransfer(String logicalName, int version, SecurityKeyType keyType, String newKey, String masterKey) {
        GXDLMSSecuritySetup dLMSSecuritySetup = new GXDLMSSecuritySetup(logicalName);
        dLMSSecuritySetup.setVersion(1);
        executeKeyTransfer(dLMSSecuritySetup, keyType, newKey, masterKey);
    }

    /**
     * Mengatur kunci keamanan.
     *
     * @param gxdlmsss Object security setup;
     * @param keyType Enum dari key tipe(AUTHENTICATION_KEY atau
     * BLOCK_CIPHER_KEY), selain itu akan dianggap BLOCK_CIPHER_KEY
     * @param newKey Kunci kemanan yang baru.
     * @param masterKey Master key meter.
     *
     * @exception Error handling.
     */
    void executeKeyTransfer(GXDLMSSecuritySetup gxdlmsss, SecurityKeyType keyType, String newKey, String masterKey) {
        logger.log(Level.WARNING, "Set new " + keyType.toString() + " for {0}({1})",
                new Object[]{gxdlmsss.getLogicalName(), gxdlmsss.getDescription()});
        if (newKey.length() != 16) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, "Perubahan kunci kemanan gagal, "
                    + "panjang kunci kemanan harus 16 octet");
            System.out.println("===========================================================================");
            return;
        }
        try {
            List<GXSimpleEntry<GlobalKeyType, byte[]>> entrys = new ArrayList<>();
            GlobalKeyType globalKeyType;
            if (keyType.equals(SecurityKeyType.AUTHENTICATION_KEY)) {
                globalKeyType = GlobalKeyType.AUTHENTICATION;
            } else {
                globalKeyType = GlobalKeyType.UNICAST_ENCRYPTION;
            }
            GXSimpleEntry<GlobalKeyType, byte[]> entry = new GXSimpleEntry(globalKeyType, newKey.getBytes());
            entrys.add(entry);;
            readDLMSPacket(gxdlmsss.globalKeyTransfer(dlms, masterKey.getBytes(), entrys));
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, "Perubahan kunci keamanan sukses, {0} yang baru adalah {1}",
                    new Object[]{keyType.toString(), newKey});
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, "Perubahan kunci kemanan gagal : {0}",
                    ex.toString());
            System.out.println("===========================================================================");
        }
    }

    public void imageTransfer() {
        try {
            GXReplyData reply = new GXReplyData();
            GXDLMSImageTransfer target = new GXDLMSImageTransfer("0.0.44.0.0.255");
            File file = new File("FL163_K20_01.bin");
            byte[] byteFile = new byte[(int) file.length()];
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
                //read file into bytes[]
                fis.read(byteFile);
            } finally {
                if (fis != null) {
                    fis.close();
                }
            }
            System.out.println("File size : " + byteFile.length);
            System.out.println("Precondition");
            read(target, 5);
            System.out.println("Image transfer enabled is " + target.getImageTransferEnabled());
            int percobaan = 1;
            while (!target.getImageTransferEnabled()) {
                System.out.println("Image transfer not enabled");
                System.out.println("Enabling image transfer");
                target.setImageTransferEnabled(true);
                if (percobaan <= 3 && !target.getImageTransferEnabled()) {
                    writeObject(target, 5);
                    percobaan++;
                } else {
                    percobaan = 1;
                    break;
                }
                read(target, 5);
            }
            if (target.getImageTransferEnabled()) {
                System.out.println("Image transfer enabled already");
            } else {
                System.out.println("Cannot enabling image transfer");
                return;
            }
            System.out.println("Step 1 : Get Image Block Size");
            read(target, 2);
            System.out.println("Image block size : " + target.getImageBlockSize());
            System.out.println("Step  2 : Initiates image transfer");
            read(target, 6);
            System.out.println("Image transfer status : " + target.getImageTransferStatus());
            while (!target.getImageTransferStatus().equals(ImageTransferStatus.IMAGE_TRANSFER_INITIATED)) {
                System.out.println("Initiating image transfer");
                if (percobaan <= 3 && !target.getImageTransferStatus().equals(
                        ImageTransferStatus.IMAGE_TRANSFER_INITIATED)) {
                    readDataBlock(target.imageTransferInitiate(dlms, "FL163_K20_01", byteFile.length), reply);
//                    readDLMSPacket(target.imageTransferInitiate(dlms, "FL163_K20_01", byteFile.length));
                    percobaan++;
                } else {
                    percobaan = 1;
                    break;
                }
                read(target, 6);
            }
            read(target, 6);
            if (!target.getImageTransferStatus().equals(ImageTransferStatus.IMAGE_TRANSFER_INITIATED)) {
                System.out.println("Initiating image transfer failed");
                return;
            }
            if (target.getImageTransferStatus().equals(ImageTransferStatus.IMAGE_TRANSFER_INITIATED)) {
                System.out.println("Initiating image transfer success");
            }
        } catch (GXDLMSException exception) {
            PrintUtil.printGXDLMSException(exception);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        }
        System.out.println("===========================================================================");
    }

    /**
     * Add DCU TIDE Meter List.
     *
     * @param cUTIDEMeterListObj
     *
     * @exception Error handling.
     */
    public void addDCUTIDEMeterList(DCUTIDEMeterListObj cUTIDEMeterListObj) {
        logger.log(Level.WARNING, "add {0} to Meter LicUTIDEMeterListObjst", cUTIDEMeterListObj.getmSN());
        try {
            GXDLMSData data = new GXDLMSData("0.1.96.1.130.255");
            data.setVersion(0);
            data.setDescription("TIDE Meter List");
            read(data, 2);
            String regex = "\n";
            String[] strings = ((String) data.getValue()).split(regex);
            List<DCUTIDEMeterListObj> list = new ArrayList<>();
            for (String string : strings) {
                String[] ses = string.split(",");
                DCUTIDEMeterListObj obj = new DCUTIDEMeterListObj(ses[0], ses[1], ses[2],
                        Integer.parseInt(ses[3]), Integer.parseInt(ses[4]));
                if (String.format("%10s", cUTIDEMeterListObj.getmSN()).replace(" ", "0").equals(obj.getmSN())) {
                    Logger.getLogger(Reader.class.getName()).log(Level.SEVERE,
                            "add {0} to Meter List gagal : Meter Serial Number sudah ada",
                            new Object[]{cUTIDEMeterListObj.getmSN()});
                    System.out.println("===========================================================================");
                    return;
                }
                list.add(obj);
            }
            list.add(cUTIDEMeterListObj);
            String value = "";
            for (DCUTIDEMeterListObj dCUTIDEMeterListObj : list) {
                value += dCUTIDEMeterListObj.getTipe() + "," + dCUTIDEMeterListObj.getmSN() + "," + 
                        dCUTIDEMeterListObj.getiPAddress() + "," + dCUTIDEMeterListObj.getPort() + "," +
                        dCUTIDEMeterListObj.getPhysicalAddress() + "\n";
            }
            data.setValue(value);
            writeObject(data, 2);
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, "add {0} to Meter List sukses.",
                    new Object[]{cUTIDEMeterListObj.getmSN()});
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            Logger.getLogger(Reader.class.getName()).log(Level.SEVERE, "add {0} to Meter List gagal : {1}",
                    new Object[]{cUTIDEMeterListObj.getmSN(), ex.toString()});
            System.out.println("===========================================================================");
        }
    }
}
