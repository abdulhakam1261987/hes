package gurux.dlms.object.meter;

/**
 * Meter Type Enumeration that used.
 * @author Tab Solutions Team
 */
public enum MeterType {
    /**
     * Other Meter Type
     */
    OTHER("Other"),
    /**
     * SMI 3000
     */
    SMI_1_Phase("SMI 3000"),
    /**
     * EDMI Mk10M
     */
    EDMI_Mk10M("EDMI Mk10M"),
    /**
     * EDMI Mk7MI
     */
    EDMI_Mk7MI("EDMI Mk7MI"),
    /**
     * EDMI Mk7MI
     */
    EDMI_Mk7MI_G2("EDMI Mk7MI Gen 2"),
    /**
     * Wasion Hymeter100
     */
    WASION_Hymeter100("Wasion Hymeter100"),
    /**
     * Wasion iMeter310
     */
    WASION_iMeter310("Wasion iMeter310"),
    /**
     * Wasion iMeter318
     */
    WASION_iMeter318("Wasion iMeter318");
    
    private final String text;

    private MeterType(String text) {
        this.text = text;
        getMappings().put(text, this);
    }
    
    /**
     * Return meter type dalam bentuk String.
     * 
     * @return String data type.
     */
    public String getText() {
        return text;
    }
    
    private static java.util.HashMap<String, MeterType> mappings;

    private static java.util.HashMap<String, MeterType> getMappings() {
        synchronized (MeterType.class) {
            if (mappings == null) {
                mappings = new java.util.HashMap<>();
            }
        }
        return mappings;
    }
    
    /**
     * Return meter type object by a string value.
     * 
     * @param value Value string MeterType.
     * @return MeterType object.
     */
    public static MeterType forValue(final String value) {
        if (getMappings().containsKey(value)) {
            return getMappings().get(value);
        } else {
            return getMappings().get("Other");
        }
    }
}
