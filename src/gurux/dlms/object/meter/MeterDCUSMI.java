/*
 * 
 */
package gurux.dlms.object.meter;

import gurux.common.GXCommon;
import gurux.dlms.GXByteBuffer;
import gurux.dlms.GXStructure;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class MeterDCUSMI {

    private short serialNumber;
    private byte[] logicalDeviceName;
    private byte portNo;
    private byte baudRate;
    private byte commProtocolType;
    private byte dataBitLength;
    private byte startBit;
    private byte stopBit;
    private byte checkBit;
    private byte meterTariff;
    private byte wiringMethod;
    private short commClient;
    private short rs485LogicalAddr;
    private byte[] collectorAddr;
    private byte commEncryption;
    private byte aARQAuthLevel;
    private byte[] lLSKey;
    private byte[] authKey;
    private byte[] broadcastKey;
    private byte[] dedicateKey;
    private byte[] encriptionKey;
    private byte[] masterKey;
    private byte keyMeterFlag;

    public MeterDCUSMI(byte[] logicalDeviceName) {
        this.serialNumber = 1;
        this.logicalDeviceName = logicalDeviceName;
        this.portNo = 31;
        this.baudRate = 5;
        this.commProtocolType = 3;
        this.dataBitLength = 8;
        this.startBit = 0;
        this.stopBit = 1;
        this.checkBit = 0;
        this.meterTariff = 4;
        if (logicalDeviceName[3] == (byte) '3') {
            this.wiringMethod = 2;
        } else {
            this.wiringMethod = 0;
        }
        this.commClient = 0;
        this.rs485LogicalAddr = 0;
        this.collectorAddr = "".getBytes();
        this.commEncryption = 0;
        this.aARQAuthLevel = 0;
        this.lLSKey = "00000000".getBytes();
        this.authKey = "0000000000000000".getBytes();
        this.broadcastKey = "0000000000000000".getBytes();
        this.dedicateKey = "0000000000000000".getBytes();
        this.encriptionKey = "0000000000000000".getBytes();
        this.masterKey = "0000000000000001".getBytes();
        this.keyMeterFlag = 0;
    }

    /**
     *
     * @param logicalDeviceName
     */
    public MeterDCUSMI(String logicalDeviceName) {
        this(logicalDeviceName.getBytes());
    }

    public short getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(short serialNumber) {
        this.serialNumber = serialNumber;
    }

    public byte[] getLogicalDeviceName() {
        return logicalDeviceName;
    }

    public String getLogicalDeviceNameToString() {
        return new String(logicalDeviceName);
    }

    public void setLogicalDeviceName(byte[] logicalDeviceName) {
        this.logicalDeviceName = logicalDeviceName;
    }

    public void setLogicalDeviceName(String logicalDeviceName) {
        this.setLogicalDeviceName(logicalDeviceName.getBytes());
    }

    public byte getPortNo() {
        return portNo;
    }

    public void setPortNo(byte portNo) {
        this.portNo = portNo;
    }

    public byte getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(byte baudRate) {
        this.baudRate = baudRate;
    }

    public byte getCommProtocolType() {
        return commProtocolType;
    }

    public void setCommProtocolType(byte commProtocolType) {
        this.commProtocolType = commProtocolType;
    }

    public byte getDataBitLength() {
        return dataBitLength;
    }

    public void setDataBitLength(byte dataBitLength) {
        this.dataBitLength = dataBitLength;
    }

    public byte getStartBit() {
        return startBit;
    }

    public void setStartBit(byte startBit) {
        this.startBit = startBit;
    }

    public byte getStopBit() {
        return stopBit;
    }

    public void setStopBit(byte stopBit) {
        this.stopBit = stopBit;
    }

    public byte getCheckBit() {
        return checkBit;
    }

    public void setCheckBit(byte checkBit) {
        this.checkBit = checkBit;
    }

    public byte getMeterTariff() {
        return meterTariff;
    }

    public void setMeterTariff(byte meterTariff) {
        this.meterTariff = meterTariff;
    }

    public byte getWiringMethod() {
        return wiringMethod;
    }

    public void setWiringMethod(byte wiringMethod) {
        this.wiringMethod = wiringMethod;
    }

    public short getCommClient() {
        return commClient;
    }

    public void setCommClient(short commClient) {
        this.commClient = commClient;
    }

    public short getRs485LogicalAddr() {
        return rs485LogicalAddr;
    }

    public void setRs485LogicalAddr(short rs485LogicalAddr) {
        this.rs485LogicalAddr = rs485LogicalAddr;
    }

    public byte[] getCollectorAddr() {
        return collectorAddr;
    }

    public void setCollectorAddr(byte[] collectorAddr) {
        this.collectorAddr = collectorAddr;
    }

    public byte getCommEncryption() {
        return commEncryption;
    }

    public void setCommEncryption(byte commEncryption) {
        this.commEncryption = commEncryption;
    }

    public byte getaARQAuthLevel() {
        return aARQAuthLevel;
    }

    public void setaARQAuthLevel(byte aARQAuthLevel) {
        this.aARQAuthLevel = aARQAuthLevel;
    }

    public byte[] getlLSKey() {
        return lLSKey;
    }

    public void setlLSKey(byte[] lLSKey) {
        this.lLSKey = lLSKey;
    }

    public byte[] getAuthKey() {
        return authKey;
    }

    public void setAuthKey(byte[] authKey) {
        this.authKey = authKey;
    }

    public byte[] getBroadcastKey() {
        return broadcastKey;
    }

    public void setBroadcastKey(byte[] broadcastKey) {
        this.broadcastKey = broadcastKey;
    }

    public byte[] getDedicateKey() {
        return dedicateKey;
    }

    public void setDedicateKey(byte[] dedicateKey) {
        this.dedicateKey = dedicateKey;
    }

    public byte[] getEncriptionKey() {
        return encriptionKey;
    }

    public void setEncriptionKey(byte[] encriptionKey) {
        this.encriptionKey = encriptionKey;
    }

    public byte[] getMasterKey() {
        return masterKey;
    }

    public void setMasterKey(byte[] masterKey) {
        this.masterKey = masterKey;
    }

    public byte getKeyMeterFlag() {
        return keyMeterFlag;
    }

    public void setKeyMeterFlag(byte keyMeterFlag) {
        this.keyMeterFlag = keyMeterFlag;
    }

    public static GXStructure[] getStructure(MeterDCUSMI[] meterDCUSMIs) {
        GXStructure[] structures = new GXStructure[meterDCUSMIs.length];
        for (short i = 0; i < meterDCUSMIs.length; i++) {
            MeterDCUSMI meterDCUSMI = meterDCUSMIs[i];
            GXStructure structure = new GXStructure();
            GXByteBuffer buffer = new GXByteBuffer();
            structure.add(meterDCUSMI.getSerialNumber());
            structure.add(meterDCUSMI.getLogicalDeviceName());
            structure.add(meterDCUSMI.getPortNo());
            structure.add(meterDCUSMI.getBaudRate());
            structure.add(meterDCUSMI.getCommProtocolType());
            structure.add(meterDCUSMI.getDataBitLength());
            structure.add(meterDCUSMI.getStartBit());
            structure.add(meterDCUSMI.getStopBit());
            structure.add(meterDCUSMI.getCheckBit());
            structure.add(meterDCUSMI.getMeterTariff());
            structure.add(meterDCUSMI.getWiringMethod());
            structure.add(meterDCUSMI.getCommClient());
            structure.add(meterDCUSMI.getRs485LogicalAddr());
            structure.add(meterDCUSMI.getCollectorAddr());
            structure.add(meterDCUSMI.getCommEncryption());
            structure.add(meterDCUSMI.getaARQAuthLevel());
            structure.add(meterDCUSMI.getlLSKey());
            structure.add(meterDCUSMI.getAuthKey());
            structure.add(meterDCUSMI.getBroadcastKey());
            structure.add(meterDCUSMI.getDedicateKey());
            structure.add(meterDCUSMI.getEncriptionKey());
            structure.add(meterDCUSMI.getMasterKey());
            structure.add(meterDCUSMI.getKeyMeterFlag());
            structures[i] = structure;
        }
        return structures;
    }

    public static byte[][] changSigned816ToUnsigneed(byte[][] bses, MeterDCUSMI[] meterDCUSMIs) {
        GXByteBuffer buffer = new GXByteBuffer();
        for (int i = 0; i < bses.length; i++) {
            byte[] header = new byte[23];
            for (int j = 0; j < bses[i].length; j++) {
                if (j <= 21) {
                    header[j] = bses[i][j];
                } else {
                    break;
                }
            }
            header[22] = (byte) meterDCUSMIs.length;
            buffer.set(header);
            for (MeterDCUSMI meterDCUSMI : meterDCUSMIs) {
                buffer.setUInt8(2);
                buffer.setUInt8(23);
                buffer.setUInt8(18);
                buffer.setUInt16(meterDCUSMI.getSerialNumber());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getLogicalDeviceName().length);
                buffer.set(meterDCUSMI.getLogicalDeviceName());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getPortNo());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getBaudRate());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getCommProtocolType());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getDataBitLength());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getStartBit());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getStopBit());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getCheckBit());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getMeterTariff());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getWiringMethod());
                buffer.setUInt8(18);
                buffer.setUInt16(meterDCUSMI.getCommClient());
                buffer.setUInt8(18);
                buffer.setUInt16(meterDCUSMI.getRs485LogicalAddr());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getCollectorAddr().length);
                buffer.set(meterDCUSMI.getCollectorAddr());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getCommEncryption());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getaARQAuthLevel());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getlLSKey().length);
                buffer.set(meterDCUSMI.getlLSKey());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getAuthKey().length);
                buffer.set(meterDCUSMI.getAuthKey());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getBroadcastKey().length);
                buffer.set(meterDCUSMI.getBroadcastKey());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getDedicateKey().length);
                buffer.set(meterDCUSMI.getDedicateKey());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getEncriptionKey().length);
                buffer.set(meterDCUSMI.getEncriptionKey());
                buffer.setUInt8(9);
                buffer.setUInt8(meterDCUSMI.getMasterKey().length);
                buffer.set(meterDCUSMI.getMasterKey());
                buffer.setUInt8(17);
                buffer.setUInt8(meterDCUSMI.getKeyMeterFlag());
            }
            System.out.println(GXCommon.bytesToHex(buffer.getData()));
            bses[i] = buffer.getData();
        }
        return null;
    }
}
