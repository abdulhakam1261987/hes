package gurux.dlms.object.meter;

import gurux.common.IGXMedia;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.Security;
import gurux.dlms.secure.GXDLMSSecureClient;
import gurux.serial.GXSerial;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class untuk mendefinisikan meter yang digunakan.
 *
 * @author Tab Solutions Team
 */
public final class Meter {

    static String x = String.valueOf((char) 215);
    /**
     * Logger generator.
     */
    private static final Logger LOG = Logger.getLogger(Meter.class.getName());
    /**
     * Meter Type.
     */
    public static MeterType meterType = MeterType.OTHER;
    /**
     * True jika pembacaan menggunakan Logical Name. False jika pembacaan
     * menggunakan Short Name. Default : true.
     */
    public static boolean useLogicalNameReferencing = true;
    /**
     * Jenis authentikasi yang digunakan meter. Available value (None, Low,
     * High, HighMd5, HighSha1, HighGMac, HighSha256). Default : Low.
     *
     * @see Authentication
     */
    public static Authentication authentication = Authentication.LOW;
    /**
     * Interface Type yang digunakan meter. Available value (HDLC, WRAPPER, PDU,
     * WIRELESS_MBUS). Default : HDLC.
     *
     * @see InterfaceType
     */
    public static InterfaceType interfaceType = InterfaceType.HDLC;
    /**
     * Meter client address. Default : 0.
     */
    public static int clientAddress = 0;
    /**
     * Meter logical address. Default : 0.
     */
    public static int logicalAddress = 0;
    /**
     * Meter physical address. Default : 0.
     */
    public static int physicalAddress = 0;
    /**
     * Meter Address Size. Default : 0.
     */
    public static int addressSize = 0;
    /**
     * Password meter. Digunakan jika meter menggunakan Low Authentication.
     * Default : null.
     */
    public static String password = "";
    /**
     * Meter security. Available value (NONE, AUTHENTICATION, ENCRYPTION,
     * AUTHENTICATION_ENCRYPTION). Default : NONE.
     *
     * @see Security
     */
    public static Security security = Security.NONE;
    /**
     * Meter system title. Default : null.
     */
    public static String systemTitle = "";
    /**
     * Meter block cipher key. Default : null.
     */
    public static String blockCipherKey = "";
    /**
     * Meter authentication key. Default : null.
     */
    public static String authenticationKey = "";
    /**
     * Meter invocation counter. Default : 0.
     */
    public static int invocation_counter = 0;
    /**
     * Media yang digunakan.
     */
    public static IGXMedia media = null;
    /**
     * True jika pembacaan dilakukan melalui gateway.
     */
    public static boolean useGateway = false;
    /**
     * Nomor modem yang digunakan meter. Digunakan jika pembacaan melalui
     * gateway. Default = 000000000000.
     */
    public static String noModem = "000000000000";
    /**
     * Hostname/IP address meter. Default : 127.0.0.1.
     */
    public static String hostname = "127.0.0.1";
    /**
     * Port meter. Default : 0
     */
    public static int port = 0;
    /**
     * True jika ingin mencetak log packet. Default : false.
     */
    public static boolean traceReadingLog = false;
    /**
     * Waktu delay jika gagal mengirim pesan ke meter. Default : 15000 (15 s).
     */
    public static int waitTime = 15000;
    /**
     * True jika menggunakan serial.
     */
    public static boolean isIEC = false;
    /**
     * Serial port.
     */
    public static String serialPort = "COM5";
    /**
     * Meter manufacture.
     */
    private static String manufacture = "Unregistered Manufacture";
    /**
     * Meter type.
     */
    private static String type = "Unregistered Type";

    /**
     * Set meter yang akan digunakan.
     *
     * @param meterType Enumeration meter yang dapat dipilih.
     * @see MeterType
     */
    public static void setMeter(MeterType meterType) {
        Meter.meterType = meterType;
        switch (Meter.meterType) {
            case SMI_1_Phase:
                authentication = Authentication.HIGH_GMAC;
                interfaceType = InterfaceType.WRAPPER;
                clientAddress = 1;
                logicalAddress = 0;
                physicalAddress = 1;
                //Use for HighGMac
                security = Security.AUTHENTICATION_ENCRYPTION;
                systemTitle = "00000001";
                authenticationKey = "0000000000000000";
                blockCipherKey = "0000000000000000";
                //Set 0 if have no set for invocation counter
                invocation_counter = 1;
                //Communication
                hostname = "182.0.22.133";
                port = 3389;
                noModem = "";
                //Manufacture Info
                manufacture = "PT. SMART METER INDONESIA";
                type = "SMI-3000";
                break;
            case EDMI_Mk7MI:
            case EDMI_Mk7MI_G2:
                authentication = Authentication.HIGH_GMAC;
                interfaceType = InterfaceType.HDLC;
                clientAddress = 4;
                logicalAddress = 1;
                physicalAddress = 35;
                addressSize = 4;
                //Use for HighGMac
                security = Security.AUTHENTICATION_ENCRYPTION;
                systemTitle = "EDMMk7MI";
                authenticationKey = "FFFFFFFFFFFFFFFF";
                blockCipherKey = "FFFFFFFFFFFFFFFF";
                //Communication
                hostname = "118.97.223.230";
                if (Meter.meterType.equals(MeterType.EDMI_Mk7MI)) {
                    port = 50038;
                } else {
                    port = 50039;
                }
                //Manufacture Info
                manufacture = "PT. EDMI MANUFACTURING INDONESIA";
                type = "Mk7MI";
                break;
            case EDMI_Mk10M:
                authentication = Authentication.HIGH_GMAC;
                interfaceType = InterfaceType.HDLC;
                clientAddress = 1;
                logicalAddress = 1;
                physicalAddress = 17;
                addressSize = 4;
                //Use for HighGMac
                security = Security.AUTHENTICATION_ENCRYPTION;
                systemTitle = "EDMMk10M";
                authenticationKey = "FFFFFFFFFFFFFFFF";
                blockCipherKey = "FFFFFFFFFFFFFFFF";
                //Communication
                hostname = "118.97.223.230";
                port = 50036;
                //Manufacture Info
                manufacture = "PT. EDMI MANUFACTURING INDONESIA";
                type = "Mk10M";
                break;
            case WASION_iMeter318:
                authentication = Authentication.LOW;
                interfaceType = InterfaceType.HDLC;
                clientAddress = 1;
                logicalAddress = 1;
                physicalAddress = 18;
                password = "22222222";
                //Use for HighGMac
                security = Security.NONE;
                //Communication
                hostname = "10.10.40.22";
                port = 100;
                //Manufacture Info
                manufacture = "PT. ELECTRA INTI PERKASA";
                type = "iMeter318";
                break;
            case WASION_Hymeter100:
                authentication = Authentication.LOW;
                interfaceType = InterfaceType.HDLC;
                clientAddress = 1;
                logicalAddress = 0;
                physicalAddress = 1;
                password = "00000000";
                security = Security.NONE;
                hostname = "10.10.40.25";
                port = 100;
                manufacture = "PT. ELECTRA INTI PERKASA";
                type = "Hymeter100";
                break;
            case WASION_iMeter310:
                authentication = Authentication.LOW;
                interfaceType = InterfaceType.HDLC;
                clientAddress = 1;
                logicalAddress = 1;
                physicalAddress = 18;
                password = "11111111";
                //Use for HighGMac
                security = Security.NONE;
                //Communication
                hostname = "10.10.40.30";
                port = 100;
                //Manufacture Info
                manufacture = "PT. ELECTRA INTI PERKASA";
                type = "iMeter310";
                break;
            default:
                Meter.meterType = MeterType.OTHER;
                useLogicalNameReferencing = true;
                authentication = Authentication.NONE;
                interfaceType = InterfaceType.HDLC;
                clientAddress = 16;
                logicalAddress = 0;
                physicalAddress = 0;
                password = "";
                security = Security.NONE;
                systemTitle = "";
                blockCipherKey = "";
                authenticationKey = "";
                invocation_counter = 0;
                useGateway = false;
                noModem = "000000000000";
                hostname = "127.0.0.1";
                port = 10100;
                manufacture = "Unregistered Manufacture";
                type = "Unregistered Type";
                traceReadingLog = false;
                waitTime = 15000;
                isIEC = false;
                break;
        }
        LOG.log(Level.INFO, "Set Meter to {0} from {1}", new Object[]{type, manufacture});
    }

    /**
     * Get server address meter. Nilai merupakan kombinasi dari logical address
     * di high byte dan physical address di low bytenya.
     *
     * @return Server address dalam bentuk bilangan cacah (Integer)
     * @see GXDLMSSecureClient
     */
    public static int getServerAddress() {
        if (meterType.equals(MeterType.OTHER)) {
            return 1;
        }
        if (Meter.addressSize == 0) {
            return GXDLMSSecureClient.getServerAddress(Meter.logicalAddress, Meter.physicalAddress);
        } else {
            return GXDLMSSecureClient.getServerAddress(Meter.logicalAddress,
                    Meter.physicalAddress, Meter.addressSize);
        }

    }

    /**
     * Mengatur penggunan gateway pada komunikasi.
     *
     * @param useGateway True jika ingin menggunakan gateway
     */
    public static void setUseGateway(boolean useGateway) {
        Meter.useGateway = useGateway;
        noModem = "000000000000";
    }

    /**
     * Get meter manufacture. Value dihardcode.
     *
     * @return Meter manufacture as String.
     */
    public static String getManufacture() {
        return manufacture;
    }

    /**
     * Get meter type. Value dihardcode.
     *
     * @return Meter type as String.
     */
    public static String getType() {
        return type;
    }

    /**
     * Getting logger
     *
     * @return Instance Logger class for Meter class
     * @see Logger
     */
    static Logger getLOG() {
        return LOG;
    }

    /**
     * Cetak informasi meter
     *
     * @return Meter informasi dalam bentuk String.
     */
    public static String tostring() {
        String rslt = "==========================================================================="
                + "\n" + "Manufacture = " + manufacture
                + "\n" + "Type = " + type
                + "\n" + "---------------------------------------------------------------------------"
                + "\n\t" + "Use logical name referencing = " + useLogicalNameReferencing
                + "\n\t" + "Authentication = " + authentication
                + "\n\t" + "Client address = " + clientAddress + " (0" + x
                + Integer.toHexString(clientAddress).toUpperCase() + ")"
                + "\n\t" + "Interface type = " + interfaceType
                + "\n\t" + "Physical address = " + physicalAddress + " (0" + x
                + Integer.toHexString(physicalAddress).toUpperCase() + ")";
        if (!interfaceType.equals(InterfaceType.WRAPPER)) {
            rslt += "\n\t" + "Logical address = " + logicalAddress + " (0" + x
                    + Integer.toHexString(logicalAddress).toUpperCase() + ")";
        }
        rslt += "\n\t" + "Server address = " + Meter.getServerAddress() + " (0" + x
                + Integer.toHexString(Meter.getServerAddress()).toUpperCase() + ")";
        if (addressSize != 0) {
            rslt += "\n\t" + "Address size = " + addressSize + " bytes";
        }
        if (authentication.equals(Authentication.LOW)) {
            rslt += "\n\t" + "Password = " + password;
        }
        if (authentication.equals(Authentication.HIGH_GMAC)) {
            rslt += "\n\t" + "Security = " + security;
            rslt += "\n\t" + "System title = " + systemTitle;
            rslt += "\n\t" + "Block cipher key = " + blockCipherKey;
            rslt += "\n\t" + "Authentication key = " + authenticationKey;
            if (invocation_counter != 0) {
                rslt += "\n\t" + "Invocation counter = " + invocation_counter;
            }
        }
        rslt += "\n" + "---------------------------------------------------------------------------";
        if (isIEC) {
            rslt += "\n" + "Port = " + serialPort;
            GXSerial serial = (GXSerial) Meter.media;
//            rslt += "\n" + "Baudrate = " + serial.getBaudRate();
//            rslt += "\n" + "Data Bits = " + serial.getDataBits();
//            rslt += "\n" + "Parity = " + serial.getParity().toString();
//            rslt += "\n" + "Stop Bits = " + serial.getStopBits();
        } else {
            if (useGateway) {
                rslt += "\n" + "Using Gateway " + useGateway;
                rslt += "\n" + "Nomor Modem = " + noModem;
                rslt += "\n" + "Gateway Hostname = " + hostname;
            } else {
                rslt += "\n" + "Hostname = " + hostname;
            }
            rslt += "\n" + "Port = " + port;
        }
        rslt += "\n===========================================================================";
        return rslt;
    }

    /**
     * Get GXDLMSSecureClient. Object untuk pembentukan pesan komunikasi.
     *
     * @return GXDLMSSecureClient
     * @see GXDLMSSecureClient
     */
    public static GXDLMSSecureClient getClient() {
        try {
            GXDLMSSecureClient client = new GXDLMSSecureClient(true);
//            if (meterType.equals(MeterType.OTHER)) {
//                return client;
//            }
            client.setUseLogicalNameReferencing(useLogicalNameReferencing);
            client.setAuthentication(authentication);
            client.setClientAddress(clientAddress);
            client.setInterfaceType(interfaceType);
            client.setServerAddress(getServerAddress());
            client.getLimits().setMaxInfoRX(256);
            client.getLimits().setMaxInfoTX(256);
            if (client.getAuthentication().equals(Authentication.HIGH_GMAC)) {
                client.getCiphering().setSecurity(security);
                client.getCiphering().setAuthenticationKey(authenticationKey.getBytes("ASCII"));
                client.getCiphering().setBlockCipherKey(blockCipherKey.getBytes("ASCII"));
                client.getCiphering().setSystemTitle(systemTitle.getBytes("ASCII"));
                if (invocation_counter != 0) {
                    client.getCiphering().setInvocationCounter(invocation_counter);
                }
            } else {
                client.setPassword(password);
            }
            return client;
        } catch (UnsupportedEncodingException ex) {
            getLOG().log(Level.SEVERE, ex.toString());
            return null;
        }
    }

    /**
     * Cek apakah ini membaca Load Profile Mk7MI?
     *
     * @param logicalName Logical name obis yg sedang dibaca
     *
     * @return true jika ini meter EDMI Mk7MI dan Logical Name-nya
     * 1.0.99.1.0.255
     */
    public static boolean isLoadProfileEDMIMk7MI(String logicalName) {
        return systemTitle.equals("EDMMk7MI") && logicalName.equals("1.0.99.1.0.255");
    }
}
