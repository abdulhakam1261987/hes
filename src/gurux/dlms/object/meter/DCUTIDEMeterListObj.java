/*
 * 
 */

package gurux.dlms.object.meter;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class DCUTIDEMeterListObj {
    private String tipe;
    private String mSN;
    private String iPAddress;
    private int port;
    private int physicalAddress;

    public DCUTIDEMeterListObj(String tipe, String mSN, String iPAddress, int port) {
        this.tipe = tipe;
        this.mSN = String.format("%10s", mSN).replace(" ", "0");
        this.iPAddress = iPAddress;
        this.port = port;
        this.physicalAddress = 35;
    }

    public DCUTIDEMeterListObj(String tipe, String mSN, String iPAddress, int port, int physicalAddress) {
        this.tipe = tipe;
        this.mSN = String.format("%10s", mSN).replace(" ", "0");
        this.iPAddress = iPAddress;
        this.port = port;
        this.physicalAddress = physicalAddress;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

    public String getmSN() {
        return mSN;
    }

    public void setmSN(String mSN) {
            this.mSN = String.format("%10s", mSN).replace(" ", "0");
    }

    public String getiPAddress() {
        return iPAddress;
    }

    public void setiPAddress(String iPAddress) {
        this.iPAddress = iPAddress;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(int physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    @Override
    public String toString() {
        return "DCUTIDEMeterListObj{\n" + 
                "\tTipe = " + tipe + "\n\tMeter Serial Number = " + mSN + "\n\tIP Address = " + iPAddress + 
                "\n\tPort = " + port + "\n\tPhysical Address = " + physicalAddress + "\n}";
    }
    
}
