package gurux.dlms.client.Global;

import gurux.dlms.objects.GXDLMSObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class untuk menampung variabel global yang digunakan.
 *
 * @author Tab Solutions team
 */
public class GlobalVar {

    /**
     * Daftar OBIS.
     */
    private static HashMap<String, Obis> obisList = new HashMap<>();
    /**
     * Logger
     */
    private static final Logger LOG = Logger.getLogger(GlobalVar.class.getName());

    /**
     * Getting Logger
     *
     * @return Logger object.
     * @see Logger
     */
    public static Logger getLOG() {
        return LOG;
    }

    /**
     * Get daftar OBIS.
     *
     * @return Daftar OBIS.
     */
    public static HashMap<String, Obis> getObisList() {
        return obisList;
    }

    /**
     * set daftar OBIS.
     *
     * @param obisList Hashmap OBIS.
     */
    public static void setObisList(HashMap<String, Obis> obisList) {
        GlobalVar.obisList = obisList;
    }

    /**
     * Get Obis object type by logical name.
     *
     * @param logicalName Logical name dari Obis object type.
     * @return Obis object type.
     */
    public static Obis getObisByLn(String logicalName) {
        return obisList.get(logicalName);
    }

    /**
     * Melakukan pencarian di daftar OBIS.
     *
     * @param logicalName Logical name yang akan dicari.
     * @return true jika Obis object type yang terdaftar ada yang memiliki
     * logical name sesuai dengan param.
     */
    public static boolean isObisAvailable(String logicalName) {
        return obisList.containsKey(logicalName);
    }

    /**
     * Get GXDLMSObject object type by logical name.
     *
     * @param logicalName Logical name dari OBIS.
     * @return GXDLMSObject object type.
     */
    public static GXDLMSObject getObjectByLogicalName(String logicalName) {
        return obisList.get(logicalName).getObject();
    }

    /**
     * Mencetak Daftar Obis tanpa berurutan
     */
    public static void cetakDaftarObis() {
        GlobalVar.cetakDaftarObis(false);
    }

    /**
     * Mencetak Daftar Obis
     *
     * @param urut pilihan untuk mengurutkan obis dari angka terkecil di
     * masing-masing value group.
     */
    public static void cetakDaftarObis(boolean urut) {
        if (urut) {
            List<Obis> list = new ArrayList<>();
            obisList.keySet().forEach((i) -> {
                list.add(new Obis(obisList.get(i).getObject()));
            });
            for (int i = 0; i < 6; i++) {
                for (int j = 0; j < list.size(); j++) {
                    for (int k = list.size() - 1; k > j; k--) {
                        switch (i) {
                            case 0:
                                if (list.get(k - 1).getA() > list.get(k).getA()) {
                                    Obis tmp = list.get(k - 1);
                                    list.set(k - 1, list.get(k));
                                    list.set(k, tmp);
                                }
                                break;
                            case 1:
                                if (list.get(k - 1).getA() == list.get(k).getA()) {
                                    if (list.get(k - 1).getB() > list.get(k).getB()) {
                                        Obis tmp = list.get(k - 1);
                                        list.set(k - 1, list.get(k));
                                        list.set(k, tmp);
                                    }
                                }
                                break;
                            case 2:
                                if (list.get(k - 1).getA() == list.get(k).getA()) {
                                    if (list.get(k - 1).getB() == list.get(k).getB()) {
                                        if (list.get(k - 1).getC() > list.get(k).getC()) {
                                            Obis tmp = list.get(k - 1);
                                            list.set(k - 1, list.get(k));
                                            list.set(k, tmp);
                                        }
                                    }
                                }
                                break;
                            case 3:
                                if (list.get(k - 1).getA() == list.get(k).getA()) {
                                    if (list.get(k - 1).getB() == list.get(k).getB()) {
                                        if (list.get(k - 1).getC() == list.get(k).getC()) {
                                            if (list.get(k - 1).getD() > list.get(k).getD()) {
                                                Obis tmp = list.get(k - 1);
                                                list.set(k - 1, list.get(k));
                                                list.set(k, tmp);
                                            }
                                        }
                                    }
                                }
                                break;
                            case 4:
                                if (list.get(k - 1).getA() == list.get(k).getA()) {
                                    if (list.get(k - 1).getB() == list.get(k).getB()) {
                                        if (list.get(k - 1).getC() == list.get(k).getC()) {
                                            if (list.get(k - 1).getD() == list.get(k).getD()) {
                                                if (list.get(k - 1).getE() > list.get(k).getE()) {
                                                    Obis tmp = list.get(k - 1);
                                                    list.set(k - 1, list.get(k));
                                                    list.set(k, tmp);
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            case 5:
                                if (list.get(k - 1).getA() == list.get(k).getA()) {
                                    if (list.get(k - 1).getB() == list.get(k).getB()) {
                                        if (list.get(k - 1).getC() == list.get(k).getC()) {
                                            if (list.get(k - 1).getD() == list.get(k).getD()) {
                                                if (list.get(k - 1).getE() == list.get(k).getE()) {
                                                    if (list.get(k - 1).getF() > list.get(k).getF()) {
                                                        Obis tmp = list.get(k - 1);
                                                        list.set(k - 1, list.get(k));
                                                        list.set(k, tmp);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            System.out.println("N0\tLogical Name" + "\tA\tB\tC\tD\tE\tF\t"
                    + "Description\tDeskripsi\tField name" + "\t" + "Class ID\t"
                    + "Version\t" + "Data Type");
            for (int i = 0; i < list.size(); i++) {
                System.out.println((i + 1) + "\t"
                        + list.get(i).getLogicalName()
                        + "\t" + list.get(i).getA()
                        + "\t" + list.get(i).getB()
                        + "\t" + list.get(i).getC()
                        + "\t" + list.get(i).getD()
                        + "\t" + list.get(i).getE()
                        + "\t" + list.get(i).getF()
                        + "\t" + list.get(i).getObject().getDescription()
                        + "\t" + list.get(i).getDeskripsi()
                        + "\t" + list.get(i).getFieldName()
                        + "\t" + list.get(i).getObject().getObjectType().getValue()
                        + "\t" + list.get(i).getObject().getVersion()
                        + "\t" + list.get(i).getObject().getObjectType());
            }
        } else {
            System.out.println("N0\tLogical Name" + "\tA\tB\tC\tD\tE\tF\t"
                    + "Description\tDeskripsi\tField name" + "\t" + "Class ID\t"
                    + "Version\t" + "Data Type");
            int counter = 0;
            for (Obis value : obisList.values()) {
                counter++;
                System.out.println((counter) + "\t"
                        + value.getLogicalName()
                        + "\t" + value.getA()
                        + "\t" + value.getB()
                        + "\t" + value.getC()
                        + "\t" + value.getD()
                        + "\t" + value.getE()
                        + "\t" + value.getF()
                        + "\t" + value.getObject().getDescription()
                        + "\t" + value.getDeskripsi()
                        + "\t" + value.getFieldName()
                        + "\t" + value.getObject().getObjectType().getValue()
                        + "\t" + value.getObject().getVersion()
                        + "\t" + value.getObject().getObjectType());
            }
        }
    }
}
