/*
 * 
 */
package gurux.dlms.client.Global.util;

import java.util.Calendar;

/**
 * Class untuk penggunaan java.util.Calendar
 *
 * @author Tab Solutions
 */
public class Kalendar {

    /**
     * get the yesterday date and time at the beginning. Value adalah hari
     * kemarin.
     *
     * @return Kemarin dalam bentuk Calendar object.
     */
    public static Calendar getYesterdaysDate() {
        Calendar start = Calendar.getInstance();
        start.add(java.util.Calendar.DATE, -1);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        return start;
    }

    /**
     * Get a month ago date. Value adalah tanggal yang sama bulan yang lalu.
     *
     * @return Tanggal yang sama bulan yang lalu dalam bentuk Calendar object.
     */
    public static Calendar getAMonthAgoDate() {
        Calendar start = Calendar.getInstance();
        start.add(java.util.Calendar.MONTH, -1);
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        return start;
    }

    /**
     * get the today date and time at the beginning. Value adalah awal mula hari
     * ini.
     *
     * @return Awal mula hari ini dalam bentuk Calendar object.
     */
    public static Calendar getStartDay() {
        Calendar start = Calendar.getInstance();
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 1);
        return start;
    }

    /**
     * get the end of today. Value adalah hari ini jam 23:59:59.
     *
     * @return Tanggal hari ini jam 23:59:59 dalam bentuk Calendar object.
     */
    public static Calendar getTheEndOfToday() {
        Calendar end = Calendar.getInstance();
        end.set(java.util.Calendar.HOUR_OF_DAY, 23);
        end.set(java.util.Calendar.MINUTE, 59);
        end.set(java.util.Calendar.SECOND, 59);
        return end;
    }

    /**
     * get the start of this month. Value adalah awal bulan jam 00:00:01.
     *
     * @return Tanggal awal bulan ini jam 00:00:01 dalam bentuk Calendar object.
     */
    public static Calendar getTheStartOfThisMonth() {
        return getTheStartOfTheMonth(Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.YEAR));
    }

    /**
     * get the start of the month. Value adalah awal bulan yang ditentukan jam
     * 00:00:01.
     *
     * @param month Bulan dalam bentuk integer, dimulai dari 0 (Januari), 1
     * (February), ...., 11 (Desember). Selain itu dihitung sebagai bulan ini.
     * @param year Tahun.
     * @return Tanggal awal bulan yang ditentukan jam 00:00:01 dalam bentuk
     * Calendar object.
     */
    public static Calendar getTheStartOfTheMonth(int month, int year) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.DATE, 1);
        if (month > 11 || month < 0) {
            c.set(Calendar.MONTH, c.get(Calendar.MONTH));
        } else {
            c.set(Calendar.MONTH, month);
        }
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 1);
        return c;
    }

    /**
     * get the end of this month. Value adalah akhir bulan jam 23:59:59.
     *
     * @return Tanggal akhir bulan ini jam 23:59:59 dalam bentuk Calendar
     * object.
     */
    public static Calendar getTheEndOfThisMonth() {
        return getTheEndOfTheMonth(Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.YEAR));
    }

    /**
     * get the end of the month. Value adalah akhir bulan yang ditentukan jam
     * 23:59:59.
     *
     * @param month Bulan dalam bentuk integer, dimulai dari 0 (Januari), 1
     * (February), ...., 11 (Desember). Selain itu dihitung sebagai bulan ini.
     * @param year Tahun.
     * @return Tanggal akhir bulan yang ditentukan jam 23:59:59 dalam bentuk
     * Calendar object.
     */
    public static Calendar getTheEndOfTheMonth(int month, int year) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        if (month > 11 || month < 0) {
            c.set(Calendar.MONTH, c.get(Calendar.MONTH));
        } else {
            c.set(Calendar.MONTH, month);
        }
        c.set(Calendar.DATE, c.getActualMaximum(Calendar.DATE));
        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        return c;
    }

    /**
     * get the start of this year. Value adalah awal tahun ini jam 00:00:01.
     *
     * @return Tanggal awal tahun ini jam 00:00:01 dalam bentuk Calendar object.
     */
    public static Calendar getTheStartOfThisYear() {
        return getTheStartOfTheMonth(0, Calendar.getInstance().get(Calendar.YEAR));
    }
}
