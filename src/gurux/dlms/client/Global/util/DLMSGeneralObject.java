/*
 * 
 */
package gurux.dlms.client.Global.util;

/**
 * Define general object (A is 0) on DLMS
 *
 * @author Tab Solutions
 */
class DLMSGeneralObject {

    private static int a;
    private static int b;
    private static int c;
    private static int d;
    private static int e;
    private static int f;

    public static String get(int aValue, int bValue, int cValue, int dValue, int eValue, int fValue) {
        a = aValue;
        b = bValue;
        c = cValue;
        d = dValue;
        e = eValue;
        f = fValue;
        String rslt = "";
        if ((c >= 3 && c <= 9) || (c == 32) || (c >= 34 && c <= 39) || (c >= 45 && c <= 64)
                || (c >= 67 && c <= 92) || (c == 95) || (c >= 100 && c <= 127)) {
            return "C-Reserved (Ref: 6.2.1)";
        }
        switch (c) {
            case 0:
                if (d == 1 && e == 0 && ((f >= 0 && f <= 99) || f == 255)) {
                    if (f == 255) {
                        rslt = "Billing period counter(1)";
                    } else {
                        rslt = "Billing period counter(1) " + f;
                    }
                } else if (d == 1 && e == 1 && f == 255) {
                    rslt = "No. of available billing periods(1)";
                } else if (d == 1 && e == 2 && f >= 0 && f <= 99) {
                    rslt = "Time stamp of the billing period(1) " + f;
                } else if (d == 1 && e == 2 && f == 255) {
                    rslt = "Time stamp of the most recent billing period(1) closed";
                } else if (d == 1 && e == 3 && ((f >= 0 && f <= 99) || f == 255)) {
                    rslt = "Billing period counter(2), VZ " + f;
                } else if (d == 1 && e == 4 && f == 255) {
                    rslt = "No. of available billing periods(2)";
                } else if (d == 1 && e == 5 && f >= 0 && f <= 99) {
                    rslt = "Time stamp of the billing period(2) " + f;
                } else if (d == 1 && e == 5 && f == 255) {
                    rslt = "Time stamp of the most recent billing period(2) closed";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "Firmware identifier";
                } else if (d == 2 && e == 1 && f == 255) {
                    rslt = "Versi firmware";
                } else if (d == 2 && e == 8 && f == 255) {
                    rslt = "Firmware signature";
                } else if (d == 9 && e == 1 && f == 255) {
                    rslt = "Local time";
                } else if (d == 9 && e == 2 && f == 255) {
                    rslt = "Local date";
                } else {
                    return "Reserved (Ref: 7.4.1)";
                }
                break;
            case 1:
                switch (d) {
                    case 0:
                        rslt += "Clock";
                        break;
                    case 1:
                        rslt += "Unix clock";
                        break;
                    case 2:
                        rslt += "High resolution clock";
                        break;
                }
                if (d == 0 || d == 1 || d == 2) {
                    if (e != 0) {
                        rslt += " #" + (e + 1) + " ";
                    }
                } else {
                    return "Reserved (Ref: 6.2.5)";
                }
                return rslt;
            case 2:
                if (e == 0 && f == 255) {
                    switch (d) {
                        case 0:
                            rslt = "Modem configuration";
                            break;
                        case 1:
                            rslt = "Auto connect";
                            break;
                        case 2:
                            rslt = "Auto answer";
                            break;
                        default:
                            return "Reserved (Ref: 6.2.6)";
                    }
                } else {
                    return "Reserved (Ref: 6.2.6)";
                }
                break;
            case 10:
                if (d >= 0 && d <= 127 && f == 255) {
                    switch (e) {
                        case 0:
                            if (d == 0) {
                                rslt = "Global meter reset";
                            } else {
                                rslt = "Global meter reset implementation specific " + (d + 1);
                            }
                            break;
                        case 1:
                            if (d == 0) {
                                rslt = "MDI reset / End of billing period";
                            } else {
                                rslt = "MDI reset / End of billing period implementation specific " + (d + 1);
                            }
                            break;
                        case 100:
                            if (d == 0) {
                                rslt = "Tariffication script table";
                            } else {
                                rslt = "Tariffication script table implementation specific " + (d + 1);
                            }
                            break;
                        case 101:
                            if (d == 0) {
                                rslt = "Activate test mode";
                            } else {
                                rslt = "Activate test mode implementation specific " + (d + 1);
                            }
                            break;
                        case 102:
                            if (d == 0) {
                                rslt = "Activate normal mode";
                            } else {
                                rslt = "Activate normal mode implementation specific " + (d + 1);
                            }
                            break;
                        case 103:
                            if (d == 0) {
                                rslt = "Set output signals";
                            } else {
                                rslt = "Set output signals implementation specific " + (d + 1);
                            }
                            break;
                        case 104:
                            if (d == 0) {
                                rslt = "Switch optical test output";
                            } else {
                                rslt = "Switch optical test output implementation specific " + (d + 1);
                            }
                            break;
                        case 105:
                            if (d == 0) {
                                rslt = "Power quality measurement management";
                            } else {
                                rslt = "Power quality measurement management implementation specific " + (d + 1);
                            }
                            break;
                        case 106:
                            if (d == 0) {
                                rslt = "Disconnect control script table";
                            } else {
                                rslt = "Disconnect control script table " + (d + 1);
                            }
                            break;
                        case 107:
                            if (d == 0) {
                                rslt = "Image activation script table";
                            } else {
                                rslt = "Image activation script table implementation specific " + (d + 1);
                            }
                            break;
                        case 108:
                            if (d == 0) {
                                rslt = "Push script table";
                            } else {
                                rslt = "Push script table implementation specific " + (d + 1);
                            }
                            break;
                        case 125:
                            if (d == 0) {
                                rslt = "Broadcast script table";
                            } else {
                                rslt = "Broadcast script table implementation specific " + (d + 1);
                            }
                            break;
                        default:
                            return "Reserved (Ref: 6.2.7)";

                    }
                } else {
                    return "Reserved (Ref: 6.2.7)";
                }
                break;
            case 11:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Special days table";
                    } else {
                        rslt = "Special days table " + (e + 1);
                    }
                } else {
                    return "Reserved (Ref: 6.2.8)";
                }
                break;
            case 12:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Schedule";
                    } else {
                        rslt = "Schedule " + (e + 1);
                    }
                } else {
                    return "Reserved (Ref: 6.2.9)";
                }
                break;
            case 13:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Activity calendar";
                    } else {
                        rslt = "Activity calendar " + (e + 1);
                    }
                } else {
                    return "Reserved (Ref: 6.2.10)";
                }
                break;
            case 14:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Register activation";
                    } else {
                        rslt = "Register activation " + (e + 1);
                    }
                } else {
                    return "Reserved (Ref: 6.2.11)";
                }
                break;
            case 15:
                if (d == 0 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "End of billing period";
                            break;
                        case 1:
                            rslt = " Disconnect control action schedule";
                            break;
                        case 2:
                            rslt = "Image activation";
                            break;
                        case 3:
                            rslt = "Output control";
                            break;
                        case 4:
                            rslt = "Push";
                            break;
                        case 5:
                            rslt = "Load profile control";
                            break;
                        case 6:
                            rslt = "M-Bus control";
                            break;
                        case 7:
                            rslt = "Function control";
                            break;
                        default:
                            return "Reserved (Ref: 6.2.12)";
                    }
                } else if (d >= 1 && d <= 127 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "End of billing period, implementation specific " + (d + 1) + " " + (e + 1);
                } else {
                    return "Reserved (Ref: 6.2.12)";
                }
                break;
            case 16:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Register monitor";
                    } else {
                        rslt = "Register monitor " + (e + 1);
                    }
                } else if (d == 1 && f == 255) {
                    if (e >= 0 && e <= 9) {
                        rslt = "Alarm monitor " + (e + 1);
                    } else {
                        return "Reserved (Ref: 6.2.13)";
                    }
                } else if (d == 2 && f == 255) {
                    if (e == 0) {
                        rslt = "Parameter monitor";
                    } else {
                        rslt = "Parameter monitor " + (e + 1);
                    }
                } else {
                    if (d == 2) {
                        return "Reserved (Ref: 6.2.14)";
                    } else {
                        return "Reserved (Ref: 6.2.13)";
                    }
                }
                break;
            case 17:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Limiter";
                    } else {
                        rslt = "Limiter " + (e + 1);
                    }
                } else {
                    return "Reserved (Ref: 6.2.15)";
                }
                break;
            case 18:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Array manager";
                    } else {
                        rslt = "Array manager " + (e);
                    }
                } else {
                    return "Reserved (Ref: 6.2.16)";
                }
                break;
            case 19:
                if (d >= 0 && d <= 9 && e == 0 && f == 255) {
                    rslt = "Account";
                } else if (d >= 10 && d <= 19 && f == 255) {
                    if (e == 0) {
                        rslt = "Credit " + (d - 10);
                    } else {
                        rslt = "Credit " + (d - 10) + " " + (e);
                    }
                } else if (d >= 20 && d <= 29 && f == 255) {
                    if (e == 0) {
                        rslt = "Charge " + (d - 20);
                    } else {
                        rslt = "Charge " + (d - 20) + " " + (e);
                    }
                } else if (d >= 40 && d <= 49 && f == 255) {
                    if (e == 0) {
                        rslt = "Token gateway " + (d - 40);
                    } else {
                        rslt = "Token gateway " + (d - 40) + " " + (e);
                    }
                } else if (d >= 50 && d <= 59 && f == 255 && (e == 1 || e == 2)) {
                    if (e == 1) {
                        rslt = "Max credit limit " + (d - 50);
                    } else {
                        rslt = "Max vend limit " + (d - 50);
                    }
                } else {
                    return "Reserved (Ref: 6.2.17)";
                }
                break;
            case 20:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "IEC optical port setup";
                } else if (d == 0 && e == 1 && f == 255) {
                    rslt = "electrical port setup";
                } else {
                    return "Reserved (Ref: 6.2.18)";
                }
                break;
            case 21:
                if (d == 0 && d == 255) {
                    switch (e) {
                        case 0:
                            rslt = "(General local port readout if has PROFILE GENERIC object type "
                                    + "or Standard readout parametrization "
                                    + (e + 1) + " if has DATA object type) ";
                            break;
                        case 1:
                            rslt = "(General display readout if has PROFILE GENERIC object type "
                                    + "or Standard readout parametrization "
                                    + (e + 1) + " if has DATA object type) ";
                            break;
                        case 2:
                            rslt = "(Alternate display readout if has PROFILE GENERIC object type "
                                    + "or Standard readout parametrization "
                                    + (e + 1) + " if has DATA object type) ";
                            break;
                        case 3:
                            rslt = "(Service display readout if has PROFILE GENERIC object type "
                                    + "or Standard readout parametrization "
                                    + (e + 1) + " if has DATA object type) ";
                            break;
                        case 4:
                            rslt = "(List of configurable meter data if has PROFILE GENERIC object type "
                                    + "or Standard readout parametrization "
                                    + (e + 1) + " if has DATA object type) ";
                            break;
                        default:
                            rslt = "(Additional readout profile " + (e - 4)
                                    + " if has PROFILE GENERIC object type or Standard readout parametrization "
                                    + (e + 1) + " if has DATA object type) ";
                            break;
                    }
                } else {
                    return "Reserved (Ref: 6.2.19)";
                }
                break;
            case 22:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "IEC HDLC setup";
                } else {
                    return "Reserved (Ref: 6.2.20)";
                }
                break;
            case 23:
                if (f == 255) {
                    switch (d) {
                        case 0:
                            switch (e) {
                                case 0:
                                    rslt = "IEC twisted pair (1) setup";
                                    break;
                                case 1:
                                    rslt = "IEC twisted pair (1) MAC address setup";
                                    break;
                                case 2:
                                    rslt = "IEC twisted pair (1) fatal error register";
                                    break;
                                default:
                                    return "Reserved (Ref: 6.2.21)";
                            }
                            break;
                        case 3:
                            if (e == 0) {
                                rslt = "IEC 62056-3-1 Short readout if has PROFILE GENERIC object type "
                                        + "or IEC 62056-3-1 readout parametrization if has DATA object type";
                            } else if (e == 1) {
                                rslt = "IEC 62056-3-1 Short readout if has PROFILE GENERIC object type "
                                        + "or IEC 62056-3-1 readout parametrization 1 if has DATA object type";
                            } else if (e >= 2 && e <= 9) {
                                rslt = "IEC 62056-3-1 Alternate readout profile " + (e - 2) + " if has PROFILE GENERIC object type "
                                        + "or IEC 62056-3-1 readout parametrization " + e + " if has DATA object type";
                            } else {
                                rslt = "IEC 62056-3-1 readout parametrization " + e;
                            }
                            break;
                        default:
                            return "Reserved (Ref: 6.2.21)";
                    }
                } else {
                    return "Reserved (Ref: 6.2.21)";
                }
                break;
            case 24:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "M-Bus slave port setup";
                } else if (d == 1 && f == 255) {
                    rslt = "M-Bus client " + (e + 1);
                } else if (d == 2 && f == 255) {
                    rslt = "M-Bus value " + (e + 1);
                } else if (d == 3 && f == 255) {
                    rslt = "M-Bus profile generic " + (e + 1);
                } else if (d == 4 && e == 0 && f == 255) {
                    rslt = "M-Bus disconnect control";
                } else if (d == 5 && e == 0 && f == 255) {
                    rslt = "M-Bus control log";
                } else if (d == 6 && e == 0 && f == 255) {
                    rslt = "M-Bus master port setup";
                } else if (d == 8 && f == 255) {
                    rslt = "DLMS/COSEM server M-Bus port setup " + (e + 1);
                } else if (d == 9 && f == 255) {
                    rslt = "M-Bus diagnostic " + (e + 1);
                } else {
                    return "Reserved (Ref: 6.2.22)";
                }
                break;
            case 25:
                if (e == 0 && f == 255) {
                    switch (d) {
                        case 0:
                            rslt = "TCP-UDP setup";
                            break;
                        case 1:
                            rslt = "IPv4 setup";
                            break;
                        case 2:
                            rslt = "MAC address setup";
                            break;
                        case 3:
                            rslt = "PPP setup";
                            break;
                        case 4:
                            rslt = "GPRS modem setup";
                            break;
                        case 5:
                            rslt = "SMTP setup";
                            break;
                        case 6:
                            rslt = "GSM diagnostic";
                            break;
                        case 7:
                            rslt = "IPv6 setup";
                            break;
                        case 9:
                            rslt = "Push setup";
                            break;
                        case 10:
                            rslt = "NTP setup";
                            break;
                        case 11:
                            rslt = "LTE monitoring";
                            break;
                        default:
                            return "Reserved (Ref: 6.2.23)";
                    }
                } else {
                    return "Reserved (Ref: 6.2.23)";
                }
                break;
            case 26:
                if (e == 0 && f == 255) {
                    switch (d) {
                        case 0:
                            rslt = "S-FSK Phy&MAC setup";
                            break;
                        case 1:
                            rslt = "S-FSK Active initiator";
                            break;
                        case 2:
                            rslt = "S-FSK MAC synchronization timeouts";
                            break;
                        case 3:
                            rslt = "S-FSK MAC counters";
                            break;
                        case 5:
                            rslt = "IEC 61334-4-32 LLC setup";
                            break;
                        case 6:
                            rslt = "S-FSK Reporting system list";
                            break;
                        default:
                            return "Reserved (Ref: 6.2.24)";
                    }
                } else {
                    return "Reserved (Ref: 6.2.24)";
                }
                break;
            case 27:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "ISO/IEC 8802-2 LLC Type 1 setup";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "ISO/IEC 8802-2 LLC Type 2 setup";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "ISO/IEC 8802-2 LLC Type 3 setup";
                } else {
                    return "Reserved (Ref: 6.2.25)";
                }
                break;
            case 28:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "61334-4-32 LLC SSCS setup";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC Physical layer counters";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC setup";
                } else if (d == 3 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC functional parameters";
                } else if (d == 4 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC counters";
                } else if (d == 5 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC network administration data";
                } else if (d == 6 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC address setup";
                } else if (d == 7 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC Application identification";
                } else {
                    return "Reserved (Ref: 6.2.26)";
                }
                break;
            case 29:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "G3-PLC MAC layer counters";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "G3-PLC MAC setup";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "G3-PLC 6LoWPAN adaptation layer setup";
                } else {
                    return "Reserved (Ref: 6.2.27)";
                }
                break;
            case 30:
                if (d == 0 && f == 255) {
                    rslt = "ZigBee� SAS startup " + (e + 1);
                } else if (d == 1 && f == 255) {
                    rslt = "ZigBee� SAS join " + (e + 1);
                } else if (d == 2 && f == 255) {
                    rslt = "ZigBee� SAS APS fragmentation " + (e + 1);
                } else if (d == 3 && f == 255) {
                    rslt = "ZigBee� SAS network control " + (e + 1);
                } else if (d == 4 && f == 255) {
                    rslt = "ZigBee� SAS tunnel setup " + (e + 1);
                } else {
                    return "Reserved (Ref: 6.2.28)";
                }
                break;
            case 31:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "Wireless Mode Q channel";
                } else {
                    return "Reserved (Ref: 6.2.22)";
                }
                break;
            case 33:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 MAC setup";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 CPAS setup";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 IP SSAS setup";
                } else if (d == 3 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 HDLC SSAS setup";
                } else {
                    return "Reserved (Ref: 6.2.29)";
                }
                break;
            case 40:
                if (b == 0 && d == 0 && f == 255) {
                    if (e == 0) {
                        return "Current association view";
                    } else {
                        return "Association view " + e;
                    }
                } else {
                    return "Reserved (Ref: 6.2.30)";
                }
            case 41:
                if (b == 0 && d == 0 && e == 0 && f == 255) {
                    return "SAP assignment";
                } else {
                    return "Reserved (Ref: 6.2.31)";
                }
            case 42:
                if (b == 0 && d == 0 && e == 0 && f == 255) {
                    return "COSEM logical device name";
                } else {
                    return "Reserved (Ref: 6.2.32)";
                }
            case 43:
                if (b == 0 && d == 0 && f == 255) {
                    if (e == 0) {
                        return "Security setup";
                    } else {
                        return "Security setup " + e;
                    }
                } else if (b == 0 && d == 2 && f == 255) {
                    if (e == 0) {
                        return "Data protection";
                    } else {
                        return "Data protection " + e;
                    }
                } else {
                    if (d == 1 && f == 255) {
                        if (e == 0) {
                            rslt = "Invocation counter";
                        } else {
                            rslt = "Invocation counter " + e;
                        }
                    } else {
                        return "Reserved (Ref: 6.2.33)";
                    }
                }
                break;
            case 44:
                if (b == 0 && d == 0 && f == 255) {
                    if (e == 0) {
                        return "Image transfer";
                    } else {
                        return "Image transfer " + e;
                    }
                } else if (b == 0 && d == 1 && f == 255) {
                    if (e == 0) {
                        return "Function control";
                    } else {
                        return "Function control " + e;
                    }
                } else {
                    return "Reserved (Ref: 6.2.35/6.2.34)";
                }
            case 65:
                if (d >= 0 && d <= 15 && f == 255) {
                    rslt = "Standard tables " + ((d * 128) + e);
                } else if (d >= 16 && d <= 31 && f == 255) {
                    rslt = "Manufacturer tables " + ((d * 128) + e);
                } else if (d >= 32 && d <= 47 && f == 255) {
                    rslt = "Std pending tables " + ((d * 128) + e);
                } else if (d >= 48 && d <= 63 && f == 255) {
                    rslt = "Mfg pending tables " + ((d * 128) + e);
                } else {
                    return "Reserved (Ref: 6.2.36)";
                }
                break;
            case 66:
                if (d == 0 && f == 255) {
                    rslt = "Compact data " + e;
                } else {
                    return "Reserved (Ref: 6.2.37)";
                }
                break;
            case 93:
                return "Reserved (Ref: 7.3.4.2)";
            case 94:
                if (CountrySpesificIdentifier.getMappings().containsKey(d)) {
                    rslt = "Identifiers for " + CountrySpesificIdentifier.forValue(d).toString() + " " + e
                            + " " + f;
                } else {
                    return "Reserved (Ref: 7.3.4.3)";
                }
                break;
            case 96:
                if (d == 1 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Nomor meter";
                            break;
                        case 1:
                            rslt = "Tipe meter";
                            break;
                        case 2:
                            rslt = "Id Pelanggan";
                            break;
                        case 3:
                            rslt = "Kode Unit PLN";
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            rslt = "Device ID #" + (e + 1);
                            break;
                        case 10:
                            rslt = "Metering point ID (abstract)";
                            break;
                        case 255:
                            rslt = "Complete device ID";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 2 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "No. of configuration program changes";
                            break;
                        case 1:
                            rslt = "Date of last configuration program change";
                            break;
                        case 2:
                            rslt = "Date of last time switch program change";
                            break;
                        case 3:
                            rslt = "Date of last ripple control program change";
                            break;
                        case 4:
                            rslt = "Status of security switches";
                            break;
                        case 5:
                            rslt = "Date of last calibration";
                            break;
                        case 6:
                            rslt = "Date of next configuration program change";
                            break;
                        case 7:
                            rslt = "Date of activation of the passive calendar";
                            break;
                        case 10:
                            rslt = "No. of protected configuration program changes";
                            break;
                        case 11:
                            rslt = "Date of last protected configuration program change";
                            break;
                        case 12:
                            rslt = "Date (corrected) of last clock synchronisation / setting";
                            break;
                        case 13:
                            rslt = "Date of last firmware activation";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 3 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "State of input/output control signals, global";
                            break;
                        case 1:
                            rslt = "State of the input control signals (status word " + e + ")";
                            break;
                        case 2:
                            rslt = "State of the output control signals (status word " + e + ")";
                            break;
                        case 3:
                            rslt = "State of input/output control signals (status word " + e + ")";
                            break;
                        case 4:
                            rslt = "State of input/output control signals (status word " + e + ")";
                            break;
                        case 10:
                            rslt = "Disconnect control";
                            break;
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                        case 28:
                        case 29:
                            rslt = "Arbitrator " + (e - 20);
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 4 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "State of the internal control signals, global";
                            break;
                        case 1:
                            rslt = "State of the internal control signals (status word 1)";
                            break;
                        case 2:
                            rslt = "State of the internal control signals (status word 2)";
                            break;
                        case 3:
                            rslt = "State of the internal control signals (status word 3)";
                            break;
                        case 4:
                            rslt = "State of the internal control signals (status word 4)";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 5 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "General Internal operating status, global";
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            rslt = "General Internal operating status (status word " + e + ")";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 6 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Battery use time counter";
                            break;
                        case 1:
                            rslt = "Battery charge display";
                            break;
                        case 2:
                            rslt = "Date of next battery change";
                            break;
                        case 3:
                            rslt = "Battery voltage";
                            break;
                        case 4:
                            rslt = "Battery initial capacity";
                            break;
                        case 5:
                            rslt = "Battery installation date and time";
                            break;
                        case 6:
                            rslt = "Battery estimated remaining use time";
                            break;
                        case 10:
                            rslt = "Aux. supply use time counter";
                            break;
                        case 11:
                            rslt = "Aux. voltage (measured)";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 7 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Jumlah padam bersamaan di semua phase";
                            break;
                        case 1:
                            rslt = "Jumlah padam phase R";
                            break;
                        case 2:
                            rslt = "Jumlah padam phase S";
                            break;
                        case 3:
                            rslt = "Jumlah padam phase T";
                            break;
                        case 4:
                            rslt = "No. of power failures Auxiliary power supply";
                            break;
                        case 21:
                            rslt = "Jumlah padam di phase manapun";
                            break;
                        case 5:
                            rslt = "Jumlah padam berkepanjangan bersamaan di semua phasa";
                            break;
                        case 6:
                            rslt = "Jumlah padam berkepanjangan phase R";
                            break;
                        case 7:
                            rslt = "Jumlah padam berkepanjangan phase S";
                            break;
                        case 8:
                            rslt = "Jumlah padam berkepanjangan phase T";
                            break;
                        case 9:
                            rslt = "Jumlah padam berkepanjangan di phase manapun";
                            break;
                        case 10:
                            rslt = "Waktu kejadian padam bersamaan di semua phasa";
                            break;
                        case 11:
                            rslt = "Waktu kejadian padam phase R";
                            break;
                        case 12:
                            rslt = "Waktu kejadian padam phase S";
                            break;
                        case 13:
                            rslt = "Waktu kejadian padam phase T";
                            break;
                        case 14:
                            rslt = "Waktu kejadian padam di phase manapun";
                            break;
                        case 15:
                            rslt = "Durasi padam berkepanjangan bersamaan di semua phasa";
                            break;
                        case 16:
                            rslt = "Durasi padam berkepanjangan phase R";
                            break;
                        case 17:
                            rslt = "Durasi padam berkepanjangan phase S";
                            break;
                        case 18:
                            rslt = "Durasi padam berkepanjangan phase T";
                            break;
                        case 19:
                            rslt = "Durasi padam berkepanjangan di phase manapun";
                            break;
                        case 20:
                            rslt = "Batas waktu padam dinyatakan berkepanjangan";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 8 && f == 255) {
                    if (e == 0) {
                        rslt = "Lama meter beroperasi";
                    } else if (e == 1) {
                        rslt = "Lama meter beroperasi di WBP";
                    } else if (e >= 2 && e <= 63) {
                        rslt = "Lama meter beroperasi di LWBP" + (e - 1);
                    } else {
                        return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 9 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Ambient temperature";
                            break;
                        case 1:
                            rslt = "Ambient pressure";
                            break;
                        case 2:
                            rslt = "Relative humidity";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 10 && f == 255) {
                    if (e >= 1 && e <= 10) {
                        rslt = "Status register " + e;
                    } else {
                        return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 11 && f == 255) {
                    if (e >= 0 && e <= 99) {
                        rslt = "Event code " + (e + 1);
                    } else {
                        return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 12 && f == 255) {
                    switch (e) {
                        case 1:
                            rslt = "Comm. port parameters No. of connections";
                            break;
                        case 4:
                            rslt = "Comm. port parameters Parameter 1";
                            break;
                        case 5:
                            rslt = "Comm. port parameters GSM field strength";
                            break;
                        case 6:
                            rslt = "Comm. port parameters Telephone number / Communication address of the physical device";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 13 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Consumer message via local consumer information port";
                            break;
                        case 1:
                            rslt = "Pesan untuk pelanggan melalui LCD meter";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 14 && f == 255) {
                    if (e >= 0 && e <= 15) {
                        rslt = "Tarif  aktif " + (e + 1);
                    } else {
                        return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 15 && f == 255) {
                    if (e >= 0 && e <= 99) {
                        rslt = "Event counter " + (e + 1);
                    } else {
                        return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 16 && f == 255) {
                    if (e >= 0 && e <= 9) {
                        rslt = "Profile entry digital signature " + (e + 1);
                    } else {
                        return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d == 20 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Meter open event counter";
                            break;
                        case 1:
                            rslt = "Meter open event, time stamp of current event occurrence";
                            break;
                        case 2:
                            rslt = "Meter open event, duration of current event";
                            break;
                        case 3:
                            rslt = "Meter open event, cumulative duration";
                            break;
                        case 5:
                            rslt = "Terminal cover open event counter";
                            break;
                        case 6:
                            rslt = "Terminal cover open event, time stamp of current event occurrence";
                            break;
                        case 7:
                            rslt = "Terminal cover open event, duration of current event";
                            break;
                        case 8:
                            rslt = "Terminal cover open event, cumulative duration";
                            break;
                        case 10:
                            rslt = "Tilt event counter";
                            break;
                        case 11:
                            rslt = "Tilt event, time stamp of current event occurrence";
                            break;
                        case 12:
                            rslt = "Tilt event, duration of current event";
                            break;
                        case 13:
                            rslt = "Tilt event, cumulative duration";
                            break;
                        case 15:
                            rslt = "Strong DC magnetic field event counter";
                            break;
                        case 16:
                            rslt = "Strong DC magnetic field event, time stamp of current event occurrence";
                            break;
                        case 17:
                            rslt = "Strong DC magnetic field event, duration of current event";
                            break;
                        case 18:
                            rslt = "Strong DC magnetic field event, cumulative duration";
                            break;
                        case 20:
                            rslt = "Supply control switch / valve tamper event counter";
                            break;
                        case 21:
                            rslt = "Supply control switch / valve tamper event, time stamp of current event occurrence";
                            break;
                        case 22:
                            rslt = "Supply control switch / valve tamper event, duration of current event";
                            break;
                        case 23:
                            rslt = "Supply control switch / valve tamper event, cumulative duration";
                            break;
                        case 25:
                            rslt = "Metrology tamper event counter";
                            break;
                        case 26:
                            rslt = "Metrology tamper event, time stamp of current event occurrence";
                            break;
                        case 27:
                            rslt = "Metrology tamper event, duration of current event";
                            break;
                        case 28:
                            rslt = "Metrology tamper event, cumulative duration";
                            break;
                        case 29:
                            rslt = "Reserved";
                            break;
                        case 30:
                            rslt = "Communication tamper event counter";
                            break;
                        case 31:
                            rslt = "Communication tamper event, time stamp of current event occurrence";
                            break;
                        case 32:
                            rslt = "Communication tamper event, duration of current event";
                            break;
                        case 33:
                            rslt = "Communication tamper event, cumulative duration";
                            break;
                        default:
                            return "Reserved (Ref: 7.4.1)";
                    }
                } else if (d >= 50 && d <= 99 && f == 255) {
                    return "Man. spec. abstract object";
                } else {
                    return "Reserved (Ref: 7.4.1)";
                }
                break;
            case 97:
                if (d == 97 && e >= 0 && e <= 9 && f == 255) {
                    rslt = "General Error object " + (e + 1);
                } else if (d == 97 && e == 255 && f == 255) {
                    rslt = "General Error profile object";
                } else if (d == 98 && e >= 0 && e <= 9 && f == 255) {
                    rslt = "Alarm register object " + (e + 1);
                } else if (d == 98 && e >= 10 && e <= 19 && f == 255) {
                    rslt = "Alarm filter object " + (e - 9);
                } else if (d == 98 && e >= 20 && e <= 29 && f == 255) {
                    rslt = "Alarm descriptor object " + (e - 9);
                } else if (d == 98 && e == 255 && f == 255) {
                    rslt = "Alarm register profile object";
                } else {
                    return "Reserved (Ref: 7.4.2)";
                }
                break;
            case 98:
                if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                    rslt = "General billing ke-" + (e + 1) + " periode " + f;
                } else if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                    rslt = "General billing ke-" + (e + 1) + " " + (f - 100) + " periode sebelumnya";
                } else if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                    rslt = "General billing ke " + (e + 1) + " dengan periode yang tidak ditentukan";
                } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                    rslt = "General billing ke-" + (e + 1) + " scheme 2 periode " + f;
                } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                    rslt = "General billing ke-" + (e + 1) + " scheme 2(" + (f - 100) + " periode sebelumnya)";
                } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                    rslt = "General billing ke-" + (e + 1) + " scheme 2 dengan periode yang tidak ditentukan";
                } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                    rslt = "General register table objects, general use " + (e + 1);
                } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                    rslt = "General register table objects, general use " + (e + 1)
                            + "(" + (f - 100) + " periode sebelumnya)";
                } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                    rslt = "General register table objects, general use dengan periode yang tidak ditentukan" + (e + 1);
                } else {
                    return "Reserved (Ref: 7.4.3/7.4.4)";
                }
                break;
            case 99:
                if (d == 1 && e >= 0 && e <= 127 && f == 255) {
                    if (e == 0) {
                        rslt = "General load profile";
                    } else {
                        rslt = "General load profile ke-" + (e + 1);
                    }
                } else if (d == 2 && e >= 0 && e <= 127 && f == 255) {
                    if (e == 0) {
                        rslt = "General load profile periode 2";
                    } else {
                        rslt = "General load profile periode 2 ke-" + (e + 1);
                    }
                } else if (d == 3 && e == 0 && f == 255) {
                    rslt = "General load profile during test";
                } else if (d == 12 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Connection profile " + (e + 1);
                } else if (d == 13 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "GSM diagnostic profile " + (e + 1);
                } else if (d == 14 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Charge collection history (Payment metering) " + (e + 1);
                } else if (d == 15 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Token credit history (Payment metering) " + (e + 1);
                } else if (d == 16 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Parameter monitor log " + (e + 1);
                } else if (d == 17 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Token transfer log (Payment metering) " + (e + 1);
                } else if (d == 98 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "General event log " + (e + 1);
                } else {
                    return "Reserved (Ref: 7.4.3/7.4.4)";
                }
                break;
            case 127:
                return "Inactive object (Ref: 6.2.62)";
            default:
                return "Undefined Object";
        }
        if (b >= 1 && b <= 64) {
            rslt += " Channel " + b;
        }
        if (rslt.equals("")) {
            return "Undefined Object";
        } else {
            return rslt;
        }
    }
}
