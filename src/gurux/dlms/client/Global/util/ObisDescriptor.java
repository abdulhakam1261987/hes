/*
 * 
 */
package gurux.dlms.client.Global.util;

import java.util.logging.Logger;

/**
 * Class Untuk mendeskripsikan OBIS
 *
 * @author Tab Solutions
 */
public class ObisDescriptor {

    private static int a;
    private static int b;
    private static int c;
    private static int d;
    private static int e;
    private static int f;

    private static final Logger LOG = Logger.getLogger(ObisDescriptor.class.getName());

    /**
     * Getting class log
     * 
     * @return Logger class
     */
    public static Logger getLOG() {
        return LOG;
    }

    /**
     * Mendeskripsikan OBIS.
     * 
     * @param logicalName Logical name dari OBIS.
     * @return Deskripsi OBIS dalam bentuk String.
     */
    public static String getDeskripsi(String logicalName) {
        int titikKe = 0;
        String as = "";
        String bs = "";
        String cs = "";
        String ds = "";
        String es = "";
        String fs = "";
        for (int i = 0; i < logicalName.length(); i++) {
            if (logicalName.substring(i, i + 1).equals(".")) {
                titikKe++;
                continue;
            }
            switch (titikKe) {
                case 0:
                    as = as + logicalName.substring(i, i + 1);
                    break;
                case 1:
                    bs = bs + logicalName.substring(i, i + 1);
                    break;
                case 2:
                    cs = cs + logicalName.substring(i, i + 1);
                    break;
                case 3:
                    ds = ds + logicalName.substring(i, i + 1);
                    break;
                case 4:
                    es = es + logicalName.substring(i, i + 1);
                    break;
                case 5:
                    fs = fs + logicalName.substring(i, i + 1);
                    break;
                default:
                    break;
            }
        }
        a = Integer.parseInt(as);
        b = Integer.parseInt(bs);
        c = Integer.parseInt(cs);
        d = Integer.parseInt(ds);
        e = Integer.parseInt(es);
        f = Integer.parseInt(fs);
        if (b >= 128 && b <= 199) {
            return "Manufacture Specific B" + b + " (ref: 7.2.2)";
        } else if (b >= 65 && b <= 127) {
            return "Utility Specific B" + b + " (ref: 7.3.2)";
        } else if (b >= 200 && b <= 255) {
            return "B-Reserved B" + b + " (ref: 7.3.2)";
        } else if ((c >= 128 && c <= 199) || (c == 240)) {
            return "Manufacture Specific C" + c + " (ref: 7.2.2)";
        } else if (d >= 128 && d <= 254) {
            return "Manufacture Specific D" + d + " (ref: 7.2.2)";
        } else if (e >= 128 && e <= 254) {
            return "Manufacture Specific E" + e + " (ref: 7.2.2)";
        } else if (f >= 128 && f <= 254) {
            return "Manufacture Specific F" + f + " (ref: 7.2.2)";
        } else {
            switch (a) {
                case 0:
                    return DLMSGeneralObject.get(a, b, c, d, e, f).trim().replaceFirst(
                            DLMSGeneralObject.get(a, b, c, d, e, f).substring(0, 1),
                            DLMSGeneralObject.get(a, b, c, d, e, f).substring(0, 1).toUpperCase());
                case 1:
                    if ((c >= 100 && c <= 127) || (c >= 200 && c <= 239) || c >= 241 || c == 95) {
                        return "Reserved (ref: 7.5.1)";
                    } else {
                        return DLMSElectricityObject.get(a, b, c, d, e, f).trim().replaceFirst(
                                DLMSElectricityObject.get(a, b, c, d, e, f).substring(0, 1),
                                DLMSElectricityObject.get(a, b, c, d, e, f).substring(0, 1).toUpperCase());
                    }
                default:
                    return "Bukan object abstract atau electricity (A != 0 | A!= 1) (ref: 7.3.1)";
            }
        }
    }
}
