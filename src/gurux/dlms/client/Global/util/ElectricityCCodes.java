package gurux.dlms.client.Global.util;

/**
 * Value group C codes � Electricity.
 *
 * Reference : Blue Book Chapter 7.5.1
 *
 * @author Tab Solutions Team
 */
public enum ElectricityCCodes {
    /**
     * Active power+ (QI+QIV) ?Li
     */
    aktif_terima(1),
    /**
     * Active power+ (QI+QIV) L1
     */
    aktif_terima_R(21),
    /**
     * Active power+ (QI+QIV) L2
     */
    aktif_terima_S(41),
    /**
     * Active power+ (QI+QIV) L3
     */
    aktif_terima_T(61),
    /**
     * Active power� (QII+QIII) ?Li
     */
    aktif_kirim(2),
    /**
     * Active power� (QII+QIII) L1
     */
    aktif_kirim_R(22),
    /**
     * Active power� (QII+QIII) L2
     */
    aktif_kirim_S(42),
    /**
     * Active power� (QII+QIII) L3
     */
    aktif_kirim_T(62),
    /**
     * Reactive power+ (QI+QII) ?Li
     */
    reaktif_terima(3),
    /**
     * Reactive power+ (QI+QII) L1
     */
    reaktif_terima_R(23),
    /**
     * Reactive power+ (QI+QII) L2
     */
    reaktif_terima_S(43),
    /**
     * Reactive power+ (QI+QII) L3
     */
    reaktif_terima_T(63),
    /**
     * Reactive power� (QIII+QIV) ?Li
     */
    reaktif_kirim(4),
    /**
     * Reactive power� (QIII+QIV) L1
     */
    reaktif_kirim_R(24),
    /**
     * Reactive power� (QIII+QIV) L2
     */
    reaktif_kirim_S(44),
    /**
     * Reactive power� (QIII+QIV) L3
     */
    reaktif_kirim_T(64),
    /**
     * Reactive power QI ?Li
     */
    reaktif_Q1(5),
    /**
     * Reactive power QI L1
     */
    reaktif_Q1_R(25),
    /**
     * Reactive power QI L2
     */
    reaktif_Q1_S(45),
    /**
     * Reactive power QI L3
     */
    reaktif_Q1_T(65),
    /**
     * Reactive power QII ?Li
     */
    reaktif_Q2(6),
    /**
     * Reactive power QII L1
     */
    reaktif_Q2_R(26),
    /**
     * Reactive power QII L2
     */
    reaktif_Q2_S(46),
    /**
     * Reactive power QII L3
     */
    reaktif_Q2_T(66),
    /**
     * Reactive power QIII ?Li
     */
    reaktif_Q3(7),
    /**
     * Reactive power QIII L1
     */
    reaktif_Q3_R(27),
    /**
     * Reactive power QIII L2
     */
    reaktif_Q3_S(47),
    /**
     * Reactive power QIII L3
     */
    reaktif_Q3_T(67),
    /**
     * Reactive power QIV ?Li
     */
    reaktif_Q4(8),
    /**
     * Reactive power QIV L1
     */
    reaktif_Q4_R(28),
    /**
     * Reactive power QIV L2
     */
    reaktif_Q4_S(48),
    /**
     * Reactive power QIV L3
     */
    reaktif_Q4_T(68),
    /**
     * Apparent power+ (QI+QIV) ?Li. if just one Apparent Energy/demand value is
     * calculated over the four-quadrant, C = 9 shall be used
     */
    apparent_terima(9),
    /**
     * Apparent power+ (QI+QIV) L1. if just one Apparent Energy/demand value is
     * calculated over the four-quadrant, C = 9 shall be used
     */
    apparent_terima_R(29),
    /**
     * Apparent power+ (QI+QIV) L2. if just one Apparent Energy/demand value is
     * calculated over the four-quadrant, C = 9 shall be used
     */
    apparent_terima_S(49),
    /**
     * Apparent power+ (QI+QIV) L3. if just one Apparent Energy/demand value is
     * calculated over the four-quadrant, C = 9 shall be used
     */
    apparent_terima_T(69),
    /**
     * Apparent power- (QII+QIII) ?Li.
     */
    apparent_kirim(10),
    /**
     * Apparent power- (QII+QIII) L1.
     */
    apparent_kirim_R(30),
    /**
     * Apparent power- (QII+QIII) L2.
     */
    apparent_kirim_S(50),
    /**
     * Apparent power- (QII+QIII) L3.
     */
    apparent_kirim_T(70),
    /**
     * Current in any phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    arus(11),
    /**
     * Current in L1 phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    arus_R(31),
    /**
     * Current in L2 phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    arus_S(51),
    /**
     * Current in L3 phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    arus_T(71),
    /**
     * Voltage in any phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    tegangan(12),
    /**
     * Voltage in L1 phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    tegangan_R(32),
    /**
     * Voltage in L2 phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    tegangan_S(52),
    /**
     * Voltage in L3 phase.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    tegangan_T(72),
    /**
     * Power factor. Kuantitas faktor daya dengan C = 13, 33, 53, 73 dihitung
     * sebagai PF = daya aktif terima (C = 1, 21, 41, 61) / daya apparent terima
     * (C = 9, 29, 49, 69) atau PF = daya aktif kirim (C = 2, 22, 42, 62) / daya
     * apparent kirim (C = 10, 30, 50, 70). Dalam kasus pertama, tandanya
     * positif (tidak ada tanda), itu berarti faktor daya dalam arah impor (PF
     * +). Dalam kasus kedua, tandanya negatif, itu berarti faktor daya dalam
     * arah ekspor (PF-). Kuantitas faktor daya C = 84, 85, 86 dan 87 selalu
     * dihitung sebagai PF kirim = daya aktif kirim / daya apparent kirim.
     * Kuantitas ini adalah faktor daya dalam arah ekspor; tidak memiliki tanda.
     */
    faktor_daya(13),
    /**
     * Power factor R. Power factor quantities with C = 13, 33, 53, 73 are
     * calculated either as PF = Active power+ (C = 1, 21, 41, 61) / Apparent
     * power+ (C = 9, 29, 49, 69) or PF = Active power� (C = 2, 22, 42, 62) /
     * Apparent power- (C = 10, 30, 50, 70). In the first case, the sign is
     * positive (no sign), it means power factor in the import direction (PF+).
     * In the second case, the sign is negative, it means power factor in the
     * export direction (PF�). Power factor quantities C = 84, 85, 86 and 87 are
     * always calculated as PF� = Active power� / Apparent power�. This quantity
     * is the power factor in the export direction; it has no sign.
     */
    faktor_daya_R(33),
    /**
     * Power factor S. Power factor quantities with C = 13, 33, 53, 73 are
     * calculated either as PF = Active power+ (C = 1, 21, 41, 61) / Apparent
     * power+ (C = 9, 29, 49, 69) or PF = Active power� (C = 2, 22, 42, 62) /
     * Apparent power- (C = 10, 30, 50, 70). In the first case, the sign is
     * positive (no sign), it means power factor in the import direction (PF+).
     * In the second case, the sign is negative, it means power factor in the
     * export direction (PF�). Power factor quantities C = 84, 85, 86 and 87 are
     * always calculated as PF� = Active power� / Apparent power�. This quantity
     * is the power factor in the export direction; it has no sign.
     */
    faktor_daya_S(53),
    /**
     * Power factor T. Power factor quantities with C = 13, 33, 53, 73 are
     * calculated either as PF = Active power+ (C = 1, 21, 41, 61) / Apparent
     * power+ (C = 9, 29, 49, 69) or PF = Active power� (C = 2, 22, 42, 62) /
     * Apparent power- (C = 10, 30, 50, 70). In the first case, the sign is
     * positive (no sign), it means power factor in the import direction (PF+).
     * In the second case, the sign is negative, it means power factor in the
     * export direction (PF�). Power factor quantities C = 84, 85, 86 and 87 are
     * always calculated as PF� = Active power� / Apparent power�. This quantity
     * is the power factor in the export direction; it has no sign.
     */
    faktor_daya_T(73),
    /**
     * Supply frequency
     */
    frekuensi(14),
    /**
     * Supply frequency L1
     */
    frekuensi_R(34),
    /**
     * Supply frequency L2
     */
    frekuensi_S(54),
    /**
     * Supply frequency L3
     */
    frekuensi_T(74),
    /**
     * Active power (abs(QI+QIV)+(abs(QII+QIII)) ?Li.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    aktif_abs_terima_plus_abs_kirim(15),
    /**
     * Active power (abs(QI+QIV)+(abs(QII+QIII)) L1.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    aktif_abs_terima_plus_abs_kirim_R(35),
    /**
     * Active power (abs(QI+QIV)+(abs(QII+QIII)) L2.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    aktif_abs_terima_plus_abs_kirim_S(55),
    /**
     * Active power (abs(QI+QIV)+(abs(QII+QIII)) L3.
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    aktif_abs_terima_plus_abs_kirim_T(75),
    /**
     * Active power (abs(QI+QIV)-abs(QII+QIII)) ?Li.
     */
    aktif_abs_terima_minq_abs_kirim(16),
    /**
     * Active power (abs(QI+QIV)-abs(QII+QIII)) L1.
     */
    aktif_abs_terima_minq_abs_kirim_R(16),
    /**
     * Active power (abs(QI+QIV)-abs(QII+QIII)) L2.
     */
    aktif_abs_terima_minq_abs_kirim_S(56),
    /**
     * Active power (abs(QI+QIV)-abs(QII+QIII)) L3.
     */
    aktif_abs_terima_minq_abs_kirim_T(76),
    /**
     * Active power QI ?Li.
     */
    aktif_Q1(17),
    /**
     * Active power QI L1.
     */
    aktif_Q1_R(37),
    /**
     * Active power QI L2.
     */
    aktif_Q1_S(57),
    /**
     * Active power QI L3.
     */
    aktif_Q1_T(77),
    /**
     * Active power QII ?Li.
     */
    aktif_Q2(18),
    /**
     * Active power QII L1.
     */
    aktif_Q2_R(38),
    /**
     * Active power QII L2.
     */
    aktif_Q2_S(58),
    /**
     * Active power QII L3.
     */
    aktif_Q2_T(78),
    /**
     * Active power QIII ?Li.
     */
    aktif_Q3(19),
    /**
     * Active power QIII L1.
     */
    aktif_Q3_R(39),
    /**
     * Active power QIII L2.
     */
    aktif_Q3_S(59),
    /**
     * Active power QIII L3.
     */
    aktif_Q3_T(79),
    /**
     * Active power QIV ?Li.
     */
    aktif_Q4(20),
    /**
     * Active power QIV L1.
     */
    aktif_Q4_R(40),
    /**
     * Active power QIV L2.
     */
    aktif_Q4_S(60),
    /**
     * Active power QIV L3.
     */
    aktif_Q4_T(80),
    /**
     * Angles. Reference : Blue Book Chapter 7.5.3.4 for details of extended
     * codes.
     */
    sudut(81),
    /**
     * Unitless quantity (pulses or pieces).
     */
    unitless_quantity_pulses_or_pieces(82),
    /**
     * Transformer and line loss quantities.
     *
     * Reference : Blue Book Chapter 7.5.3.5 for details of extended codes.
     */
    transformer_and_line_loss_quantities(83),
    /**
     * ?Li Power factor�. Apparent power+ (C = 9, 29, 49, 69) or PF = Active
     * power� (C = 2, 22, 42, 62) / Apparent power- (C = 10, 30, 50, 70). In the
     * first case, the sign is positive (no sign), it means power factor in the
     * import direction (PF+). In the second case, the sign is negative, it
     * means power factor in the export direction (PF�). Power factor quantities
     * C = 84, 85, 86 and 87 are always calculated as PF� = Active power� /
     * Apparent power�. This quantity is the power factor in the export
     * direction; it has no sign.
     */
    faktor_daya_kirim(84),
    /**
     * L1 Power factor�.
     */
    faktor_daya_kirim_R(85),
    /**
     * L2 Power factor�.
     */
    faktor_daya_kirim_S(86),
    /**
     * L3 Power factor�.
     */
    faktor_daya_kirim_T(87),
    /**
     * ?Li Ampere-squared hours (QI+QII+QIII+QIV)
     */
    ampere_minq_squaredQIV(88),
    /**
     * ?Li Volt-squared hours (QI+QII+QIII+QIV)
     */
    volt_minq_squaredQIV(89),
    /**
     * ?Li current (algebraic sum of the � unsigned � value of the currents in
     * all phases)
     */
    current_algebraic(90),
    /**
     * L0 current (neutral).
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    arus_N(91),
    /**
     * L0 voltage (neutral).
     *
     * Reference : Blue Book Chapter 7.5.3.3 for details of extended codes.
     */
    tegangan_N(92),
    /**
     * Other
     */
    none(0);

    private final int cValue;
    private static java.util.HashMap<Integer, ElectricityCCodes> mappings;

    private static java.util.HashMap<Integer, ElectricityCCodes> getMappings() {
        synchronized (ElectricityCCodes.class) {
            if (mappings == null) {
                mappings = new java.util.HashMap<>();
            }
        }
        return mappings;
    }

    ElectricityCCodes(final int value) {
        cValue = value;
        getMappings().put(value, this);
    }

    /**
     * Get C value from enum.
     *
     * @return Nilai C dari object electricity (A adalah 1).
     */
    public int getcValue() {
        return cValue;
    }

    /**
     * Convert integer for enum value.
     *
     * @param value Nilai C dari OBIS electricity object (A adalah 1).
     * @return ElectricityCCodes enum value.
     */
    public static ElectricityCCodes forValue(final int value) {
        if (getMappings().containsKey(value)) {
            return getMappings().get(value);
        } else {
            return getMappings().get(0);
        }
    }

    /**
     * Get value group C codes � Electricity as String.
     *
     * Reference : Blue Book Table 61.
     *
     * @return String of value group C codes � Electricity.
     */
    public String getString() {
        return forValue(cValue).toString().replace("_", " ").replace("plus", "+").replace("minq", "-").
                replace("abs terima", "(abs(terima)").replace("abs kirim", "abs(kirim))").
                replace("pulses_or_pieces", "(pulses_or_pieces)").
                replace(" squaredQIV", "squared hours (Q1 + Q2 + Q3 + Q4").
                replace("algebraic", "(algebraic sum of the - unsigned - value of the currents in all phases)").
                replace("none", "");
    }
}
