/*
 * 
 */
package gurux.dlms.client.Global.util;

/**
 * Menentukan penggunaan kelompok nilai D untuk pengaplikasian di negara
 * tertentu. Jika memungkinkan, kode panggilan negara digunakan. Tidak ada
 * rentang yang disediakan untuk kode khusus pabrikan. Penggunaan nilai grup E
 * dan F didefinisikan dalam dokumen spesifik negara. Objek yang sudah
 * diidentifikasi dalam dokumen ini tidak boleh diidentifikasi kembali oleh
 * pengidentifikasi khusus negara. Dengan pembubaran Yugoslavia ke negara-negara
 * yang terpisah, kode negara 38 dinonaktifkan.
 *
 * @author Tab Solutions Team
 */
public enum CountrySpesificIdentifier {
    /**
     * FINLAND
     */
    FINLAND(0),
    /**
     * USA
     */
    USA(1),
    /**
     * CANADA
     */
    CANADA(2),
    /**
     * SERBIA
     */
    SERBIA(3),
    /**
     * RUSSIA
     */
    RUSSIA(7),
    /**
     * CZECH
     */
    CZECH(10),
    /**
     * BULGARIA
     */
    BULGARIA(11),
    /**
     * CROATIA
     */
    CROATIA(12),
    /**
     * IRELAND
     */
    IRELAND(13),
    /**
     * ISRAEL
     */
    ISRAEL(14),
    /**
     * UKRAINE
     */
    UKRAINE(15),
    /**
     * YUGOSLAVIA
     */
    YUGOSLAVIA(16),
    /**
     * EGYIPT
     */
    EGYIPT(20),
    /**
     * SOUTH AFRICA
     */
    SOUTH_AFRICA(27),
    /**
     * GREECE
     */
    GREECE(30),
    /**
     * NETHERLANDS
     */
    NETHERLANDS(31),
    /**
     * BELGIUM
     */
    BELGIUM(32),
    /**
     * FRANCE
     */
    FRANCE(33),
    /**
     * SPAIN
     */
    SPAIN(34),
    /**
     * PORTUGAL
     */
    PORTUGAL(35),
    /**
     * HUNGARY
     */
    HUNGARY(36),
    /**
     * LITHUANIA
     */
    LITHUANIA(37),
    /**
     * SLOVENIA
     */
    SLOVENIA(38),
    /**
     * ITALY
     */
    ITALY(39),
    /**
     * ROMANIA
     */
    ROMANIA(40),
    /**
     * SWITZERLAND
     */
    SWITZERLAND(41),
    /**
     * SLOVAKIA
     */
    SLOVAKIA(42),
    /**
     * AUSTRIA
     */
    AUSTRIA(43),
    /**
     * UK
     */
    UK(44),
    /**
     * DENMARK
     */
    DENMARK(45),
    /**
     * SWEDEN
     */
    SWEDEN(46),
    /**
     * NORWAY
     */
    NORWAY(47),
    /**
     * POLAND
     */
    POLAND(48),
    /**
     * GERMANY
     */
    GERMANY(49),
    /**
     * PERU
     */
    PERU(51),
    /**
     * SOUTH KOREA
     */
    SOUTH_KOREA(52),
    /**
     * CUBA
     */
    CUBA(53),
    /**
     * ARGENTINA
     */
    ARGENTINA(54),
    /**
     * BRAZIL
     */
    BRAZIL(55),
    /**
     * CHILE
     */
    CHILE(56),
    /**
     * COLOMBIA
     */
    COLOMBIA(57),
    /**
     * VENEZUELA
     */
    VENEZUELA(58),
    /**
     * MALAYSIA
     */
    MALAYSIA(60),
    /**
     * AUSTRALIA
     */
    AUSTRALIA(61),
    /**
     * INDONESIA
     */
    INDONESIA(62),
    /**
     * PHILIPPINES
     */
    PHILIPPINES(63),
    /**
     * NEW ZEALAND
     */
    NEW_ZEALAND(64),
    /**
     * SINGAPORE
     */
    SINGAPORE(65),
    /**
     * THAILAND
     */
    THAILAND(66),
    /**
     * MOLDOVA
     */
    MOLDOVA(73),
    /**
     * BELARUS
     */
    BELARUS(75),
    /**
     * JAPAN
     */
    JAPAN(81),
    /**
     * HONG KONG
     */
    HONG_KONG(85),
    /**
     * CHINA
     */
    CHINA(86),
    /**
     * BOSNIA AND HERZEGOVINA
     */
    BOSNIA_AND_HERZEGOVINA(87),
    /**
     * TURKEY
     */
    TURKEY(90),
    /**
     * INDIA
     */
    INDIA(91),
    /**
     * PAKISTAN
     */
    PAKISTAN(92),
    /**
     * SAUDI ARABIA
     */
    SAUDI_ARABIA(96),
    /**
     * THE UNITED ARAB EMIRATES
     */
    THE_UNITED_ARAB_EMIRATES(97),
    /**
     * IRAN
     */
    IRAN(98);

    private final int intValue;
    private static java.util.HashMap<Integer, CountrySpesificIdentifier> mappings;

    /**
     * Get mapping of country spesific identifier.
     *
     * @return List of country spesific identifier.
     */
    public static java.util.HashMap<Integer, CountrySpesificIdentifier> getMappings() {
        synchronized (CountrySpesificIdentifier.class) {
            if (mappings == null) {
                mappings = new java.util.HashMap<>();
            }
        }
        return mappings;
    }

    CountrySpesificIdentifier(final int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    /**
     * Get integer value from enumeration.
     *
     * @return Country specific identifiers in integer value.
     */
    public int getValue() {
        return intValue;
    }

    /**
     * Convert integer for enum value.
     *
     * @param value Country specific identifiers in integer value.
     * @return CountrySpesificIdentifier object by integer value.
     */
    public static CountrySpesificIdentifier forValue(final int value) {
        CountrySpesificIdentifier type = getMappings().get(value);
        if (type == null) {
            throw new IllegalArgumentException(
                    "Invalid data type: " + String.valueOf(value));
        }
        return type;
    }

}
