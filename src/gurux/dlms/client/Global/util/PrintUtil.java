/*
 * Tab Solutions Team
 */
package gurux.dlms.client.Global.util;

import gurux.common.GXCommon;
import gurux.dlms.GXDLMSException;
import gurux.dlms.GXDateTime;
import gurux.dlms.client.Global.PhysicalUnit;
import gurux.dlms.client.Global.SkaladanUnit;
import gurux.dlms.client.Global.util.fieldname.ObisFieldName;
import gurux.dlms.enums.ClockStatus;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.DateTimeExtraInfo;
import gurux.dlms.enums.DateTimeSkips;
import gurux.dlms.enums.ErrorCode;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.objects.GXDLMSActionSchedule;
import gurux.dlms.objects.GXDLMSActivityCalendar;
import gurux.dlms.objects.GXDLMSCertificateInfo;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDayProfile;
import gurux.dlms.objects.GXDLMSDayProfileAction;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSDisconnectControl;
import gurux.dlms.objects.GXDLMSExtendedRegister;
import gurux.dlms.objects.GXDLMSLimiter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.GXDLMSRegisterActivation;
import gurux.dlms.objects.GXDLMSScript;
import gurux.dlms.objects.GXDLMSScriptAction;
import gurux.dlms.objects.GXDLMSScriptTable;
import gurux.dlms.objects.GXDLMSSeasonProfile;
import gurux.dlms.objects.GXDLMSSecuritySetup;
import gurux.dlms.objects.GXDLMSWeekProfile;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tab Solutions
 *
 * Class untuk mencetak value
 */
public class PrintUtil {

    /**
     * Logger generator
     */
    private static final Logger LOG = Logger.getLogger(PrintUtil.class.getName());

    /**
     * {@value #READING_LOG}
     */
    public static final String READING_LOG = "Read {0} object ({1}|{2})";
    /**
     * {@value #WRITING_LOG}
     */
    public static final String WRITING_LOG = "Write {0} object ({1}|{2})";
    /**
     * {@value #FAIL_READING_INDEX_LOG}
     */
    public static final String FAIL_READING_INDEX_LOG = "Error membaca index ke-{0}: {1}";
    /**
     * {@value #FAIL_LOG}
     */
    public static final String FAIL_LOG = "Error : {0}";

    /**
     * Fungsi untuk mencetak informasi OBIS.
     *
     * @param object GXDLMSObject yang akan dicetak informasinya.
     */
    public static void printObjectDefinition(GXDLMSObject object) {
        System.out.println("Logical Name (OBIS code) = " + object.getLogicalName());
        System.out.println("Deskripsi = " + object.getDescription());
        System.out.println("Field name = " + ObisFieldName.getFieldName(object.getLogicalName()));
        System.out.println("Version = " + object.getVersion());
    }

    /**
     * Fungsi untuk mencetak informasi object data.
     *
     * @param data GXDLMSData yang akan dicetak informasinya.
     */
    public static void printdata(GXDLMSData data) {
        printObjectDefinition(data);
        try {
            System.out.println(data.getDescription() + " = " + lihatNilai(2, data.getValue()));
            System.out.println(data.getDescription() + " data type = " + data.getDataType(2) + "("
                    + data.getDataType(2).getValue() + ")");
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object register.
     *
     * @param register GXDLMSRegister yang akan dicetak informasinya.
     */
    public static void printRegister(GXDLMSRegister register) {
        printObjectDefinition(register);
        try {
            System.out.println("Value = " + register.getValue());
            System.out.println("Skala = " + register.getScaler());
            System.out.println("Unit = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getUnit().toString()
                    + "(" + PhysicalUnit.getPhysicalUnit(register.getUnit()).getUkuran() + ")");
            System.out.println("Satuan = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getSatuan());
            System.out.println("Nama satuan = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getNamaSatuan());
            System.out.println("SI definition = " + PhysicalUnit.getPhysicalUnit(register.getUnit()).getSIDefinition());
            System.out.println("---------------------------------------------------------------------------");
            System.out.println(register.getDescription() + " = " + register.getValue() + " "
                    + PhysicalUnit.getPhysicalUnit(register.getUnit()).getSatuan());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object extended register.
     *
     * @param extendedRegister GXDLMSExtendedRegister yang akan dicetak
     * informasinya.
     */
    public static void printExtendedRegister(GXDLMSExtendedRegister extendedRegister) {
        printObjectDefinition(extendedRegister);
        try {
            System.out.println("Value = " + extendedRegister.getValue());
            System.out.println("Skala = " + extendedRegister.getScaler());
            System.out.println("Unit = " + PhysicalUnit.getPhysicalUnit(extendedRegister.getUnit()).getUnit().toString()
                    + "(" + PhysicalUnit.getPhysicalUnit(extendedRegister.getUnit()).getUkuran() + ")");
            System.out.println("Satuan = " + PhysicalUnit.getPhysicalUnit(extendedRegister.getUnit()).getSatuan());
            System.out.println("Nama satuan = " + PhysicalUnit.getPhysicalUnit(
                    extendedRegister.getUnit()).getNamaSatuan());
            System.out.println("SI definition = " + PhysicalUnit.getPhysicalUnit(
                    extendedRegister.getUnit()).getSIDefinition());
            System.out.println("Status = " + DataType.forValue(
                    Integer.parseInt(extendedRegister.getStatus().toString())) + "("
                    + extendedRegister.getStatus().toString() + ")");
            System.out.println("Capture time = " + extendedRegister.getCaptureTime().toString());
            System.out.println("---------------------------------------------------------------------------");
            System.out.println(extendedRegister.getDescription() + " = "
                    + extendedRegister.getValue() + " "
                    + PhysicalUnit.getPhysicalUnit(extendedRegister.getUnit()).getSatuan()
                    + " Captured @" + extendedRegister.getCaptureTime().toString());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object demand register.
     *
     * @param demandRegister GXDLMSDemandRegister yang akan dicetak
     * informasinya.
     */
    public static void printDemandRegister(GXDLMSDemandRegister demandRegister) {
        printObjectDefinition(demandRegister);
        try {
            System.out.println("Current average value = " + demandRegister.getCurrentAverageValue().toString());
            System.out.println("Last average value = " + demandRegister.getLastAverageValue().toString());
            System.out.println("Skala = " + demandRegister.getScaler());
            System.out.println("Unit = " + PhysicalUnit.getPhysicalUnit(demandRegister.getUnit()).getUnit().toString()
                    + "(" + PhysicalUnit.getPhysicalUnit(demandRegister.getUnit()).getUkuran() + ")");
            System.out.println("Satuan = " + PhysicalUnit.getPhysicalUnit(demandRegister.getUnit()).getSatuan());
            System.out.println("Nama satuan = " + PhysicalUnit.getPhysicalUnit(demandRegister.getUnit()).getNamaSatuan());
            System.out.println("SI definition = " + PhysicalUnit.getPhysicalUnit(
                    demandRegister.getUnit()).getSIDefinition());
            System.out.println("Status = " + DataType.forValue(
                    Integer.parseInt(demandRegister.getStatus().toString())) + "("
                    + demandRegister.getStatus().toString() + ")");
            System.out.println("Capture time = " + demandRegister.getCaptureTime().toString());
            System.out.println("Start time current = " + demandRegister.getStartTimeCurrent().toString());
            System.out.println("Period = " + demandRegister.getPeriod());
            System.out.println("Number of period = " + demandRegister.getNumberOfPeriods());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object disconnect control.
     *
     * @param disconnectControl GXDLMSDisconnectControl yang akan dicetak
     * informasinya.
     */
    public static void printDisconnectControl(GXDLMSDisconnectControl disconnectControl) {
        printObjectDefinition(disconnectControl);
        try {
            System.out.println("Output State = " + disconnectControl.getOutputState());
            System.out.println("Control State = " + disconnectControl.getControlState());
            System.out.println("Control Mode = " + disconnectControl.getControlMode());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object Activity calendar.
     *
     * @param activityCalendar GXDLMSActivityCalendar yang akan dicetak
     * informasinya.
     */
    public static void printActivityCalendar(GXDLMSActivityCalendar activityCalendar) {
        printObjectDefinition(activityCalendar);
        try {
            System.out.println("activate_passive_calendar_time = " + activityCalendar.getTime());
            System.out.println("Calendar_name_active = " + activityCalendar.getCalendarNameActive());
            System.out.println("Season_profile_active list = ");
            System.out.println("\t" + "Name" + "\t" + "Start" + "\t" + "Week Name");
            for (GXDLMSSeasonProfile seasonProfile : activityCalendar.getSeasonProfileActive()) {
                System.out.println("\t" + new String(seasonProfile.getName())
                        + "\t" + seasonProfile.getStart().toString()
                        + "\t" + new String(seasonProfile.getWeekName()));
            }
            System.out.println("Week_profile_table_active list = ");
            System.out.println("\t" + "Name" + "\t" + "Monday" + "\t" + "Tuesday" + "\t"
                    + "Wednesday" + "\t" + "Thursday" + "\t" + "Friday" + "\t" + "Saturday"
                    + "\t" + "Monday");
            for (GXDLMSWeekProfile weekProfile : activityCalendar.getWeekProfileTableActive()) {
                System.out.println("\t" + new String(weekProfile.getName())
                        + "\t" + weekProfile.getMonday() + "\t" + weekProfile.getTuesday()
                        + "\t" + weekProfile.getWednesday() + "\t" + weekProfile.getThursday()
                        + "\t" + weekProfile.getFriday() + "\t" + weekProfile.getSaturday()
                        + "\t" + weekProfile.getMonday());
            }
            System.out.println("Day_profile_table_active list = ");
            for (GXDLMSDayProfile dayProfile : activityCalendar.getDayProfileTableActive()) {
                System.out.println("\t" + "Day ID = " + dayProfile.getDayId());
                for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
                    System.out.print("\t\t" + "Start Time = " + profileAction.getStartTime());
                    System.out.print("\t\t" + "Script = " + profileAction.getScriptLogicalName());
                    System.out.println("\t\t" + "Selector = " + profileAction.getScriptSelector());
                }
            }
            System.out.println("Calendar_name_passive = " + activityCalendar.getCalendarNamePassive());
            System.out.println("Season_profile_passive list = ");
            System.out.println("\t" + "Name" + "\t" + "Start" + "\t" + "Week Name");
            for (GXDLMSSeasonProfile seasonProfile : activityCalendar.getSeasonProfilePassive()) {
                System.out.println("\t" + new String(seasonProfile.getName())
                        + "\t" + seasonProfile.getStart().toString()
                        + "\t" + new String(seasonProfile.getWeekName()));
            }
            System.out.println("Week_profile_table_passive list = ");
            System.out.println("\t" + "Name" + "\t" + "Monday" + "\t" + "Tuesday" + "\t"
                    + "Wednesday" + "\t" + "Thursday" + "\t" + "Friday" + "\t" + "Saturday"
                    + "\t" + "Monday");
            for (GXDLMSWeekProfile weekProfile : activityCalendar.getWeekProfileTablePassive()) {
                System.out.println("\t" + new String(weekProfile.getName())
                        + "\t" + weekProfile.getMonday() + "\t" + weekProfile.getTuesday()
                        + "\t" + weekProfile.getWednesday() + "\t" + weekProfile.getThursday()
                        + "\t" + weekProfile.getFriday() + "\t" + weekProfile.getSaturday()
                        + "\t" + weekProfile.getMonday());
            }
            System.out.println("Day_profile_table_passive list = ");
            for (GXDLMSDayProfile dayProfile : activityCalendar.getDayProfileTablePassive()) {
                System.out.println("\t" + "Day ID = " + dayProfile.getDayId());
                for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
                    System.out.print("\t\t" + "Start Time = " + profileAction.getStartTime());
                    System.out.print("\t\t" + "Script = " + profileAction.getScriptLogicalName());
                    System.out.println("\t\t" + "Selector = " + profileAction.getScriptSelector());
                }
            }
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object limiter.
     *
     * @param limiter GXDLMSLimiter yang akan dicetak informasinya.
     * @param su Skala dan unit dari value yang dimonitor.
     */
    public static void printLimiter(GXDLMSLimiter limiter, SkaladanUnit su) {
        printObjectDefinition(limiter);
        try {
            System.out.println("Monitored Value = " + limiter.getMonitoredValue().getLogicalName() + " ("
                    + limiter.getMonitoredValue().getDescription() + ") index "
                    + limiter.getmonitoredAttributeIndex());
            System.out.println("Threshold Active = " + limiter.getThresholdActive().toString() + " "
                    + PhysicalUnit.getPhysicalUnit(su.getUnit()).getSatuan());
            System.out.println("Threshold Normal = " + limiter.getThresholdNormal().toString() + " "
                    + PhysicalUnit.getPhysicalUnit(su.getUnit()).getSatuan());
            System.out.println("Threshold Emergency = " + limiter.getThresholdEmergency().toString() + " "
                    + PhysicalUnit.getPhysicalUnit(su.getUnit()).getSatuan());
            System.out.println("Min Over Threshold Duration = " + limiter.getMinOverThresholdDuration() + " detik");
            System.out.println("Min Under Threshold Duration = " + limiter.getMinUnderThresholdDuration() + " detik");
            System.out.println("Emergency Profile = {\n\tId = " + limiter.getEmergencyProfile().getID() + ",\n\t"
                    + "Activation Time = " + limiter.getEmergencyProfile().getActivationTime().toString() + ",\n\t"
                    + "Duration = " + limiter.getEmergencyProfile().getDuration());
            System.out.print("Emergency Profile ID List = {");
            int rem = 1;
            for (int emergencyProfileGroupID : limiter.getEmergencyProfileGroupIDs()) {
                if (rem == 1) {
                    System.out.print(emergencyProfileGroupID);
                    rem++;
                } else {
                    System.out.print(", " + emergencyProfileGroupID);
                }
            }
            System.out.println("}");
            System.out.println("Emergency Profile Active = " + limiter.getEmergencyProfileActive());
            System.out.println("Actions = {\n\tAction Over\n\t\tScript = "
                    + limiter.getActionOverThreshold().getLogicalName() + "\n\t\tSelector = "
                    + limiter.getActionOverThreshold().getScriptSelector() + "\n\tAction Under\n\t\tScript = "
                    + limiter.getActionUnderThreshold().getLogicalName() + "\n\t\tSelector = "
                    + limiter.getActionUnderThreshold().getScriptSelector());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object script table.
     *
     * @param scriptTable GXDLMSScriptTable yang akan dicetak informasinya.
     */
    public static void printScriptTable(GXDLMSScriptTable scriptTable) {
        printObjectDefinition(scriptTable);
        try {
            System.out.println("Script = ");
            for (GXDLMSScript script : scriptTable.getScripts()) {
                System.out.println("\t" + script.getId() + "{");
                for (GXDLMSScriptAction action : script.getActions()) {
                    System.out.println("\t\t{\n\t\t Target = " + action.getTarget().getLogicalName()
                            + "(" + ObisDescriptor.getDeskripsi(action.getTarget().getLogicalName()) + "),");
                    System.out.println("\t\t Index = " + action.getIndex() + ",");
                    System.out.println("\t\t Action type = " + action.getType());
                    System.out.println("\t\t Parameter = " + action.getParameter().toString() + ",");
                    System.out.println("\t\t Parameter type = " + action.getParameterType() + ",");
                    System.out.println("\t\t}");
                }
                System.out.println("\t}");
            }
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object single action schedule.
     *
     * @param actionSchedule GXDLMSActionSchedule yang akan dicetak
     * informasinya.
     */
    public static void printSingleActionSchedule(GXDLMSActionSchedule actionSchedule) {
        printObjectDefinition(actionSchedule);
        try {
            System.out.println("Executed script logical name = " + actionSchedule.getTarget().getLogicalName()
                    + " (" + ObisDescriptor.getDeskripsi(actionSchedule.getTarget().getLogicalName()) + ")");
            System.out.println("Executed script selector = " + actionSchedule.getExecutedScriptSelector());
            System.out.println("Script type = " + actionSchedule.getType().getValue());
            System.out.println("Execution time = ");
            System.out.println("\t{");
            for (GXDateTime executionTime : actionSchedule.getExecutionTime()) {
                Calendar calendar = executionTime.getLocalCalendar();
                System.out.println("\t\t" + executionTime.toString());
//                System.out.println("\t\t" + calendar.get(Calendar.DATE) + "-" + calendar.get(Calendar.DATE) +
//                        "-" + calendar.get(Calendar.YEAR) + " " + calendar.get(Calendar.HOUR_OF_DAY) + ":" +
//                        calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND));
            }
            System.out.println("\t}");
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object single action schedule.
     *
     * @param actionSchedule GXDLMSActionSchedule yang akan dicetak informasi
     * waktu rekamnya.
     */
    public static void printWaktuRekamBilling(GXDLMSActionSchedule actionSchedule) {
        printObjectDefinition(actionSchedule);
        try {
            System.out.println("Executed script logical name = " + actionSchedule.getTarget().getLogicalName()
                    + " (" + ObisDescriptor.getDeskripsi(actionSchedule.getTarget().getLogicalName()) + ")");
            System.out.println("Executed script selector = " + actionSchedule.getExecutedScriptSelector());
            System.out.println("Script type = " + actionSchedule.getType().getValue());
            System.out.print("Execution time = ");
            GXDateTime executionTime = actionSchedule.getExecutionTime()[0];
            Calendar calendar = executionTime.getLocalCalendar();
            String waktuRekamBilling = calendar.get(Calendar.DATE) + "-" + calendar.get(Calendar.DATE)
                    + "-" + calendar.get(Calendar.YEAR) + " ";
            if (calendar.get(Calendar.HOUR_OF_DAY) < 10) {
                waktuRekamBilling += calendar.get(Calendar.HOUR_OF_DAY) + " :";
            } else {
                waktuRekamBilling += calendar.get(Calendar.HOUR_OF_DAY) + ":";
            }
            if (calendar.get(Calendar.MINUTE) < 10) {
                waktuRekamBilling += calendar.get(Calendar.MINUTE);
            } else {
                waktuRekamBilling += calendar.get(Calendar.MINUTE);
            }
            System.out.println(waktuRekamBilling);
            System.out.println("Day of week = " + executionTime.getDayOfWeek());
            System.out.println("Deviation = " + executionTime.getDeviation());
            for (DateTimeExtraInfo dateTimeExtraInfo : executionTime.getExtra()) {
                System.out.println("dateTimeExtraInfo = " + dateTimeExtraInfo.getValue());
            }
            for (DateTimeSkips dateTimeSkips : executionTime.getSkip()) {
                System.out.println("dateTimeSkips = " + dateTimeSkips.toString());
            }
            for (ClockStatus status : executionTime.getStatus()) {
                System.out.println("Status = " + status.toString());
            }
            System.out.println("Meter Calendar = " + executionTime.getMeterCalendar().getTime().toString());
            System.out.println("Local Calendar = " + executionTime.getLocalCalendar().getTime().toString());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi data waktu rekam billing.
     *
     * @param data GXDLMSData yang akan dicetak informasi waktu rekamnya.
     */
    public static void printWaktuRekamBilling(GXDLMSData data) {
        printObjectDefinition(data);
        try {
            byte[] value = (byte[]) data.getValue();
            System.out.println(GXCommon.bytesToHex(value));
            String[] nilai = GXCommon.bytesToHex(value).split(" ");
            String tahun = hexStringtoDecString(nilai[0] + nilai[1]);
            String bulan = hexStringtoDecString(nilai[2]);
            String tanggal = hexStringtoDecString(nilai[3]);
            String hari = hexStringtoDecString(nilai[4]);
            String jam = hexStringtoDecString(nilai[5]);
            String menit = hexStringtoDecString(nilai[6]);
            String detik = hexStringtoDecString(nilai[7]);
            String milidetik = hexStringtoDecString(nilai[8]);
            String deviation = hexStringtoDecString(nilai[9] + nilai[10]);
            String clockStatus = hexStringtoDecString(nilai[11]);
            System.out.println(hari + " " + tanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + menit
                    + ":" + detik + ":" + milidetik + " " + deviation + " " + clockStatus);
            System.out.println(tanggal + "-" + bulan + "-" + tahun + " " + jam + ":" + menit + ":00");
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    static String hexStringtoDecString(String hex) {
        try {
            if (hex.equals("FFFF") || hex.equals("FF")) {
                return hex.replace("F", "*");
            }
            if (hex.equals("00")) {
                return hex;
            }
            if (Integer.parseInt(hex, 16) < 10) {
                return Integer.parseInt(hex, 16) + " ";
            } else {
                return Integer.parseInt(hex, 16) + "";
            }
        } catch (Exception e) {
            return "*";
        }
    }

    /**
     * Fungsi untuk mencetak informasi object clock.
     *
     * @param clock GXDLMSClock yang akan dicetak informasinya.
     */
    public static void printClock(GXDLMSClock clock) {
        printObjectDefinition(clock);
        try {
            System.out.println("Time = " + clock.getTime());
            System.out.println("Time zone = " + clock.getTimeZone());
            System.out.println("Status = " + clock.getStatus());
            System.out.println("Daylight saving begin = " + clock.getBegin());
            System.out.println("Daylight saving end = " + clock.getEnd());
            System.out.println("Daylight saving deviation = " + clock.getDeviation());
            System.out.println("Daylight saving enabled is " + clock.getEnabled());
            System.out.println("Clock base = " + clock.getClockBase().toString());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi setting profile generic.
     *
     * @param profileGeneric GXDLMSProfileGeneric yang akan dicetak
     * informasinya.
     */
    public static void printProfileGenericSetting(GXDLMSProfileGeneric profileGeneric) {
        printObjectDefinition(profileGeneric);
        try {
            System.out.println("Capture Object =");
            System.out.println("\tLogical Name\tDeskripsi\tAttribute index\tData index");
            profileGeneric.getCaptureObjects().forEach((captureObject) -> {
                System.out.println("\t" + captureObject.getKey().getLogicalName() + "\t"
                        + ObisDescriptor.getDeskripsi(captureObject.getKey().getLogicalName()) + "\t"
                        + captureObject.getValue().getAttributeIndex() + "\t"
                        + captureObject.getValue().getDataIndex());
            });
            System.out.print("Capture period = ");
            if (profileGeneric.getCapturePeriod() < 0) {
                System.out.println("Wrong value, Capture period can't smaller than 0.");
            } else if (profileGeneric.getCapturePeriod() == 0) {
                System.out.println("No automatic capturing, capturing is triggered externally or capture events "
                        + "occur asynchronously.");
            } else {
                System.out.println(profileGeneric.getCapturePeriod() + "s");
            }
            System.out.println("Sort method = (" + profileGeneric.getSortMethod().getValue() + ")"
                    + profileGeneric.getSortMethod().toString());
            System.out.println("Sort object = " + profileGeneric.getSortObject().getLogicalName() + "("
                    + ObisDescriptor.getDeskripsi(profileGeneric.getSortObject().getLogicalName()) + ")");
            System.out.println("Entries in use = " + profileGeneric.getEntriesInUse());
            System.out.println("Profile entries = " + profileGeneric.getProfileEntries());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object register activation.
     *
     * @param registerActivation GXDLMSRegisterActivation yang akan dicetak
     * informasinya.
     */
    public static void printRegisterActivation(GXDLMSRegisterActivation registerActivation) {
        printObjectDefinition(registerActivation);
        try {
            System.out.println("Register assignment = ");
            System.out.println("\tLogical Name\tDeskripsi\tType");
            registerActivation.getRegisterAssignment().forEach((gXDLMSObjectDefinition) -> {
                System.out.println("\t" + gXDLMSObjectDefinition.getLogicalName() + "\t"
                        + ObisDescriptor.getDeskripsi(gXDLMSObjectDefinition.getLogicalName()) + "\t"
                        + gXDLMSObjectDefinition.getObjectType());
            });
            System.out.println("Mask list = ");
            registerActivation.getMaskList().forEach((entry) -> {
                System.out.println("\t" + entry.getKey() + "\t" + entry.getValue());
            });
            System.out.println("Active mask = " + registerActivation.getActiveMask());
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Fungsi untuk mencetak informasi object security setup.
     *
     * @param securitySetup GXDLMSSecuritySetup yang akan dicetak informasinya.
     */
    public static void printSecuritySetup(GXDLMSSecuritySetup securitySetup) {
        printObjectDefinition(securitySetup);
        try {
            if (securitySetup.getVersion() == 0) {
                System.out.println("Security policy = " + securitySetup.getSecurityPolicy0().toString());
            } else {
                System.out.println("Security policy = ");
                securitySetup.getSecurityPolicy().forEach((securityPolicy) -> {
                    System.out.println("\t" + securityPolicy.toString() + "(" + securityPolicy.getValue() + ")");
                });
            }
            System.out.println("Security suite = " + securitySetup.getSecuritySuite().toString());
            System.out.println("Client system title = " + new String(securitySetup.getClientSystemTitle()));
            System.out.println("Server system title = " + new String(securitySetup.getServerSystemTitle()));
            for (GXDLMSCertificateInfo certificate : securitySetup.getCertificates()) {
                System.out.println("{\tCertificate entity = " + certificate.getEntity().toString() + "("
                        + certificate.getEntity().getValue() + ")");
                System.out.println("\tCertificate type = " + certificate.getType().toString() + "("
                        + certificate.getType().getValue() + ")");
                System.out.println("\tSerial number = " + certificate.getSerialNumber());
                System.out.println("\tIssuer = " + certificate.getIssuer());
                System.out.println("\tSubject = " + certificate.getSubject());
                System.out.println("\tSubject alt name = " + certificate.getSubjectAltName() + "}");
            }
            System.out.println();
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }

    /**
     * Salinan dari method showValue() bawaan Gurux di class
     * gurux.dlms.client.GXDLMSReader. Hanya menambahkan return String.
     *
     * @param pos Index attribute.
     * @param value Hasil baca GXDLMSObject.
     *
     * @return Hasil baca dalam bentuk String.
     */
    public static String lihatNilai(final int pos, final Object value) {
        Object val = value;
        if (val instanceof byte[]) {
            val = new String((byte[]) val);
        } else if (val instanceof Double) {
            NumberFormat formatter = NumberFormat.getNumberInstance();
            val = formatter.format(val);
        } else if (val instanceof List) {
            StringBuilder sb = new StringBuilder();
            for (Object tmp : (List<?>) val) {
                if (sb.length() != 0) {
                    sb.append(";\n");
                }
                if (tmp instanceof byte[]) {
                    sb.append(new String((byte[]) tmp));
                } else {
                    if (tmp instanceof List) {
                        for (Object object : (List<?>) tmp) {
                            if (sb.length() != 0) {
                                sb.append(", ");
                            }
                            if (object instanceof byte[]) {
                                sb.append(new String((byte[]) object));
                            } else {
                                sb.append(String.valueOf(object));
                            }
                        }
                    } else {
                        sb.append(String.valueOf(tmp));
                    }
                }
            }
            val = sb.toString();
        } else if (val != null && val.getClass().isArray()) {
            StringBuilder sb = new StringBuilder();
            for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                Object tmp = Array.get(val, pos2);
                if (tmp instanceof byte[]) {
                    sb.append(new String((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        }
        return String.valueOf(val);
    }

    public static void printGXDLMSException(GXDLMSException exception) {
        switch (ErrorCode.forValue(exception.getErrorCode())) {
            case READ_WRITE_DENIED:
                LOG.log(Level.SEVERE, PrintUtil.FAIL_LOG, "Read with "
                        + Meter.authentication.toString() + "-level security is denied for this index");
                break;
            case ACCESS_VIOLATED:
                LOG.log(Level.SEVERE, PrintUtil.FAIL_LOG, "Akses pembacaan tidak aktif");
                break;
            case UNDEFINED_OBJECT:
                LOG.log(Level.SEVERE, PrintUtil.FAIL_LOG, "OBIS not found");
                break;
            default:
                LOG.log(Level.SEVERE, PrintUtil.FAIL_LOG, exception.toString());
                break;
        }
    }

//    /**
//     * Fungsi untuk mencetak informasi TOU.
//     *
//     * @param activityCalendar GXDLMSActivityCalendar yang akan dicetak
//     * informasinya.
//     */
//    public static void printTOU(GXDLMSActivityCalendar activityCalendar) {
//        printObjectDefinition(activityCalendar);
//        try {
//            String timeString;
//            for (GXDLMSDayProfile dayProfile : activityCalendar.getDayProfileTableActive()) {
//                System.out.println("Day ID = " + dayProfile.getDayId());
//                if (dayProfile.getDaySchedules().length == 1) {
//                    for (GXDLMSDayProfileAction profileAction : dayProfile.getDaySchedules()) {
//                        Calendar c = profileAction.getStartTime().getLocalCalendar();
//                        System.out.print("\t" + "Start Time = " + c.get(Calendar.HOUR) + ":"
//                                + (c.get(Calendar.MINUTE) < 10
//                                ? ("0" + c.get(Calendar.MINUTE)) : c.get(Calendar.MINUTE)) + ":"
//                                + (c.get(Calendar.SECOND) < 10
//                                ? ("0" + c.get(Calendar.SECOND)) : c.get(Calendar.SECOND))
//                                + (c.get(Calendar.AM_PM) == 0
//                                ? " AM" : " PM"));
//                        c.setTimeInMillis(c.getTimeInMillis() - 1000);
//                        System.out.print("\t\tEnd Time = " + c.get(Calendar.HOUR) + ":"
//                                + (c.get(Calendar.MINUTE) < 10
//                                ? ("0" + c.get(Calendar.MINUTE)) : c.get(Calendar.MINUTE)) + ":"
//                                + (c.get(Calendar.SECOND) < 10
//                                ? ("0" + c.get(Calendar.SECOND)) : c.get(Calendar.SECOND))
//                                + (c.get(Calendar.AM_PM) == 0
//                                ? " AM" : " PM"));
//                    }
//                } else {
//                    HashMap<Integer, String[]> daftarTarif = new HashMap<>();
//                    for (int i = 0; i < dayProfile.getDaySchedules().length; i++) {
//                        if (i == dayProfile.getDaySchedules().length - 1) {
//                            Calendar startTime = dayProfile.getDaySchedules()[i].getStartTime().getLocalCalendar();
//                            Calendar endTime = dayProfile.getDaySchedules()[0].getStartTime().getLocalCalendar();
//                            endTime.setTimeInMillis(endTime.getTimeInMillis() - 1000);
//                            timeString = startTime.get(Calendar.HOUR_OF_DAY) + ":"
//                                    + (startTime.get(Calendar.MINUTE) < 10
//                                    ? ("0" + startTime.get(Calendar.MINUTE)) : startTime.get(Calendar.MINUTE)) + ":"
//                                    + (startTime.get(Calendar.SECOND) < 10
//                                    ? ("0" + startTime.get(Calendar.SECOND)) : startTime.get(Calendar.SECOND))
//                                    + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : "PM") + " s/d "
//                                    + endTime.get(Calendar.HOUR) + ":"
//                                    + (endTime.get(Calendar.MINUTE) < 10
//                                    ? ("0" + endTime.get(Calendar.MINUTE)) : endTime.get(Calendar.MINUTE)) + ":"
//                                    + (endTime.get(Calendar.SECOND) < 10
//                                    ? ("0" + endTime.get(Calendar.SECOND)) : endTime.get(Calendar.SECOND)
//                                    + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : "PM"));
//                        } else {
//                            Calendar startTime = dayProfile.getDaySchedules()[i].getStartTime().getLocalCalendar();
//                            Calendar endTime = dayProfile.getDaySchedules()[i + 1].getStartTime().getLocalCalendar();
//                            endTime.setTimeInMillis(endTime.getTimeInMillis() - 1000);
//                            timeString = startTime.get(Calendar.HOUR) + ":"
//                                    + (startTime.get(Calendar.MINUTE) < 10
//                                    ? ("0" + startTime.get(Calendar.MINUTE)) : startTime.get(Calendar.MINUTE)) + ":"
//                                    + (startTime.get(Calendar.SECOND) < 10
//                                    ? ("0" + startTime.get(Calendar.SECOND)) : startTime.get(Calendar.SECOND))
//                                    + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : "PM") + " s/d "
//                                    + endTime.get(Calendar.HOUR) + ":"
//                                    + (endTime.get(Calendar.MINUTE) < 10
//                                    ? ("0" + endTime.get(Calendar.MINUTE)) : endTime.get(Calendar.MINUTE)) + ":"
//                                    + (endTime.get(Calendar.SECOND) < 10
//                                    ? ("0" + endTime.get(Calendar.SECOND)) : endTime.get(Calendar.SECOND)
//                                    + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : "PM"));
//                        }
//                        if (daftarTarif.containsKey(dayProfile.getDaySchedules()[i].getScriptSelector())) {
//                            String[] strings = new String[daftarTarif.get(dayProfile.getDaySchedules()[i].getScriptSelector()).length + 1];
//                            for (int j = 0; j < strings.length; j++) {
//                                if (j == strings.length - 1) {
//                                    strings[j] = timeString;
//                                } else {
//                                    strings[j] = daftarTarif.get(dayProfile.getDaySchedules()[i].getScriptSelector())[j];
//                                }
//                            }
//                            daftarTarif.replace(dayProfile.getDaySchedules()[i].getScriptSelector(), strings);
//                        } else {
//                            String[] strings = {timeString};
//                            daftarTarif.put(dayProfile.getDaySchedules()[i].getScriptSelector(), strings);
//                        }
//                    }
//                    for (Map.Entry<Integer, String[]> entry : daftarTarif.entrySet()) {
//                        if (entry.getKey() == 1) {
//                            System.out.print("\tWBP = {");
//                            int dataKe = 1;
//                            for (String string : entry.getValue()) {
//                                if (dataKe == 1) {
//                                    System.out.print(string);
//                                    dataKe++;
//                                } else {
//                                    System.out.print(", " + string);
//                                }
//                            }
//                            System.out.println("}");
//                        } else {
//                            if (daftarTarif.size() == 2) {
//                                System.out.print("\tLWBP = {");
//                                int dataKe = 1;
//                                for (String string : entry.getValue()) {
//                                    if (dataKe == 1) {
//                                        System.out.print(string);
//                                        dataKe++;
//                                    } else {
//                                        System.out.print(", " + string);
//                                    }
//                                }
//                                System.out.println("}");
//                            } else {
//                                System.out.print("\tLWBP" + (entry.getKey() - 1) + " = {");
//                                int dataKe = 1;
//                                for (String string : entry.getValue()) {
//                                    if (dataKe == 1) {
//                                        System.out.print(string);
//                                        dataKe++;
//                                    } else {
//                                        System.out.print(", " + string);
//                                    }
//                                }
//                                System.out.println("}");
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception e) {
//            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
//        }
//    }
    /**
     * Fungsi untuk mencetak informasi TOU.
     *
     * @param activityCalendar GXDLMSActivityCalendar yang akan dicetak
     * informasinya.
     */
    public static void printTOU(GXDLMSActivityCalendar activityCalendar) {
        printObjectDefinition(activityCalendar);
        try {
            String timeString;
            HashMap<String, String> daftarTarif = new HashMap<>();
            for (int i = 0; i < activityCalendar.getDayProfileTableActive()[0].getDaySchedules().length; i++) {
                if (i == activityCalendar.getDayProfileTableActive()[0].getDaySchedules().length - 1) {
                    Calendar startTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[i].getStartTime().getLocalCalendar();
                    Calendar endTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[0].getStartTime().getLocalCalendar();
                    endTime.setTimeInMillis(endTime.getTimeInMillis() - 1000);
                    timeString = startTime.get(Calendar.HOUR) + ":"
                            + (startTime.get(Calendar.MINUTE) < 10
                            ? ("0" + startTime.get(Calendar.MINUTE)) : startTime.get(Calendar.MINUTE)) + ":"
                            + (startTime.get(Calendar.SECOND) < 10
                            ? ("0" + startTime.get(Calendar.SECOND)) : startTime.get(Calendar.SECOND))
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM") + " s/d "
                            + endTime.get(Calendar.HOUR) + ":"
                            + (endTime.get(Calendar.MINUTE) < 10
                            ? ("0" + endTime.get(Calendar.MINUTE)) : endTime.get(Calendar.MINUTE)) + ":"
                            + (endTime.get(Calendar.SECOND) < 10
                            ? ("0" + endTime.get(Calendar.SECOND)) : endTime.get(Calendar.SECOND)
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM"));
                } else {
                    Calendar startTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[i].getStartTime().getLocalCalendar();
                    Calendar endTime = activityCalendar.getDayProfileTableActive()[0].
                            getDaySchedules()[i + 1].getStartTime().getLocalCalendar();
                    endTime.setTimeInMillis(endTime.getTimeInMillis() - 1000);
                    timeString = startTime.get(Calendar.HOUR) + ":"
                            + (startTime.get(Calendar.MINUTE) < 10
                            ? ("0" + startTime.get(Calendar.MINUTE)) : startTime.get(Calendar.MINUTE)) + ":"
                            + (startTime.get(Calendar.SECOND) < 10
                            ? ("0" + startTime.get(Calendar.SECOND)) : startTime.get(Calendar.SECOND))
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM") + " s/d "
                            + endTime.get(Calendar.HOUR) + ":"
                            + (endTime.get(Calendar.MINUTE) < 10
                            ? ("0" + endTime.get(Calendar.MINUTE)) : endTime.get(Calendar.MINUTE)) + ":"
                            + (endTime.get(Calendar.SECOND) < 10
                            ? ("0" + endTime.get(Calendar.SECOND)) : endTime.get(Calendar.SECOND)
                            + (startTime.get(Calendar.AM_PM) == 0 ? " AM" : " PM"));
                }
                if (activityCalendar.getDayProfileTableActive()[0].getDaySchedules()[i].getScriptSelector() == 1
                        || activityCalendar.getDayProfileTableActive()[0].getDaySchedules()[i].
                                getScriptSelector() == 2) {
                    String wbp = "";
                    if (activityCalendar.getDayProfileTableActive()[0].getDaySchedules()[i].getScriptSelector() == 1) {
                        wbp = "WBP";
                    } else {
                        wbp = "LWBP";
                    }
                    if (daftarTarif.containsKey(wbp)) {
                        daftarTarif.replace(wbp, daftarTarif.get(wbp) + ", " + timeString);
                    } else {
                        daftarTarif.put(wbp, timeString);
                    }
                }
            }
            for (Map.Entry<String, String> entry : daftarTarif.entrySet()) {
                System.out.println(entry.getKey() + " : " + entry.getValue());
            }
        } catch (Exception e) {
            LOG.log(Level.INFO, FAIL_LOG, "Cetak value error @" + e.toString());
        }
    }
}
