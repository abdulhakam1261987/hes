/*
 * 
 */
package gurux.dlms.client.Global.util.fieldname;

import java.util.logging.Logger;

/**
 * Class Untuk mendeskripsikan OBIS field name
 *
 * @author Tab Solutions
 */
public class ObisFieldName {

    private static int a;
    private static int b;
    private static int c;
    private static int d;
    private static int e;
    private static int f;

    private static final Logger LOG = Logger.getLogger(ObisFieldName.class.getName());

    /**
     * Getting class log
     *
     * @return Logger class
     */
    public static Logger getLOG() {
        return LOG;
    }

    /**
     * Mendeskripsikan OBIS field name.
     *
     * @param logicalName Logical name dari OBIS.
     * @return Field name OBIS dalam bentuk String.
     */
    public static String getFieldName(String logicalName) {
        int titikKe = 0;
        String as = "";
        String bs = "";
        String cs = "";
        String ds = "";
        String es = "";
        String fs = "";
        for (int i = 0; i < logicalName.length(); i++) {
            if (logicalName.substring(i, i + 1).equals(".")) {
                titikKe++;
                continue;
            }
            switch (titikKe) {
                case 0:
                    as = as + logicalName.substring(i, i + 1);
                    break;
                case 1:
                    bs = bs + logicalName.substring(i, i + 1);
                    break;
                case 2:
                    cs = cs + logicalName.substring(i, i + 1);
                    break;
                case 3:
                    ds = ds + logicalName.substring(i, i + 1);
                    break;
                case 4:
                    es = es + logicalName.substring(i, i + 1);
                    break;
                case 5:
                    fs = fs + logicalName.substring(i, i + 1);
                    break;
                default:
                    break;
            }
        }
        a = Integer.parseInt(as);
        b = Integer.parseInt(bs);
        c = Integer.parseInt(cs);
        d = Integer.parseInt(ds);
        e = Integer.parseInt(es);
        f = Integer.parseInt(fs);
        return getFieldName().replace(" ", "_");
    }

    /**
     * Mendeskripsikan OBIS field name.
     *
     * @return Field name OBIS dalam bentuk String.
     */
    static String getFieldName() {
        if (b >= 128 && b <= 199) {
            return "MAN_SPEC B" + b + " (ref: 7.2.2)";
        } else if (b >= 65 && b <= 127) {
            return "UTIL_SPEC B" + b + " (ref: 7.3.2)";
        } else if (b >= 200 && b <= 255) {
            return "B-Reserved B" + b + " (ref: 7.3.2)";
        } else if ((c >= 128 && c <= 199) || (c == 240)) {
            return "MAN_SPEC C" + c + " (ref: 7.2.2)";
        } else if (d >= 128 && d <= 254) {
            return "MAN_SPEC D" + d + " (ref: 7.2.2)";
        } else if (e >= 128 && e <= 254) {
            return "MAN_SPEC E" + e + " (ref: 7.2.2)";
        } else if (f >= 128 && f <= 254) {
            return "MAN_SPEC F" + f + " (ref: 7.2.2)";
        } else {
            switch (a) {
                case 0:
                    return DLMSGeneralFieldName.get(a, b, c, d, e, f).trim().replaceFirst(
                            DLMSGeneralFieldName.get(a, b, c, d, e, f).substring(0, 1),
                            DLMSGeneralFieldName.get(a, b, c, d, e, f).substring(0, 1).toUpperCase());
                case 1:
                    if ((c >= 100 && c <= 127) || (c >= 200 && c <= 239) || c >= 241 || c == 95) {
                        return "RSV (ref: 7.5.1)";
                    } else {
                        return DLMSElectricityFieldName.get(a, b, c, d, e, f).trim().replaceFirst(
                                DLMSElectricityFieldName.get(a, b, c, d, e, f).substring(0, 1),
                                DLMSElectricityFieldName.get(a, b, c, d, e, f).substring(0, 1).toUpperCase());
                    }
                default:
                    return "SKIPPED";
            }
        }
    }
}
