/*
 * 
 */
package gurux.dlms.client.Global.util.fieldname;

import gurux.dlms.client.Global.util.*;

/**
 * Define electricity OBJ (A is 1) field name on DLMS
 *
 * @author Tab Solutions
 */
class DLMSElectricityFieldName {

    private static int a;
    private static int b;
    private static int c;
    private static int d;
    private static int e;
    private static int f;

    public static String get(int aValue, int bValue, int cValue, int dValue, int eValue, int fValue) {
        a = aValue;
        b = bValue;
        c = cValue;
        d = dValue;
        e = eValue;
        f = fValue;
        String rslt = "";
        if (c == 0) {
            switch (d) {
                case 0:
                    if (e >= 0 && e <= 9) {
                        rslt = "Electricity ID " + (e + 1);
                    } else if (e == 255) {
                        rslt = "Complete combined electricity ID";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 1:
                    if (e == 0 && ((f >= 0 && f <= 99) || f == 255)) {
                        rslt = "BILL period counter #" + f;
                    } else if (e == 1 && f == 255) {
                        rslt = "No. of available BILL periods";
                    } else if (e == 2 && f >= 0 && f <= 99) {
                        rslt = "Time stamp of the BILL period #" + f;
                    } else if (e == 2 && f == 255) {
                        rslt = "Time stamp of the most recent BILL period closed";
                    } else if (e == 3 && ((f >= 0 && f <= 99) || f == 255)) {
                        rslt = "BILL period counter kedua #" + f;
                    } else if (e == 4 && f == 255) {
                        rslt = "No. of available BILL periods kedua";
                    } else if (e == 5 && f >= 0 && f <= 99) {
                        rslt = "Time stamp of the BILL period kedua #" + f;
                    } else if (e == 5 && f == 255) {
                        rslt = "Time stamp of the most recent BILL period closed";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 2:
                    if (e == 0 && f == 255) {
                        rslt = "Electricity firmware ID";
                    } else if (e == 1 && f == 1) {
                        rslt = "Parameter number line 1";
                    } else if (e == 1 && f >= 2 && f <= 127) {
                        rslt = "RSV for future use (Ref: 7.5.5.1)";
                    } else if (e == 2 && f == 255) {
                        rslt = "Time switch program number";
                    } else if (e == 3 && f == 255) {
                        rslt = "RCR program number";
                    } else if (e == 4 && f == 255) {
                        rslt = "Meter connection diagram ID";
                    } else if (e == 7 && f == 255) {
                        rslt = "Passive calendar name";
                    } else if (e == 8 && f == 255) {
                        rslt = "Electricity firmware signature";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 3:
                    if (e == 0 && f == 255) {
                        rslt = "CONST EN AK, metrological LED";
                    } else if (e == 1 && f == 255) {
                        rslt = "CONST EN RE, metrological LED";
                    } else if (e == 2 && f == 255) {
                        rslt = "CONST AP EN, metrological LED";
                    } else if (e == 3 && f == 255) {
                        rslt = "CONST EN AK, output pulse";
                    } else if (e == 4 && f == 255) {
                        rslt = "CONST EN RE, output pulse";
                    } else if (e == 5 && f == 255) {
                        rslt = "CONST AP EN, output pulse";
                    } else if (e == 6 && f == 255) {
                        rslt = "CONST VOLT-squared hours, metrological LED";
                    } else if (e == 7 && f == 255) {
                        rslt = "CONST AMP-squared hours, metrological LED";
                    } else if (e == 8 && f == 255) {
                        rslt = "CONST VOLT-squared hours, output pulse";
                    } else if (e == 9 && f == 255) {
                        rslt = "CONST AMP-squared hours, output pulse";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 4:
                    if (e == 0 && f == 255) {
                        rslt = "Faktor baca DA";
                    } else if (e == 1 && f == 255) {
                        rslt = "Faktor baca EN";
                    } else if (e == 2 && ((f >= 0 && f <= 99) || f == 255)) {
                        switch (f) {
                            case 0:
                                rslt = "Pembilang CT";
                                break;
                            case 255:
                                rslt = "Pembilang CT (numerator)";
                                break;
                            default:
                                rslt = "Pembilang CT " + f;
                                break;
                        }
                    } else if (e == 3 && ((f >= 0 && f <= 99) || f == 255)) {
                        switch (f) {
                            case 0:
                                rslt = "Pembilang VT";
                                break;
                            case 255:
                                rslt = "Pembilang VT (numerator)";
                                break;
                            default:
                                rslt = "Pembilang VT " + f;
                                break;
                        }
                    } else if (e == 4 && ((f >= 0 && f <= 99) || f == 255)) {
                        switch (f) {
                            case 0:
                                rslt = "Pembilang meter rasio";
                                break;
                            case 255:
                                rslt = "Pembilang meter rasio (numerator)";
                                break;
                            default:
                                rslt = "Pembilang meter rasio " + f;
                                break;
                        }
                    } else if (e == 5 && ((f >= 0 && f <= 99) || f == 255)) {
                        switch (f) {
                            case 0:
                                rslt = "Penyebut CT";
                                break;
                            case 255:
                                rslt = "Penyebut CT (denominator)";
                                break;
                            default:
                                rslt = "Penyebut CT " + f;
                                break;
                        }
                    } else if (e == 6 && ((f >= 0 && f <= 99) || f == 255)) {
                        switch (f) {
                            case 0:
                                rslt = "Penyebut VT";
                                break;
                            case 255:
                                rslt = "Penyebut VT (denominator)";
                                break;
                            default:
                                rslt = "Penyebut VT " + f;
                                break;
                        }
                    } else if (e == 7 && ((f >= 0 && f <= 99) || f == 255)) {
                        switch (f) {
                            case 0:
                                rslt = "Penyebut meter rasio";
                                break;
                            case 255:
                                rslt = "Penyebut meter rasio (denominator)";
                                break;
                            default:
                                rslt = "Penyebut meter rasio " + f;
                                break;
                        }
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 5:
                    if (e == 1 && f >= 1 && f <= 4) {
                        rslt = "DE limit " + f;
                    } else if (e == 2 && f >= 1 && f <= 9) {
                        rslt = "DE limit rate " + f;
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 6:
                    if (e == 0 && f == 255) {
                        rslt = "Nominal VOLT";
                    } else if (e == 1 && f == 255) {
                        rslt = "Basic/nominal CURR";
                    } else if (e == 2 && f == 255) {
                        rslt = "Nominal FREQ";
                    } else if (e == 3 && f == 255) {
                        rslt = "AR MAKS";
                    } else if (e == 4 && ((f >= 0 && f <= 99) || f == 255)) {
                        rslt = "Reference VOLT for PWR QW MEAS";
                    } else if (e == 5 && ((f >= 0 && f <= 99) || f == 255)) {
                        rslt = "Reference VOLT of aux. PWR supply";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 7:
                    if (e == 0 && f == 255) {
                        rslt = "Input pulse values or CONST, EN AK";
                    } else if (e == 1 && f == 255) {
                        rslt = "Input pulse values or CONST, EN RE";
                    } else if (e == 2 && f == 255) {
                        rslt = "Input pulse values or CONST, AP EN";
                    } else if (e == 3 && f == 255) {
                        rslt = " Input pulse values or CONST, VOLT-squared hour";
                    } else if (e == 4 && f == 255) {
                        rslt = " Input pulse values or CONST, AMP-squared hour";
                    } else if (e == 5 && f == 255) {
                        rslt = " Input pulse values or CONST, unitless quantities";
                    } else if (e == 10 && f == 255) {
                        rslt = "Input pulse values or CONST, EN AK kirim";
                    } else if (e == 11 && f == 255) {
                        rslt = "Input pulse values or CONST, EN RE kirim";
                    } else if (e == 12 && f == 255) {
                        rslt = "Input pulse values or CONST, AP EN kirim";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 8:
                    if (e == 0 && f == 255) {
                        rslt = "MEAS period 1,  for AVG scheme 1";
                    } else if (e == 1 && f == 255) {
                        rslt = "MEAS period 2,  for AVG scheme 2";
                    } else if (e == 2 && f == 255) {
                        rslt = "MEAS period 3,  for INST value";
                    } else if (e == 3 && f == 255) {
                        rslt = "MEAS period 4 for test value";
                    } else if (e == 4 && f == 255) {
                        rslt = "Recording interval 1, for LP";
                    } else if (e == 5 && f == 255) {
                        rslt = "Recording interval 2, for LP";
                    } else if (e == 6 && f == 255) {
                        rslt = "BILL period (BILL period 1 if there are two BILL periods)";
                    } else if (e == 7 && f == 255) {
                        rslt = "BILL period 2";
                    } else if (e == 8 && f == 255) {
                        rslt = "MEAS period 4,  for harmonics";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 9:
                    if (e == 0 && f == 255) {
                        rslt = "Time expired since last end of BILL period";
                    } else if (e == 1 && f == 255) {
                        rslt = "Local time";
                    } else if (e == 2 && f == 255) {
                        rslt = "Local date";
                    } else if (e == 3 && f == 255) {
                        rslt = "RSV for Germany (Ref: 7.5.5.1)";
                    } else if (e == 4 && f == 255) {
                        rslt = "RSV for Germany (Ref: 7.5.5.1)";
                    } else if (e == 5 && f == 255) {
                        rslt = "Week day (0...7)";
                    } else if (e == 6 && f == 255) {
                        rslt = "Time of last reset";
                    } else if (e == 7 && f == 255) {
                        rslt = "Tanggal reset terakhir kali";
                    } else if (e == 8 && f == 255) {
                        rslt = "Output pulse duration";
                    } else if (e == 9 && f == 255) {
                        rslt = "Clock synchronisation window";
                    } else if (e == 10 && f == 255) {
                        rslt = "Clock synchronisation method";
                    } else if (e == 11 && f == 255) {
                        rslt = "Clock time shift limit";
                    } else if (e == 12 && f == 255) {
                        rslt = "BILL period reset lockout time";
                    } else if (e == 13 && f == 255) {
                        rslt = "Time expired since last end of BILL period";
                    } else if (e == 14 && f == 255) {
                        rslt = "Time of last reset (Second BILL period scheme)";
                    } else if (e == 15 && f == 255) {
                        rslt = "Date of last reset (Second BILL period scheme)";
                    } else if (e == 16 && f == 255) {
                        rslt = "BILL period reset lockout time";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 10:
                    if (e == 0 && f == 255) {
                        rslt = "Transformer magnetic LOSS Xm";
                    } else if (e == 1 && f == 255) {
                        rslt = "Transformer iron LOSS RFe";
                    } else if (e == 2 && f == 255) {
                        rslt = "Line resistance LOSS RCu";
                    } else if (e == 3 && f == 255) {
                        rslt = "Line reactance LOSS Xs";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                case 11:
                    if (e == 1 && f == 255) {
                        rslt = "Algoritma pengukuran DA AK";
                    } else if (e == 2 && f == 255) {
                        rslt = "Algoritma pengukuran EN AK";
                    } else if (e == 3 && f == 255) {
                        rslt = "Algoritma pengukuran DA RE";
                    } else if (e == 4 && f == 255) {
                        rslt = "Algoritma pengukuran EN RE";
                    } else if (e == 5 && f == 255) {
                        rslt = "Algoritma pengukuran DA AP";
                    } else if (e == 6 && f == 255) {
                        rslt = "Algoritma pengukuran EN AP";
                    } else if (e == 7 && f == 255) {
                        rslt = "Algoritma pengukuran perhitung faktor DA";
                    } else {
                        return "RSV (Ref: 7.5.5.1)";
                    }
                    break;
                default:
                    return "RSV (Ref: 7.5.5.1)";
            }
        } else if (c == 81) {
            if (d == 7 && f == 255) {
                rslt = getPhaseAngleMEAS();
            } else {
                return "RSV (Ref: 7.5.3.4)";
            }
        } else if (c == 83) {
            if (((d >= 0 && d <= 30) || d == 55 || d == 58) && f == 255) {
                switch (e) {
                    case 1:
                        rslt = "AK line LOSS+";
                        break;
                    case 2:
                        rslt = "AK line LOSS-";
                        break;
                    case 3:
                        rslt = "AK line LOSS";
                        break;
                    case 4:
                        rslt = "AK transformer LOSS+";
                        break;
                    case 5:
                        rslt = "AK transformer LOSS-";
                        break;
                    case 6:
                        rslt = "AK transformer LOSS";
                        break;
                    case 7:
                        rslt = "AK LOSS+";
                        break;
                    case 8:
                        rslt = "AK LOSS-";
                        break;
                    case 9:
                        rslt = "AK LOSS";
                        break;
                    case 10:
                        rslt = "RE line LOSS+";
                        break;
                    case 11:
                        rslt = "RE line LOSS-";
                        break;
                    case 12:
                        rslt = "RE line LOSS";
                        break;
                    case 13:
                        rslt = "RE transformer LOSS+";
                        break;
                    case 14:
                        rslt = "RE transformer LOSS-";
                        break;
                    case 15:
                        rslt = "RE transformer LOSS";
                        break;
                    case 16:
                        rslt = "RE LOSS+";
                        break;
                    case 17:
                        rslt = "RE LOSS-";
                        break;
                    case 18:
                        rslt = "RE LOSS";
                        break;
                    case 19:
                        rslt = "TOT transformer LOSS with normalised RFE = 1 MOhm";
                        break;
                    case 20:
                        rslt = "TOT line LOSS with normalised RCU = 10 Ohm";
                        break;
                    case 21:
                        rslt = "Compensated AK gross +";
                        break;
                    case 22:
                        rslt = "Compensated AK net +";
                        break;
                    case 23:
                        rslt = "Compensated AK gross -";
                        break;
                    case 24:
                        rslt = "Compensated AK net -";
                        break;
                    case 25:
                        rslt = "Compensated RE gross +";
                        break;
                    case 26:
                        rslt = "Compensated RE net +";
                        break;
                    case 27:
                        rslt = "Compensated RE gross -";
                        break;
                    case 28:
                        rslt = "Compensated RE net -";
                        break;
                    case 31:
                        rslt = "AK line LOSS+ R";
                        break;
                    case 32:
                        rslt = "AK line LOSS- R";
                        break;
                    case 33:
                        rslt = "AK line LOSS R";
                        break;
                    case 34:
                        rslt = "AK transformer LOSS+ R";
                        break;
                    case 35:
                        rslt = "AK transformer LOSS- R";
                        break;
                    case 36:
                        rslt = "AK transformer LOSS R";
                        break;
                    case 37:
                        rslt = "AK LOSS+ R";
                        break;
                    case 38:
                        rslt = "AK LOSS- R";
                        break;
                    case 39:
                        rslt = "AK LOSS R";
                        break;
                    case 40:
                        rslt = "RE line LOSS+ R";
                        break;
                    case 41:
                        rslt = "RE line LOSS- R";
                        break;
                    case 42:
                        rslt = "RE line LOSS R";
                        break;
                    case 43:
                        rslt = "RE transformer LOSS+ R";
                        break;
                    case 44:
                        rslt = "RE transformer LOSS- R";
                        break;
                    case 45:
                        rslt = "RE transformer LOSS R";
                        break;
                    case 46:
                        rslt = "RE LOSS+ R";
                        break;
                    case 47:
                        rslt = "RE LOSS- R";
                        break;
                    case 48:
                        rslt = "RE LOSS R";
                        break;
                    case 49:
                        rslt = "A2h R";
                        break;
                    case 50:
                        rslt = "V2h R";
                        break;
                    case 51:
                        rslt = "AK line LOSS+ S";
                        break;
                    case 52:
                        rslt = "AK line LOSS- S";
                        break;
                    case 53:
                        rslt = "AK line LOSS S";
                        break;
                    case 54:
                        rslt = "AK transformer LOSS+ S";
                        break;
                    case 55:
                        rslt = "AK transformer LOSS- S";
                        break;
                    case 56:
                        rslt = "AK transformer LOSS S";
                        break;
                    case 57:
                        rslt = "AK LOSS+ S";
                        break;
                    case 58:
                        rslt = "AK LOSS- S";
                        break;
                    case 59:
                        rslt = "AK LOSS S";
                        break;
                    case 60:
                        rslt = "RE line LOSS+ S";
                        break;
                    case 61:
                        rslt = "RE line LOSS- S";
                        break;
                    case 62:
                        rslt = "RE line LOSS S";
                        break;
                    case 63:
                        rslt = "RE transformer LOSS+ S";
                        break;
                    case 64:
                        rslt = "RE transformer LOSS- S";
                        break;
                    case 65:
                        rslt = "RE transformer LOSS S";
                        break;
                    case 66:
                        rslt = "RE LOSS+ S";
                        break;
                    case 67:
                        rslt = "RE LOSS- S";
                        break;
                    case 68:
                        rslt = "RE LOSS S";
                        break;
                    case 69:
                        rslt = "A2h S";
                        break;
                    case 70:
                        rslt = "V2h S";
                        break;
                    case 71:
                        rslt = "AK line LOSS+ T";
                        break;
                    case 72:
                        rslt = "AK line LOSS- T";
                        break;
                    case 73:
                        rslt = "AK line LOSS T";
                        break;
                    case 74:
                        rslt = "AK transformer LOSS+ T";
                        break;
                    case 75:
                        rslt = "AK transformer LOSS- T";
                        break;
                    case 76:
                        rslt = "AK transformer LOSS T";
                        break;
                    case 77:
                        rslt = "AK LOSS+ T";
                        break;
                    case 78:
                        rslt = "AK LOSS- T";
                        break;
                    case 79:
                        rslt = "AK LOSS T";
                        break;
                    case 80:
                        rslt = "RE line LOSS+ T";
                        break;
                    case 81:
                        rslt = "RE line LOSS- T";
                        break;
                    case 82:
                        rslt = "RE line LOSS T";
                        break;
                    case 83:
                        rslt = "RE transformer LOSS+ T";
                        break;
                    case 84:
                        rslt = "RE transformer LOSS- T";
                        break;
                    case 85:
                        rslt = "RE transformer LOSS T";
                        break;
                    case 86:
                        rslt = "RE LOSS+ T";
                        break;
                    case 87:
                        rslt = "RE LOSS- T";
                        break;
                    case 88:
                        rslt = "RE LOSS T";
                        break;
                    case 89:
                        rslt = "A2h T";
                        break;
                    case 90:
                        rslt = "V2h T";
                        break;
                    default:
                        return "RSV (Ref: 7.5.3.5)";
                }
            } else {
                return "RSV (Ref: 7.5.3.5)";
            }
        } else if (c == 94) {
            if (CountrySpesificIdentifier.getMappings().containsKey(d)) {
                rslt = "Electricity ID for " + CountrySpesificIdentifier.forValue(d).toString() + " " + e
                        + " " + f;
            } else {
                return "RSV (Ref: 7.3.4.3)";
            }
        } else if (c == 96) {
            if (d == 1 && e >= 0 && e <= 9 && f == 255) {
                rslt = "Metering point ID combined " + (e + 1);
            } else if (d == 1 && e == 255 && f == 255) {
                rslt = "Metering point ID combined " + (e + 1);
            } else if (d == 5 && f == 255) {
                switch (e) {
                    case 0:
                        rslt = "Internal operating STAT, global";
                        break;
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        rslt = "Internal operating STAT (STAT word " + e + ")";
                        break;
                    case 5:
                        rslt = "Meter started STAT flag";
                        break;
                    default:
                        return "RSV (Ref: 7.5.5.1)";
                }
            } else if (d == 10 && e == 0 && f == 255) {
                rslt = "STAT INF missing VOLT";
            } else if (d == 10 && e == 1 && f == 255) {
                rslt = "STAT INF missing CURR";
            } else if (d == 10 && e == 2 && f == 255) {
                rslt = "STAT INF CURR without VOLT";
            } else if (d == 10 && e == 3 && f == 255) {
                rslt = "STAT INF aux. PWR supply";
            } else if (d >= 50 && d <= 99) {
                rslt = "Electricity MAN SPEC " + (d - 49) + " (Ref: 7.5.5.1)";
            } else {
                return "RSV (Ref: 7.5.5.1)";
            }
        } else if (c == 97) {
            if (d == 97 && e >= 0 && e <= 9 && f == 255) {
                rslt = "Error OBJ " + (e + 1);
            } else if (d == 97 && e == 255 && f == 255) {
                rslt = "Error profile OBJ";
            } else {
                return "RSV (Ref: 7.5.5.2)";
            }
        } else if (c == 98) {
            if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                rslt = "BILL ke-" + (e + 1) + " periode " + f;
            } else if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                rslt = "BILL ke-" + (e + 1) + " " + (f - 100) + " periode sebelumnya";
            } else if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                rslt = "BILL ke " + (e + 1) + " dengan periode yang tidak ditentukan";
            } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                rslt = "BILL ke-" + (e + 1) + " scheme 2 periode " + f;
            } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                rslt = "BILL ke-" + (e + 1) + " scheme 2(" + (f - 100) + " periode sebelumnya)";
            } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                rslt = "BILL ke-" + (e + 1) + " scheme 2 dengan periode yang tidak ditentukan";
            } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                rslt = "Register table OBJs, general use " + (e + 1);
            } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                rslt = "Register table OBJs, general use " + (e + 1)
                        + "(" + (f - 100) + " periode sebelumnya)";
            } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                rslt = "Register table OBJs, general use dengan periode yang tidak ditentukan" + (e + 1);
            } else {
                return "RSV (Ref: 7.4.3/7.4.4)";
            }
        } else if (c == 99) {
            if (d == 1 && e >= 0 && e <= 127 && f == 255) {
                if (e == 0) {
                    rslt = "LP";
                } else {
                    rslt = "LP ke-" + (e + 1);
                }
            } else if (d == 2 && e >= 0 && e <= 127 && f == 255) {
                if (e == 0) {
                    rslt = "LP periode 2";
                } else {
                    rslt = "LP periode 2 ke-" + (e + 1);
                }
            } else if (d == 3 && e == 0 && f == 255) {
                rslt = "LP during test";
            } else if (d == 10 && e == 1 && f == 255) {
                rslt = "Dips VOLT profile";
            } else if (d == 10 && e == 2 && f == 255) {
                rslt = "Swells VOLT profile";
            } else if (d == 10 && e == 3 && f == 255) {
                rslt = "Cuts VOLT profile";
            } else if (d == 11 && ((e >= 1 && e <= 120) || (e >= 124 && e <= 127)) && f == 255) {
                rslt = "VOLT harmonic profile " + e;
            } else if (d == 12 && ((e >= 1 && e <= 120) || (e >= 124 && e <= 127)) && f == 255) {
                rslt = "CURR harmonic profile " + e;
            } else if (d == 13 && e == 0 && f == 255) {
                rslt = "VOLT unbalance profile";
            } else if (d == 97 && e >= 0 && e <= 127 && f == 255) {
                rslt = "PWR failure event log " + (e + 1);
            } else if (d == 98 && e >= 0 && e <= 127 && f == 255) {
                rslt = "Event log " + (e + 1);
            } else if (d == 99 && e >= 0 && e <= 127 && f == 255) {
                rslt = "Certification data log " + (e + 1);
            } else {
                return "RSV (Ref: 7.4.3/7.4.4)";
            }
        } else if (c >= 1 && c <= 92) {
            rslt += ElectricityCFieldName.forValue(c).getString();
            if ((c % 20 != 11) && (c % 20 != 12) && (c % 20 != 13) && (c % 20 != 14) && (c >= 1 && c <= 80)) {
                if (d == 8 || d == 9 || d == 10 || d == 17 || d == 18 || d == 19 || d == 20
                        || d == 29 || d == 30 || d == 54) {
                    rslt = "EN " + rslt;
                } else {
                    rslt = "DA " + rslt;
                }
            }
            if (isCPart1()) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt + getRate();
                if (c >= 32 && c != 31 && c != 39) {
                    if (f == 255) {
                        rslt += " periode BILL berjalan";
                    } else {
                        rslt += " periode BILL ke-" + f;
                    }
                }
            } else if (isCPart2()) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt + getRate();
            } else if (isCPart3()) {
                if (f == 255) {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt + getRate();
                } else if (f >= 0 && f <= 99) {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt + getRate()
                            + " periode BILL ke-" + f;
                } else if (f >= 101 && f <= 125) {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt + getRate()
                            + " " + (f - 100) + " periode BILL sebelumnya";
                } else if (f == 126) {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt + getRate()
                            + " periode BILL yang tidak ditentukan";
                }
            } else if (isCPart4() && (d == 17 || d == 18 || d == 19 || d == 20) && e == 0 && f == 255) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt;
            } else if (isCPart4() && (d == 55 || d == 58) && (e == 0 || e == 255) && f == 255) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt;
            } else if (isCPart4() && (d == 4 || d == 5 || d == 14 || d == 15 || d == 25 || d == 46)
                    && (e >= 0 || e <= 63) && f == 255) {
                if (d == 25) {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt;
                } else {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt + getRate();
                }
            } else if (isCPart4() && (d == 27 || d == 28 || d == 29 || d == 30 || d == 51 || d == 52 || d == 53 || d == 54)
                    && ((e >= 0 || e <= 63) || e == 255) && f == 255) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt;
            } else if (isCDHarmonics1()) {
                if (d <= 25) {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                            + " harmonic/distortion factor " + e;
                } else if (d == 31 || d == 35 || d == 39) {
                    rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                            + " harmonic/distortion factor " + e
                            + " threshold " + f;
                } else {
                    if (f == 255) {
                        rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                                + " harmonic/distortion factor " + e + " periode BILL berjalan";
                    } else {
                        rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                                + " harmonic/distortion factor " + e + " periode BILL ke-" + f;
                    }
                }
            } else if (isCDHarmonics2()) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                        + " harmonic " + e;
            } else if (isCDHarmonics3()) {
                switch (e) {
                    case 124:
                        rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                                + " TOT harmonic distortion";
                        break;
                    case 125:
                        rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                                + " TOT DE distortion";
                        break;
                    case 126:
                        rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                                + " all harmonics";
                        break;
                    case 127:
                        rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                                + " all harmonics to nominal value ratio";
                        break;
                }
            } else if ((c == 11 || c == 12 || c == 15 || c == 31 || c == 32 || c == 35 || c == 51 || c == 52 || c == 55
                    || c == 71 || c == 72 || c == 75 || (c >= 90 && c <= 92)) && d == 56 && e == 0 && f == 255) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt
                        + " fundamental + all harmonics";
            } else if (isUnipede()) {
                switch (e) {
                    case 0:
                        rslt += "Unipede VOLT dip Class 10...<= 15%, 10ms...<= 100ms";
                        break;
                    case 1:
                        rslt += "Unipede VOLT dip Class 10...<=  15%, 100ms...<=  500ms";
                        break;
                    case 2:
                        rslt += "Unipede VOLT dip Class 10...<=  15%, 500ms...<=  1000ms";
                        break;
                    case 3:
                        rslt += "Unipede VOLT dip Class 10...<=  15%, 1s...<=  3s";
                        break;
                    case 4:
                        rslt += "Unipede VOLT dip Class 10...<=  15%, 3s...<= 20s";
                        break;
                    case 5:
                        rslt += "Unipede VOLT dip Class 10...<= 15%, 20s...<= 60s";
                        break;
                    case 10:
                        rslt += "Unipede VOLT dip Class 15...<= 30%, 10...<= 100ms";
                        break;
                    case 11:
                        rslt += "Unipede VOLT dip Class 15...<= 30%, 100...<= 500ms";
                        break;
                    case 12:
                        rslt += "Unipede VOLT dip Class 15...<= 30%, 500...<= 1000ms";
                        break;
                    case 13:
                        rslt += "Unipede VOLT dip Class 15...<= 30%, 1s...<= 3s";
                        break;
                    case 14:
                        rslt += "Unipede VOLT dip Class 15...<= 30%, 3s...<= 20s";
                        break;
                    case 15:
                        rslt += "Unipede VOLT dip Class 15...<= 30%, 20s...<= 60s";
                        break;
                    case 20:
                        rslt += "Unipede VOLT dip Class 30...<= 60%, 10...<= 100ms";
                        break;
                    case 21:
                        rslt += "Unipede VOLT dip Class 30...<= 60%, 100...<= 500ms";
                        break;
                    case 22:
                        rslt += "Unipede VOLT dip Class 30...<= 60%, 500...<= 1000ms";
                        break;
                    case 23:
                        rslt += "Unipede VOLT dip Class 30...<= 60%, 1s...<= 3s";
                        break;
                    case 24:
                        rslt += "Unipede VOLT dip Class 30...<= 60%, 3s...<= 20s";
                        break;
                    case 25:
                        rslt += "Unipede VOLT dip Class 30...<= 60%, 20s...<= 60s";
                        break;
                    case 30:
                        rslt += "Unipede VOLT dip Class 60...<= 90%, 10...<= 100ms";
                        break;
                    case 31:
                        rslt += "Unipede VOLT dip Class 60...<= 90%, 100...<= 500ms";
                        break;
                    case 32:
                        rslt += "Unipede VOLT dip Class 60...<= 90%, 500...<= 1000ms";
                        break;
                    case 33:
                        rslt += "Unipede VOLT dip Class 60...<= 90%, 1s...<= 3s";
                        break;
                    case 34:
                        rslt += "Unipede VOLT dip Class 60...<= 90%, 3s...<= 20s";
                        break;
                    case 35:
                        rslt += "Unipede VOLT dip Class 60...<= 90%, 20s...<= 60s";
                        break;
                    case 40:
                        rslt += "Unipede VOLT dip Class 90...<= 99%, 10...<= 100ms";
                        break;
                    case 41:
                        rslt += "Unipede VOLT dip Class 90...<= 99%, 100...<= 500ms";
                        break;
                    case 42:
                        rslt += "Unipede VOLT dip Class 90...<= 99%, 500...<= 1000ms";
                        break;
                    case 43:
                        rslt += "Unipede VOLT dip Class 90...<= 99%, 1s...<= 3s";
                        break;
                    case 44:
                        rslt += "Unipede VOLT dip Class 90...<= 99%, 3s...<= 20s";
                        break;
                    case 45:
                        rslt += "Unipede VOLT dip Class 90...<= 99%, 20s...<= 90s";
                        break;
                    case 255:
                        rslt += "Unipede VOLT dip Summary";
                        break;

                }
            } else if ((c == 14 || c == 34 || c == 54 || c == 74) && (d == 7 || d == 24)
                    && (e == 0 || e == 255) && f == 255) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt;
            } else if (d != 9 && (c % 20 != 11) && (c % 20 != 12) && (c % 20 != 13) && (c % 20 != 14)) {
                rslt = ElectricityDFieldName.forValue(d).getString() + " " + rslt;
            } else {
                return "Electricity OBJ RSV (Ref: 7.5)";
            }
        } else {
            return "RSV (Ref: 7.5.2.1)";
        }
        if (b >= 1 && b <= 64) {
            rslt += " CH" + b;
        }
        if (rslt.equals("")) {
            return "Undefined OBJ";
        } else {
            return rslt;
        }
    }

    static String getPhaseAngleMEAS() {
        String from;
        String to;
        switch (e % 10) {
            case 0:
                from = "Tegangan R";
                break;
            case 1:
                from = "Tegangan S";
                break;
            case 2:
                from = "Tegangan T";
                break;
            case 4:
                from = "AR R";
                break;
            case 5:
                from = "AR S";
                break;
            case 6:
                from = "AR T";
                break;
            case 7:
                from = "AR N";
                break;
            case 255:
                from = "";
                break;
            default:
                return "RSV (Ref: 7.5.3.4)";
        }
        switch (e) {
            case 0:
            case 1:
            case 2:
            case 4:
            case 5:
            case 6:
            case 7:
                to = "Tegangan R";
                break;
            case 10:
            case 11:
            case 12:
            case 14:
            case 15:
            case 16:
            case 17:
                to = "Tegangan S";
                break;
            case 20:
            case 21:
            case 22:
            case 24:
            case 25:
            case 26:
            case 27:
                to = "Tegangan T";
                break;
            case 40:
            case 41:
            case 42:
            case 44:
            case 45:
            case 46:
            case 47:
                to = "AR R";
                break;
            case 50:
            case 51:
            case 52:
            case 54:
            case 55:
            case 56:
            case 57:
                to = "AR S";
                break;
            case 60:
            case 61:
            case 62:
            case 64:
            case 65:
            case 66:
            case 67:
                to = "AR T";
                break;
            case 70:
            case 71:
            case 72:
            case 74:
            case 75:
            case 76:
            case 77:
                to = "AR N";
                break;
            case 255:
                to = "";
                break;
            default:
                return "RSV (Ref: 7.5.3.4)";
        }
        if (e == 255) {
            return "Summary of phase angles";
        } else {
            return "Sudut " + from + " - " + to;
        }
    }

    static boolean isCPart1() {
        return ((c >= 1 && c <= 10) || (c == 13) || (c == 14) || (c >= 16 && c <= 30) || (c == 33) || (c == 34)
                || (c >= 36 && c <= 50) || (c == 53) || (c == 54) || (c >= 56 && c <= 70)
                || (c == 73) || (c == 74) || (c >= 76 && c <= 80) || (c == 82) || (c >= 84 && c <= 89))
                && ((d == 4) || (d == 5) || (d == 14) || (d == 15) || (d == 24) || (d == 25)
                || (d == 31) || (d == 32) || (d == 33) || (d == 34) || (d == 35) || (d == 35)
                || (d == 36) || (d == 37) || (d == 38) || (d == 39)
                || (d == 40) || (d == 41) || (d == 42) || (d == 43) || (d == 44) || (d == 45))
                && (e >= 0 && e <= 63) && ((f >= 0 && f <= 99) || f == 255);
    }

    static boolean isCPart2() {
        return ((c >= 1 && c <= 13) || (c >= 15 && c <= 33) || (c >= 35 && c <= 53) || (c >= 55 && c <= 73)
                || (c >= 75 && c <= 80) || (c >= 84 && c <= 92))
                && (d == 7 || d == 24) && e == 0 & f == 255;
    }

    static boolean isCPart3() {
        return ((c >= 1 && c <= 80) || (c == 82) || (c >= 84 && c <= 92))
                && (d == 0 || d == 1 || d == 2 || d == 3 || d == 6 || d == 8 || d == 9 || d == 10
                || d == 11 || d == 12 || d == 13 || d == 16 || d == 21 || d == 22 || d == 23 || d == 26)
                && (e >= 0 && e <= 63) && ((f >= 0 && f <= 99) || (f >= 101 && f <= 125) || f == 126 || f == 255);
    }

    static boolean isCPart4() {
        return (c >= 1 && c <= 80) || (c == 82) || (c >= 84 && c <= 92);
    }

    static String getRate() {
        if (e >= 0 && e <= 63) {
            switch (e) {
                case 0:
                    return " TOT";
                case 1:
                    return " WBP";
                default:
                    return " LWBP" + (e - 1);
            }
        } else {
            return "";
        }
    }

    static boolean isCDHarmonics1() {
        return (c == 11 || c == 12 || c == 15 || c == 31 || c == 32 || c == 35 || c == 51 || c == 52 || c == 55
                || c == 71 || c == 72 || c == 75 || (c >= 90 && c <= 92))
                && (d == 4 || d == 5 || d == 14 || d == 15 || d == 24 || d == 25 || d == 31 || d == 33 || d == 34
                || d == 35 || d == 36 || d == 37 || d == 38 || d == 39 || d == 40 || d == 41 || d == 42 || d == 43
                || d == 44 || d == 45) && ((e >= 0 && e <= 120) || (e >= 124 && e <= 127))
                && ((f >= 0 && f <= 99) || f == 255);
    }

    static boolean isCDHarmonics2() {
        return (c == 11 || c == 12 || c == 15 || c == 31 || c == 32 || c == 35 || c == 51 || c == 52 || c == 55
                || c == 71 || c == 72 || c == 75 || (c >= 90 && c <= 92))
                && (d == 7 || d == 24 || d == 56) && (e >= 1 && e <= 120) && f == 255;
    }

    static boolean isCDHarmonics3() {
        return (c == 11 || c == 12 || c == 15 || c == 31 || c == 32 || c == 35 || c == 51 || c == 52 || c == 55
                || c == 71 || c == 72 || c == 75 || (c >= 90 && c <= 92))
                && (d == 7 || d == 24 || d == 56) && (e >= 124 && e <= 127) && f == 255;
    }

    static boolean isUnipede() {
        return (c == 12 || c == 32 || c == 52 || c == 72) && d == 32 && f == 255
                && (e == 0 || e == 1 || e == 2 || e == 3 || e == 4 || e == 5 || e == 10 || e == 11 || e == 12
                || e == 13 || e == 14 || e == 15 || e == 20 || e == 21 || e == 22 || e == 23 || e == 24
                || e == 25 || e == 30 || e == 31 || e == 32 || e == 33 || e == 34 || e == 35 || e == 40
                || e == 41 || e == 42 || e == 43 || e == 44 || e == 45 || e == 255);
    }
}
