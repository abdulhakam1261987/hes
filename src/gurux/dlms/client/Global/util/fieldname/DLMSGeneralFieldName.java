/*
 * 
 */
package gurux.dlms.client.Global.util.fieldname;

import gurux.dlms.client.Global.util.*;

/**
 * Define general object (A is 0) field name on DLMS
 *
 * @author Tab Solutions
 */
class DLMSGeneralFieldName {

    private static int a;
    private static int b;
    private static int c;
    private static int d;
    private static int e;
    private static int f;

    public static String get(int aValue, int bValue, int cValue, int dValue, int eValue, int fValue) {
        a = aValue;
        b = bValue;
        c = cValue;
        d = dValue;
        e = eValue;
        f = fValue;
        String rslt = "";
        if ((c >= 3 && c <= 9) || (c == 32) || (c >= 34 && c <= 39) || (c >= 45 && c <= 64)
                || (c >= 67 && c <= 92) || (c == 95) || (c >= 100 && c <= 127)) {
            return "C-RSV (Ref: 6.2.1)";
        }
        switch (c) {
            case 0:
                if (d == 1 && e == 0 && ((f >= 0 && f <= 99) || f == 255)) {
                    if (f == 255) {
                        rslt = "BILL_PERIOD_COUNT(1)";
                    } else {
                        rslt = "BILL_PERIOD_COUNT(1) " + f;
                    }
                } else if (d == 1 && e == 1 && f == 255) {
                    rslt = "No_of_AVAIL_BILL_periods(1)";
                } else if (d == 1 && e == 2 && f >= 0 && f <= 99) {
                    rslt = "Time stamp of the BILL period(1) " + f;
                } else if (d == 1 && e == 2 && f == 255) {
                    rslt = "Time stamp of the most recent BILL period(1) closed";
                } else if (d == 1 && e == 3 && ((f >= 0 && f <= 99) || f == 255)) {
                    rslt = "BILL period CTR(2), VZ " + f;
                } else if (d == 1 && e == 4 && f == 255) {
                    rslt = "No of AVAIL BILL periods(2)";
                } else if (d == 1 && e == 5 && f >= 0 && f <= 99) {
                    rslt = "Time stamp of the BILL period(2) " + f;
                } else if (d == 1 && e == 5 && f == 255) {
                    rslt = "Time stamp of the most recent BILL period(2) closed";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "FIRMWARE_ID";
                } else if (d == 2 && e == 1 && f == 255) {
                    rslt = "V_FIRMWARE";
                } else if (d == 2 && e == 8 && f == 255) {
                    rslt = "FIRMWARE_SIGN";
                } else if (d == 9 && e == 1 && f == 255) {
                    rslt = "LOCAL_TIME";
                } else if (d == 9 && e == 2 && f == 255) {
                    rslt = "LOCAL_DATE";
                } else {
                    return "RSV (Ref: 7.4.1)";
                }
                break;
            case 1:
                switch (d) {
                    case 0:
                        rslt += "CLOCK";
                        break;
                    case 1:
                        rslt += "UNIX_CLOCK";
                        break;
                    case 2:
                        rslt += "HR_CLOCK";
                        break;
                }
                if (d == 0 || d == 1 || d == 2) {
                    if (e != 0) {
                        rslt += "_" + (e + 1);
                    }
                } else {
                    return "RSV (Ref: 6.2.5)";
                }
                return rslt;
            case 2:
                if (e == 0 && f == 255) {
                    switch (d) {
                        case 0:
                            rslt = "Modem CFG";
                            break;
                        case 1:
                            rslt = "Auto connect";
                            break;
                        case 2:
                            rslt = "Auto answer";
                            break;
                        default:
                            return "RSV (Ref: 6.2.6)";
                    }
                } else {
                    return "RSV (Ref: 6.2.6)";
                }
                break;
            case 10:
                if (d >= 0 && d <= 127 && f == 255) {
                    switch (e) {
                        case 0:
                            if (d == 0) {
                                rslt = "meter reset";
                            } else {
                                rslt = "meter reset IMP SPEC " + (d + 1);
                            }
                            break;
                        case 1:
                            if (d == 0) {
                                rslt = "MDI reset/End of BILL period";
                            } else {
                                rslt = "MDI reset/End of BILL period IMP SPEC " + (d + 1);
                            }
                            break;
                        case 100:
                            if (d == 0) {
                                rslt = "TARIF script";
                            } else {
                                rslt = "TARIF script IMP SPEC " + (d + 1);
                            }
                            break;
                        case 101:
                            if (d == 0) {
                                rslt = "Activate TMODE";
                            } else {
                                rslt = "Activate TMODE IMP SPEC " + (d + 1);
                            }
                            break;
                        case 102:
                            if (d == 0) {
                                rslt = "Activate normal mode";
                            } else {
                                rslt = "Activate normal mode IMP SPEC " + (d + 1);
                            }
                            break;
                        case 103:
                            if (d == 0) {
                                rslt = "Set OUTSIGN";
                            } else {
                                rslt = "Set OUTSIGN IMP SPEC " + (d + 1);
                            }
                            break;
                        case 104:
                            if (d == 0) {
                                rslt = "Switch optical test output";
                            } else {
                                rslt = "Switch optical test output IMP SPEC " + (d + 1);
                            }
                            break;
                        case 105:
                            if (d == 0) {
                                rslt = "PWR QW MEAS MGT";
                            } else {
                                rslt = "PWR QW MEAS MGT IMP SPEC " + (d + 1);
                            }
                            break;
                        case 106:
                            if (d == 0) {
                                rslt = "DISC CTRL SCRIPT";
                            } else {
                                rslt = "DISC CTRL SCRIPT " + (d + 1);
                            }
                            break;
                        case 107:
                            if (d == 0) {
                                rslt = "IMG activation SCRIPT";
                            } else {
                                rslt = "IMG activation SCRIPT IMP SPEC " + (d + 1);
                            }
                            break;
                        case 108:
                            if (d == 0) {
                                rslt = "Push SCRIPT";
                            } else {
                                rslt = "Push SCRIPT IMP SPEC " + (d + 1);
                            }
                            break;
                        case 125:
                            if (d == 0) {
                                rslt = "Broadcast SCRIPT";
                            } else {
                                rslt = "Broadcast SCRIPT IMP SPEC " + (d + 1);
                            }
                            break;
                        default:
                            return "RSV (Ref: 6.2.7)";

                    }
                } else {
                    return "RSV (Ref: 6.2.7)";
                }
                break;
            case 11:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Special days table";
                    } else {
                        rslt = "Special days table " + (e + 1);
                    }
                } else {
                    return "RSV (Ref: 6.2.8)";
                }
                break;
            case 12:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Schedule";
                    } else {
                        rslt = "Schedule " + (e + 1);
                    }
                } else {
                    return "RSV (Ref: 6.2.9)";
                }
                break;
            case 13:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Activity calendar";
                    } else {
                        rslt = "Activity calendar " + (e + 1);
                    }
                } else {
                    return "RSV (Ref: 6.2.10)";
                }
                break;
            case 14:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "REG activation";
                    } else {
                        rslt = "REG activation " + (e + 1);
                    }
                } else {
                    return "RSV (Ref: 6.2.11)";
                }
                break;
            case 15:
                if (d == 0 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "End of BILL period";
                            break;
                        case 1:
                            rslt = "DISC CTRL ACT schedule";
                            break;
                        case 2:
                            rslt = "IMG activation";
                            break;
                        case 3:
                            rslt = "Output CTRL";
                            break;
                        case 4:
                            rslt = "Push";
                            break;
                        case 5:
                            rslt = "LP CTRL";
                            break;
                        case 6:
                            rslt = "M-Bus CTRL";
                            break;
                        case 7:
                            rslt = "FUNC CTRL";
                            break;
                        default:
                            return "RSV (Ref: 6.2.12)";
                    }
                } else if (d >= 1 && d <= 127 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "End of BILL period, IMP SPEC " + (d + 1) + " " + (e + 1);
                } else {
                    return "RSV (Ref: 6.2.12)";
                }
                break;
            case 16:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "REG monitor";
                    } else {
                        rslt = "REG monitor " + (e + 1);
                    }
                } else if (d == 1 && f == 255) {
                    if (e >= 0 && e <= 9) {
                        rslt = "Alarm monitor " + (e + 1);
                    } else {
                        return "RSV (Ref: 6.2.13)";
                    }
                } else if (d == 2 && f == 255) {
                    if (e == 0) {
                        rslt = "PARAM monitor";
                    } else {
                        rslt = "PARAM monitor " + (e + 1);
                    }
                } else {
                    if (d == 2) {
                        return "RSV (Ref: 6.2.14)";
                    } else {
                        return "RSV (Ref: 6.2.13)";
                    }
                }
                break;
            case 17:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Limiter";
                    } else {
                        rslt = "Limiter " + (e + 1);
                    }
                } else {
                    return "RSV (Ref: 6.2.15)";
                }
                break;
            case 18:
                if (d == 0 && f == 255) {
                    if (e == 0) {
                        rslt = "Array manager";
                    } else {
                        rslt = "Array manager " + (e);
                    }
                } else {
                    return "RSV (Ref: 6.2.16)";
                }
                break;
            case 19:
                if (d >= 0 && d <= 9 && e == 0 && f == 255) {
                    rslt = "Account";
                } else if (d >= 10 && d <= 19 && f == 255) {
                    if (e == 0) {
                        rslt = "Credit " + (d - 10);
                    } else {
                        rslt = "Credit " + (d - 10) + " " + (e);
                    }
                } else if (d >= 20 && d <= 29 && f == 255) {
                    if (e == 0) {
                        rslt = "Charge " + (d - 20);
                    } else {
                        rslt = "Charge " + (d - 20) + " " + (e);
                    }
                } else if (d >= 40 && d <= 49 && f == 255) {
                    if (e == 0) {
                        rslt = "Token gateway " + (d - 40);
                    } else {
                        rslt = "Token gateway " + (d - 40) + " " + (e);
                    }
                } else if (d >= 50 && d <= 59 && f == 255 && (e == 1 || e == 2)) {
                    if (e == 1) {
                        rslt = "Max credit limit " + (d - 50);
                    } else {
                        rslt = "Max vend limit " + (d - 50);
                    }
                } else {
                    return "RSV (Ref: 6.2.17)";
                }
                break;
            case 20:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "IEC optical port setup";
                } else if (d == 0 && e == 1 && f == 255) {
                    rslt = "electrical port setup";
                } else {
                    return "RSV (Ref: 6.2.18)";
                }
                break;
            case 21:
                if (d == 0 && d == 255) {
                    switch (e) {
                        case 0:
                            rslt = "READOUT " + (e + 1) + " ";
                            break;
                        case 1:
                            rslt = "READOUT " + (e + 1) + " ";
                            break;
                        case 2:
                            rslt = "READOUT " + (e + 1) + " ";
                            break;
                        case 3:
                            rslt = "READOUT " + (e + 1) + " ";
                            break;
                        case 4:
                            rslt = "READOUT " + (e + 1) + " ";
                            break;
                        default:
                            rslt = "READOUT " + (e + 1) + " ";
                            break;
                    }
                } else {
                    return "RSV (Ref: 6.2.19)";
                }
                break;
            case 22:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "IEC HDLC setup";
                } else {
                    return "RSV (Ref: 6.2.20)";
                }
                break;
            case 23:
                if (f == 255) {
                    switch (d) {
                        case 0:
                            switch (e) {
                                case 0:
                                    rslt = "IEC twisted pair (1) setup";
                                    break;
                                case 1:
                                    rslt = "IEC twisted pair (1) MAC ADDR setup";
                                    break;
                                case 2:
                                    rslt = "IEC twisted pair (1) fatal error REG";
                                    break;
                                default:
                                    return "RSV (Ref: 6.2.21)";
                            }
                            break;
                        case 3:
                            if (e == 0) {
                                rslt = "IEC 62056-3-1_READOUT";
                            } else if (e == 1) {
                                rslt = "IEC 62056-3-1_READOUT_1";
                            } else if (e >= 2 && e <= 9) {
                                rslt = "IEC 62056-3-1_ALT READOUT";
                            } else {
                                rslt = "IEC 62056-3-1_READOUT_PARAM_" + e;
                            }
                            break;
                        default:
                            return "RSV (Ref: 6.2.21)";
                    }
                } else {
                    return "RSV (Ref: 6.2.21)";
                }
                break;
            case 24:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "M-Bus slave port setup";
                } else if (d == 1 && f == 255) {
                    rslt = "M-Bus client " + (e + 1);
                } else if (d == 2 && f == 255) {
                    rslt = "M-Bus value " + (e + 1);
                } else if (d == 3 && f == 255) {
                    rslt = "M-Bus PG " + (e + 1);
                } else if (d == 4 && e == 0 && f == 255) {
                    rslt = "M-Bus DISC CTRL";
                } else if (d == 5 && e == 0 && f == 255) {
                    rslt = "M-Bus CTRL log";
                } else if (d == 6 && e == 0 && f == 255) {
                    rslt = "M-Bus master port setup";
                } else if (d == 8 && f == 255) {
                    rslt = "DLMS/COSEM server M-Bus port setup " + (e + 1);
                } else if (d == 9 && f == 255) {
                    rslt = "M-Bus DIAG " + (e + 1);
                } else {
                    return "RSV (Ref: 6.2.22)";
                }
                break;
            case 25:
                if (e == 0 && f == 255) {
                    switch (d) {
                        case 0:
                            rslt = "TCP-UDP setup";
                            break;
                        case 1:
                            rslt = "IPv4 setup";
                            break;
                        case 2:
                            rslt = "MAC ADDR setup";
                            break;
                        case 3:
                            rslt = "PPP setup";
                            break;
                        case 4:
                            rslt = "GPRS modem setup";
                            break;
                        case 5:
                            rslt = "SMTP setup";
                            break;
                        case 6:
                            rslt = "GSM DIAG";
                            break;
                        case 7:
                            rslt = "IPv6 setup";
                            break;
                        case 9:
                            rslt = "Push setup";
                            break;
                        case 10:
                            rslt = "NTP setup";
                            break;
                        case 11:
                            rslt = "LTE monitoring";
                            break;
                        default:
                            return "RSV (Ref: 6.2.23)";
                    }
                } else {
                    return "RSV (Ref: 6.2.23)";
                }
                break;
            case 26:
                if (e == 0 && f == 255) {
                    switch (d) {
                        case 0:
                            rslt = "S-FSK Phy&MAC setup";
                            break;
                        case 1:
                            rslt = "S-FSK Active initiator";
                            break;
                        case 2:
                            rslt = "S-FSK MAC SYNC timeouts";
                            break;
                        case 3:
                            rslt = "S-FSK MAC CTRs";
                            break;
                        case 5:
                            rslt = "IEC 61334-4-32 LLC setup";
                            break;
                        case 6:
                            rslt = "S-FSK Reporting system list";
                            break;
                        default:
                            return "RSV (Ref: 6.2.24)";
                    }
                } else {
                    return "RSV (Ref: 6.2.24)";
                }
                break;
            case 27:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "ISO/IEC 8802-2 LLC Type 1 setup";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "ISO/IEC 8802-2 LLC Type 2 setup";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "ISO/IEC 8802-2 LLC Type 3 setup";
                } else {
                    return "RSV (Ref: 6.2.25)";
                }
                break;
            case 28:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "61334-4-32 LLC SSCS setup";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC Physical layer CTRs";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC setup";
                } else if (d == 3 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC functional PARAM";
                } else if (d == 4 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC CTRs";
                } else if (d == 5 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC network administration data";
                } else if (d == 6 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC MAC ADDR setup";
                } else if (d == 7 && e == 0 && f == 255) {
                    rslt = "PRIME NB OFDM PLC Application ID";
                } else {
                    return "RSV (Ref: 6.2.26)";
                }
                break;
            case 29:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "G3-PLC MAC layer CTRs";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "G3-PLC MAC setup";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "G3-PLC 6LoWPAN adaptation layer setup";
                } else {
                    return "RSV (Ref: 6.2.27)";
                }
                break;
            case 30:
                if (d == 0 && f == 255) {
                    rslt = "ZigBee� SAS startup " + (e + 1);
                } else if (d == 1 && f == 255) {
                    rslt = "ZigBee� SAS join " + (e + 1);
                } else if (d == 2 && f == 255) {
                    rslt = "ZigBee� SAS APS fragmentation " + (e + 1);
                } else if (d == 3 && f == 255) {
                    rslt = "ZigBee� SAS network CTRL " + (e + 1);
                } else if (d == 4 && f == 255) {
                    rslt = "ZigBee� SAS tunnel setup " + (e + 1);
                } else {
                    return "RSV (Ref: 6.2.28)";
                }
                break;
            case 31:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "Wireless Mode Q channel";
                } else {
                    return "RSV (Ref: 6.2.22)";
                }
                break;
            case 33:
                if (d == 0 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 MAC setup";
                } else if (d == 1 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 CPAS setup";
                } else if (d == 2 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 IP SSAS setup";
                } else if (d == 3 && e == 0 && f == 255) {
                    rslt = "HS-PLC ISO/IEC 12139-1 HDLC SSAS setup";
                } else {
                    return "RSV (Ref: 6.2.29)";
                }
                break;
            case 40:
                if (b == 0 && d == 0 && f == 255) {
                    if (e == 0) {
                        return "Current ASSOC_VIEW";
                    } else {
                        return "ASSOC_VIEW " + e;
                    }
                } else {
                    return "RSV (Ref: 6.2.30)";
                }
            case 41:
                if (b == 0 && d == 0 && e == 0 && f == 255) {
                    return "SAP ASSIGN";
                } else {
                    return "RSV (Ref: 6.2.31)";
                }
            case 42:
                if (b == 0 && d == 0 && e == 0 && f == 255) {
                    return "COSEM logical device name";
                } else {
                    return "RSV (Ref: 6.2.32)";
                }
            case 43:
                if (b == 0 && d == 0 && f == 255) {
                    if (e == 0) {
                        return "Security setup";
                    } else {
                        return "Security setup " + e;
                    }
                } else if (b == 0 && d == 2 && f == 255) {
                    if (e == 0) {
                        return "Data protection";
                    } else {
                        return "Data protection " + e;
                    }
                } else {
                    if (d == 1 && f == 255) {
                        if (e == 0) {
                            rslt = "Invocation CTR";
                        } else {
                            rslt = "Invocation CTR " + e;
                        }
                    } else {
                        return "RSV (Ref: 6.2.33)";
                    }
                }
                break;
            case 44:
                if (b == 0 && d == 0 && f == 255) {
                    if (e == 0) {
                        return "IMG transfer";
                    } else {
                        return "IMG transfer " + e;
                    }
                } else if (b == 0 && d == 1 && f == 255) {
                    if (e == 0) {
                        return "FUNC CTRL";
                    } else {
                        return "FUNC CTRL " + e;
                    }
                } else {
                    return "RSV (Ref: 6.2.35/6.2.34)";
                }
            case 65:
                if (d >= 0 && d <= 15 && f == 255) {
                    rslt = "Standard tables " + ((d * 128) + e);
                } else if (d >= 16 && d <= 31 && f == 255) {
                    rslt = "MANU tables " + ((d * 128) + e);
                } else if (d >= 32 && d <= 47 && f == 255) {
                    rslt = "Std pending tables " + ((d * 128) + e);
                } else if (d >= 48 && d <= 63 && f == 255) {
                    rslt = "Mfg pending tables " + ((d * 128) + e);
                } else {
                    return "RSV (Ref: 6.2.36)";
                }
                break;
            case 66:
                if (d == 0 && f == 255) {
                    rslt = "Compact data " + e;
                } else {
                    return "RSV (Ref: 6.2.37)";
                }
                break;
            case 93:
                return "RSV (Ref: 7.3.4.2)";
            case 94:
                if (CountrySpesificIdentifier.getMappings().containsKey(d)) {
                    rslt = "ID for " + CountrySpesificIdentifier.forValue(d).toString() + " " + e
                            + " " + f;
                } else {
                    return "RSV (Ref: 7.3.4.3)";
                }
                break;
            case 96:
                if (d == 1 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "NOMOR_METER";
                            break;
                        case 1:
                            rslt = "Tipe meter";
                            break;
                        case 2:
                            rslt = "Id Pelanggan";
                            break;
                        case 3:
                            rslt = "ID PLN";
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            rslt = "Device ID_" + (e + 1);
                            break;
                        case 10:
                            rslt = "Metering point ID (abstract)";
                            break;
                        case 255:
                            rslt = "Complete device ID";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 2 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "No of CONFIG program changes";
                            break;
                        case 1:
                            rslt = "Date of last CONFIG program change";
                            break;
                        case 2:
                            rslt = "Date of last time switch program change";
                            break;
                        case 3:
                            rslt = "Date of last ripple CTRL program change";
                            break;
                        case 4:
                            rslt = "Status of security switches";
                            break;
                        case 5:
                            rslt = "Date of last calibration";
                            break;
                        case 6:
                            rslt = "Date of next CONFIG program change";
                            break;
                        case 7:
                            rslt = "Date of activation of the passive calendar";
                            break;
                        case 10:
                            rslt = "No of protected CONFIG program changes";
                            break;
                        case 11:
                            rslt = "Date of last protected CONFIG program change";
                            break;
                        case 12:
                            rslt = "Date (corrected) of last clock synchronisation / setting";
                            break;
                        case 13:
                            rslt = "Date of last FIRMWARE activation";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 3 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "State of input/output CTRL signals, global";
                            break;
                        case 1:
                            rslt = "State of the input CTRL signals (status word " + e + ")";
                            break;
                        case 2:
                            rslt = "State of the output CTRL signals (status word " + e + ")";
                            break;
                        case 3:
                            rslt = "State of input/output CTRL signals (status word " + e + ")";
                            break;
                        case 4:
                            rslt = "State of input/output CTRL signals (status word " + e + ")";
                            break;
                        case 10:
                            rslt = "DISC CTRL";
                            break;
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                        case 28:
                        case 29:
                            rslt = "Arbitrator " + (e - 20);
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 4 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "State of the internal CTRL signals, global";
                            break;
                        case 1:
                            rslt = "State of the internal CTRL signals (status word 1)";
                            break;
                        case 2:
                            rslt = "State of the internal CTRL signals (status word 2)";
                            break;
                        case 3:
                            rslt = "State of the internal CTRL signals (status word 3)";
                            break;
                        case 4:
                            rslt = "State of the internal CTRL signals (status word 4)";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 5 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "General Internal operating status, global";
                            break;
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            rslt = "General Internal operating status (status word " + e + ")";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 6 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Battery use time CTR";
                            break;
                        case 1:
                            rslt = "Battery charge display";
                            break;
                        case 2:
                            rslt = "Date of next battery change";
                            break;
                        case 3:
                            rslt = "Battery voltage";
                            break;
                        case 4:
                            rslt = "Battery initial capacity";
                            break;
                        case 5:
                            rslt = "Battery installation date and time";
                            break;
                        case 6:
                            rslt = "Battery estimated remaining use time";
                            break;
                        case 10:
                            rslt = "Aux supply use time CTR";
                            break;
                        case 11:
                            rslt = "Aux voltage (measured)";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 7 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Jumlah padam bersamaan di semua phase";
                            break;
                        case 1:
                            rslt = "Jumlah padam R";
                            break;
                        case 2:
                            rslt = "Jumlah padam S";
                            break;
                        case 3:
                            rslt = "Jumlah padam T";
                            break;
                        case 4:
                            rslt = "No of power failures Auxiliary power supply";
                            break;
                        case 21:
                            rslt = "Jumlah padam";
                            break;
                        case 5:
                            rslt = "Jumlah padam berkepanjangan bersamaan di semua phasa";
                            break;
                        case 6:
                            rslt = "Jumlah padam berkepanjangan R";
                            break;
                        case 7:
                            rslt = "Jumlah padam berkepanjangan S";
                            break;
                        case 8:
                            rslt = "Jumlah padam berkepanjangan T";
                            break;
                        case 9:
                            rslt = "Jumlah padam berkepanjangan";
                            break;
                        case 10:
                            rslt = "Waktu kejadian padam bersamaan di semua phasa";
                            break;
                        case 11:
                            rslt = "Waktu kejadian padam R";
                            break;
                        case 12:
                            rslt = "Waktu kejadian padam S";
                            break;
                        case 13:
                            rslt = "Waktu kejadian padam T";
                            break;
                        case 14:
                            rslt = "Waktu kejadian padam";
                            break;
                        case 15:
                            rslt = "Durasi padam berkepanjangan bersamaan di semua phasa";
                            break;
                        case 16:
                            rslt = "Durasi padam berkepanjangan R";
                            break;
                        case 17:
                            rslt = "Durasi padam berkepanjangan S";
                            break;
                        case 18:
                            rslt = "Durasi padam berkepanjangan T";
                            break;
                        case 19:
                            rslt = "Durasi padam berkepanjangan";
                            break;
                        case 20:
                            rslt = "Batas waktu padam dinyatakan berkepanjangan";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 8 && f == 255) {
                    if (e == 0) {
                        rslt = "Lama meter beroperasi";
                    } else if (e == 1) {
                        rslt = "Lama meter beroperasi di WBP";
                    } else if (e >= 2 && e <= 63) {
                        rslt = "Lama meter beroperasi di LWBP" + (e - 1);
                    } else {
                        return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 9 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Ambient temperature";
                            break;
                        case 1:
                            rslt = "Ambient pressure";
                            break;
                        case 2:
                            rslt = "Relative humidity";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 10 && f == 255) {
                    if (e >= 1 && e <= 10) {
                        rslt = "Status REG " + e;
                    } else {
                        return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 11 && f == 255) {
                    if (e >= 0 && e <= 99) {
                        rslt = "Event code " + (e + 1);
                    } else {
                        return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 12 && f == 255) {
                    switch (e) {
                        case 1:
                            rslt = "Comm port PARAM No of connections";
                            break;
                        case 4:
                            rslt = "Comm port PARAM PARAM 1";
                            break;
                        case 5:
                            rslt = "Comm port PARAM GSM field strength";
                            break;
                        case 6:
                            rslt = "Comm port PARAM Telephone number / COMM ADDR of the physical device";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 13 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Consumer message via local consumer information port";
                            break;
                        case 1:
                            rslt = "Pesan untuk pelanggan melalui LCD meter";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 14 && f == 255) {
                    if (e >= 0 && e <= 15) {
                        rslt = "TARIF  aktif " + (e + 1);
                    } else {
                        return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 15 && f == 255) {
                    if (e >= 0 && e <= 99) {
                        rslt = "Event CTR " + (e + 1);
                    } else {
                        return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 16 && f == 255) {
                    if (e >= 0 && e <= 9) {
                        rslt = "Profile entry digital signature " + (e + 1);
                    } else {
                        return "RSV (Ref: 7.4.1)";
                    }
                } else if (d == 20 && f == 255) {
                    switch (e) {
                        case 0:
                            rslt = "Meter open EVE CTR";
                            break;
                        case 1:
                            rslt = "Meter open EVE, time stamp of current EVE occurrence";
                            break;
                        case 2:
                            rslt = "Meter open EVE, duration of current EVE";
                            break;
                        case 3:
                            rslt = "Meter open EVE, KUMUL duration";
                            break;
                        case 5:
                            rslt = "Terminal cover open EVE CTR";
                            break;
                        case 6:
                            rslt = "Terminal cover open EVE, time stamp of current EVE occurrence";
                            break;
                        case 7:
                            rslt = "Terminal cover open EVE, duration of current EVE";
                            break;
                        case 8:
                            rslt = "Terminal cover open EVE, KUMUL duration";
                            break;
                        case 10:
                            rslt = "Tilt EVE CTR";
                            break;
                        case 11:
                            rslt = "Tilt EVE, time stamp of current EVE occurrence";
                            break;
                        case 12:
                            rslt = "Tilt EVE, duration of current EVE";
                            break;
                        case 13:
                            rslt = "Tilt EVE, KUMUL duration";
                            break;
                        case 15:
                            rslt = "Strong DC magnetic field EVE CTR";
                            break;
                        case 16:
                            rslt = "Strong DC magnetic field EVE, time stamp of current EVE occurrence";
                            break;
                        case 17:
                            rslt = "Strong DC magnetic field EVE, duration of current EVE";
                            break;
                        case 18:
                            rslt = "Strong DC magnetic field EVE, KUMUL duration";
                            break;
                        case 20:
                            rslt = "Supply CTRL switch / valve tamper EVE CTR";
                            break;
                        case 21:
                            rslt = "Supply CTRL switch / valve tamper EVE, time stamp of current EVE occurrence";
                            break;
                        case 22:
                            rslt = "Supply CTRL switch / valve tamper EVE, duration of current EVE";
                            break;
                        case 23:
                            rslt = "Supply CTRL switch / valve tamper EVE, KUMUL duration";
                            break;
                        case 25:
                            rslt = "Metrology tamper EVE CTR";
                            break;
                        case 26:
                            rslt = "Metrology tamper EVE, time stamp of current EVE occurrence";
                            break;
                        case 27:
                            rslt = "Metrology tamper EVE, duration of current EVE";
                            break;
                        case 28:
                            rslt = "Metrology tamper EVE, cumuKUMULlative duration";
                            break;
                        case 29:
                            rslt = "RSV";
                            break;
                        case 30:
                            rslt = "COMM tamper EVE CTR";
                            break;
                        case 31:
                            rslt = "COMM tamper EVE, time stamp of current EVE occurrence";
                            break;
                        case 32:
                            rslt = "COMM tamper EVE, duration of current EVE";
                            break;
                        case 33:
                            rslt = "COMM tamper EVE, KUMUL duration";
                            break;
                        default:
                            return "RSV (Ref: 7.4.1)";
                    }
                } else if (d >= 50 && d <= 99 && f == 255) {
                    return "Man spec abstract object";
                } else {
                    return "RSV (Ref: 7.4.1)";
                }
                break;
            case 97:
                if (d == 97 && e >= 0 && e <= 9 && f == 255) {
                    rslt = "General Error object " + (e + 1);
                } else if (d == 97 && e == 255 && f == 255) {
                    rslt = "General Error profile object";
                } else if (d == 98 && e >= 0 && e <= 9 && f == 255) {
                    rslt = "Alarm REG object " + (e + 1);
                } else if (d == 98 && e >= 10 && e <= 19 && f == 255) {
                    rslt = "Alarm filter object " + (e - 9);
                } else if (d == 98 && e >= 20 && e <= 29 && f == 255) {
                    rslt = "Alarm descriptor object " + (e - 9);
                } else if (d == 98 && e == 255 && f == 255) {
                    rslt = "Alarm REG profile object";
                } else {
                    return "RSV (Ref: 7.4.2)";
                }
                break;
            case 98:
                if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                    rslt = "General BILL ke-" + (e + 1) + " periode " + f;
                } else if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                    rslt = "General BILL ke-" + (e + 1) + " " + (f - 100) + " periode sebelumnya";
                } else if (d == 1 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                    rslt = "General BILL ke " + (e + 1) + " dengan periode yang tidak ditentukan";
                } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                    rslt = "General BILL ke-" + (e + 1) + " scheme 2 periode " + f;
                } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                    rslt = "General BILL ke-" + (e + 1) + " scheme 2(" + (f - 100) + " periode sebelumnya)";
                } else if (d == 2 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                    rslt = "General BILL ke-" + (e + 1) + " scheme 2 dengan periode yang tidak ditentukan";
                } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f >= 0 && f <= 99)) {
                    rslt = "General REG table objects, general use " + (e + 1);
                } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f >= 101 && f <= 125)) {
                    rslt = "General REG table objects, general use " + (e + 1)
                            + "(" + (f - 100) + " periode sebelumnya)";
                } else if (d == 10 && ((e >= 0 && e <= 127) || e == 255) && (f == 126 || f == 255)) {
                    rslt = "General REG table objects, general use dengan periode yang tidak ditentukan" + (e + 1);
                } else {
                    return "RSV (Ref: 7.4.3/7.4.4)";
                }
                break;
            case 99:
                if (d == 1 && e >= 0 && e <= 127 && f == 255) {
                    if (e == 0) {
                        rslt = "General LP";
                    } else {
                        rslt = "General LP ke-" + (e + 1);
                    }
                } else if (d == 2 && e >= 0 && e <= 127 && f == 255) {
                    if (e == 0) {
                        rslt = "General LP periode 2";
                    } else {
                        rslt = "General LP periode 2 ke-" + (e + 1);
                    }
                } else if (d == 3 && e == 0 && f == 255) {
                    rslt = "General LP during test";
                } else if (d == 12 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Connection profile " + (e + 1);
                } else if (d == 13 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "GSM DIAG profile " + (e + 1);
                } else if (d == 14 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Charge collection history (Payment metering) " + (e + 1);
                } else if (d == 15 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Token credit history (Payment metering) " + (e + 1);
                } else if (d == 16 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "PARAM monitor log " + (e + 1);
                } else if (d == 17 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "Token transfer log (Payment metering) " + (e + 1);
                } else if (d == 98 && e >= 0 && e <= 127 && f == 255) {
                    rslt = "General EVE log " + (e + 1);
                } else {
                    return "RSV (Ref: 7.4.3/7.4.4)";
                }
                break;
            case 127:
                return "Inactive object (Ref: 6.2.62)";
            default:
                return "Undefined";
        }
        if (b >= 1 && b <= 64) {
            rslt += " CH " + b;
        }
        if (rslt.equals("")) {
            return "Undefined";
        } else {
            return rslt;
        }
    }
}
