package gurux.dlms.client.Global.util.fieldname;

/**
 * Value group D codes - Electricity as processing of measurement values.
 *
 * Reference : Blue Book Chapter 7.5.2.1
 *
 * @author Tab Solutions Team
 */
public enum ElectricityDFieldName {
    /**
     * Billing period average (since last reset). Averaging scheme 1 Controlled
     * by measurement period 1, a set of registers is calculated by a metering
     * device (codes 1...6). The typical usage is for billing purposes.
     * Averaging scheme 2 Controlled by measurement period 2, a set of registers
     * is calculated by a metering device (codes 11...16). The typical usage is
     * for billing purposes. Averaging scheme 3 Controlled by measurement period
     * 3, a set of registers is calculated by a metering device (codes 21...26).
     * The typical usage is for instantaneous values. Averaging scheme 4
     * Controlled by measurement period 4, a test average value (code 55) is
     * calculated by the metering device.
     *
     * Reference : Blue Book Table 68
     */
    billing_periode_average(0),
    /**
     * Cumulative minimum 1. The cumulative sum of minimum values over all the
     * past billing periods.
     */
    KUM_MIN(1),
    /**
     * Cumulative maximum 1. The cumulative sum of maximum values over all the
     * past billing periods.
     */
    KUM_MAKS(2),
    /**
     * Minimum 1. The smallest of last average values during a billing period.
     *
     * Reference : Blue Book Table 68.
     */
    MIN(3),
    /**
     * Current average 1. The value is calculated using measurement period 1, 2
     * and/or 3 respectively.
     *
     * Reference : The definition of the �Demand register� IC in Blue Book
     * Chapter 4.3.4.
     */
    DE(4),
    /**
     * Last average 1. The value is calculated using measurement period 1, 2
     * and/or 3 respectively.
     *
     * Reference : The definition of the �Demand register� IC in Blue Book
     * Chapter 4.3.4.
     */
    LAST(5),
    /**
     * Maximum 1. The largest of last average values during a billing period.
     */
    MAKS(6),
    /**
     * Instantaneous value
     */
    INS(7),
    /**
     * Time integral 1. For a current billing period (F= 255): Time integral of
     * the quantity calculated from the origin (first start of measurement) to
     * the instantaneous time point. For a historical billing period (F= 0�99):
     * Time integral of the quantity calculated from the origin to the end of
     * the billing period given by the billing period code.
     */
    STAND(8),
    /**
     * Time integral 2. For a current billing period (F = 255): Time integral of
     * the quantity calculated from the beginning of the current billing period
     * to the instantaneous time point. For a historical billing period (F =
     * 0�99): Time integral of the quantity calculated over the billing period
     * given by the billing period code.
     */
    PE(9),
    /**
     * Time integral 3. Time integral of the positive difference between the
     * quantity and a prescribed threshold value.
     */
    TI3(10),
    /**
     * Cumulative minimum 2. The cumulative sum of minimum values over all the
     * past billing periods.
     */
    KUM_MIN_2(11),
    /**
     * Cumulative maximum 2. The cumulative sum of maximum values over all the
     * past billing periods.
     */
    KUM_MAKS_2(12),
    /**
     * Minimum 2. The smallest of last average values during a billing period.
     *
     * Reference : Blue Book Table 68.
     */
    MIN_2(13),
    /**
     * Current average 2. The value is calculated using measurement period 1, 2
     * and/or 3 respectively.
     *
     * Reference : The definition of the �Demand register� IC in Blue Book
     * Chapter 4.3.4.
     */
    DE_2(14),
    /**
     * Last average 2. The value is calculated using measurement period 1, 2
     * and/or 3 respectively.
     *
     * Reference : The definition of the �Demand register� IC in Blue Book
     * Chapter 4.3.4.
     */
    LAST_2(15),
    /**
     * Maximum 2. The largest of last average values during a billing period.
     */
    MAKS_2(16),
    /**
     * Time integral 7. Time integral of the quantity calculated from the origin
     * (first start of measurement) up to the end of the last recording period
     * with recording period 1.
     *
     * Reference : Blue Book Table 68.
     */
    TI7(17),
    /**
     * Time integral 8. Time integral of the quantity calculated from the origin
     * (first start of measurement) up to the end of the last recording period
     * with recording period 2.
     *
     * Reference : Blue Book Table 68.
     */
    TI8(18),
    /**
     * Time integral 9. Time integral of the quantity calculated from the
     * beginning of the current billing period up to the end of the last
     * recording period with recording period 1.
     *
     * Reference : Blue Book Table 68.
     */
    TI9(19),
    /**
     * Time integral 10. Time integral of the quantity calculated from the
     * beginning of the current billing period up to the end of the last
     * recording period with recording period 2.
     *
     * Reference : Blue Book Table 68.
     */
    TI10(20),
    /**
     * Cumulative minimum 3. The cumulative sum of minimum values over all the
     * past billing periods.
     */
    KUM_MIN_3(21),
    /**
     * Minimum 3. The smallest of last average values during a billing period.
     *
     * Reference : Blue Book Table 68.
     */
    KUM_MAKS_3(22),
    /**
     * Minimum 3. The smallest of last average values during a billing period.
     *
     * Reference : Blue Book Table 68.
     */
    MIN_3(23),
    /**
     * Current average 3. The value is calculated using measurement period 1, 2
     * and/or 3 respectively.
     *
     * Reference : The definition of the �Demand register� IC in Blue Book
     * Chapter 4.3.4.
     */
    DE_3(24),
    /**
     * Last average 3. The value is calculated using measurement period 1, 2
     * and/or 3 respectively.
     *
     * Reference : The definition of the �Demand register� IC in Blue Book
     * Chapter 4.3.4.
     */
    LAST_3(25),
    /**
     * Maximum 3. The largest of last average values during a billing period.
     */
    MAKS_3(26),
    /**
     * Current average 5. The value is calculated using recording interval 1.
     *
     * Reference : Blue Book Table 68. Reference : The definition of the �Demand
     * register� IC in Blue Book Chapter 4.3.4.
     */
    DE_5(27),
    /**
     * Current average 6. The value is calculated using recording interval 2.
     *
     * Reference : The definition of the �Demand register� IC in Blue Book
     * Chapter 4.3.4.
     */
    DE_6(28),
    /**
     * Time integral 5. Used as a base for load profile recording: Time integral
     * of the quantity calculated from the beginning of the current recording
     * interval to the instantaneous time point for recording period 1.
     *
     * Reference : Blue Book Table 68.
     */
    TI5(29),
    /**
     * Time integral 6. Used as a base for load profile recording: Time integral
     * of the quantity calculated from the beginning of the current recording
     * interval to the instantaneous time point for recording period 2.
     *
     * Reference : Blue Book Table 68.
     */
    TI6(30),
    /**
     * Under limit threshold. Values under a certain threshold (for example
     * dips).
     */
    batas_minimum(31),
    /**
     * Under limit occurrence counter. Values under a certain threshold (for
     * example dips).
     */
    jumlah_kejadian_di_bawah_batas_minimum(32),
    /**
     * Under limit duration. Values under a certain threshold (for example
     * dips).
     */
    durasi_batas_minimum(33),
    /**
     * Under limit magnitude. Values under a certain threshold (for example
     * dips).
     */
    under_limit_magnitude(34),
    /**
     * Over limit threshold. Values above a certain threshold (for example
     * swells).
     */
    batas_maksimum(35),
    /**
     * Over limit occurrence counter. Values above a certain threshold (for
     * example swells).
     */
    jumlah_kejadian_melebihi_batas_maksimum(36),
    /**
     * Over limit duration. Values above a certain threshold (for example
     * swells).
     */
    durasi_batas_maksimum(37),
    /**
     * Over limit magnitude. Values above a certain threshold (for example
     * swells).
     */
    over_limit_magnitude(38),
    /**
     * Missing threshold. Values considered as missing (for example
     * interruptions).
     */
    ambang_batas_hilang(39),
    /**
     * Missing occurrence counter. Values considered as missing (for example
     * interruptions).
     */
    jumlah_kejadian_hilang(40),
    /**
     * Missing duration. Values considered as missing (for example
     * interruptions).
     */
    durasi_hilang(41),
    /**
     * Missing magnitude. Values considered as missing (for example
     * interruptions).
     */
    missing_magnitude(42),
    /**
     * Time threshold for under limit
     */
    batas_waktu_kejadian_di_bawah_batas_minimum(43),
    /**
     * Time threshold for over limit
     */
    batas_waktu_kejadian_melebihi_batas_maksimum(44),
    /**
     * Time threshold for missing magnitude
     */
    batas_waktu_kejadian_hilang(45),
    /**
     * Contracted value
     */
    contracted_value(46),
    /**
     * Minimum for recording interval 1
     */
    minimum_for_recording_interval_1(51),
    /**
     * Minimum for recording interval 2
     */
    minimum_for_recording_interval_2(52),
    /**
     * Maximum for recording interval 1
     */
    maksimum_for_recording_interval_1(53),
    /**
     * Maximum for recording interval 2
     */
    maksimum_for_recording_interval_2(54),
    /**
     * Test average
     */
    rata_minq_rata_hasil_test(55),
    /**
     * Current average 4. For harmonics measurement
     */
    DE_4_(56),
    /**
     * Time integral 4 ("Test time integral�). Time integral of the quantity
     * calculated over a time specific to the device or determined by test
     * equipment.
     */
    TEST_TI(58),
    /**
     * Reserved
     *
     * Reference : Blue Book Table 62.
     */
    reserved(59);

    private final int dValue;
    private static java.util.HashMap<Integer, ElectricityDFieldName> mappings;

    private static java.util.HashMap<Integer, ElectricityDFieldName> getMappings() {
        synchronized (ElectricityDFieldName.class) {
            if (mappings == null) {
                mappings = new java.util.HashMap<>();
            }
        }
        return mappings;
    }

    ElectricityDFieldName(final int value) {
        dValue = value;
        getMappings().put(value, this);
    }

    /**
     * Get D value from enum.
     *
     * @return Nilai D dari object electricity (A adalah 1).
     */
    public int getdValue() {
        return dValue;
    }

    /**
     * Convert integer for enum value.
     *
     * @param value Nilai D dari OBIS electricity object (A adalah 1).
     * @return ElectricityDCodes enum value.
     */
    public static ElectricityDFieldName forValue(final int value) {
        if (getMappings().containsKey(value)) {
            return getMappings().get(value);
        } else {
            return getMappings().get(59);
        }
    }

    /**
     * Get value group D codes � Electricity processing of measurement values as
     * String.
     *
     * Reference : Blue Book Table 62.
     *
     * @return String of processing of measurement values
     */
    public String getString() {
        return forValue(dValue).toString().replace("minq", "-");
    }
}
