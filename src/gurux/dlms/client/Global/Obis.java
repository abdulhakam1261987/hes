/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gurux.dlms.client.Global;

import gurux.dlms.client.Global.util.ObisDescriptor;
import gurux.dlms.client.Global.util.fieldname.ObisFieldName;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import java.util.ArrayList;
import java.util.List;

/**
 * Class untuk mendefinisikan OBIS
 *
 * @author Tab Solutions team
 */
public final class Obis {

    /**
     * Object dari OBIS.
     *
     * @see GXDLMSObject
     */
    private final GXDLMSObject object;
    /**
     * Logical name dari OBIS.
     */
    private final String logicalName;
    /**
     * Field name dari OBIS.
     *
     * @see ObisFieldName.
     */
    private final String fieldName;
    /**
     * Deskripsi dari OBIS.
     *
     * @see ObisDescriptor.
     */
    private final String deskripsi;
    /**
     * Value group A.
     */
    private final int a;
    /**
     * Value group B.
     */
    private final int b;
    /**
     * Value group C.
     */
    private final int c;
    /**
     * Value group D.
     */
    private final int d;
    /**
     * Value group E.
     */
    private final int e;
    /**
     * Value group F.
     */
    private final int f;

    /**
     * Constructor
     *
     * @param object GXDLMSObject data type.
     * @see GXDLMSObject
     */
    public Obis(GXDLMSObject object) {
        this.object = object;
        this.logicalName = this.object.getLogicalName();
        int titikKe = 0;
        String as = "";
        String bs = "";
        String cs = "";
        String ds = "";
        String es = "";
        String fs = "";
        for (int i = 0; i < this.logicalName.length(); i++) {
            if (this.logicalName.substring(i, i + 1).equals(".")) {
                titikKe++;
                continue;
            }
            switch (titikKe) {
                case 0:
                    as = as + this.logicalName.substring(i, i + 1);
                    break;
                case 1:
                    bs = bs + this.logicalName.substring(i, i + 1);
                    break;
                case 2:
                    cs = cs + this.logicalName.substring(i, i + 1);
                    break;
                case 3:
                    ds = ds + this.logicalName.substring(i, i + 1);
                    break;
                case 4:
                    es = es + this.logicalName.substring(i, i + 1);
                    break;
                case 5:
                    fs = fs + this.logicalName.substring(i, i + 1);
                    break;
                default:
                    break;
            }
        }
        this.a = Integer.parseInt(as);
        this.b = Integer.parseInt(bs);
        this.c = Integer.parseInt(cs);
        this.d = Integer.parseInt(ds);
        this.e = Integer.parseInt(es);
        this.f = Integer.parseInt(fs);
        this.deskripsi = setDeskripsi();
        this.fieldName = setFieldName();
    }

    /**
     * Get object dari OBIS yang digunakan.
     *
     * @return GXDLMSObject data type
     */
    public GXDLMSObject getObject() {
        return object;
    }

    /**
     * Get logical name dari OBIS yang digunakan.
     *
     * @return String data type.
     */
    public String getLogicalName() {
        return logicalName;
    }

    /**
     * Get deskripsi dari OBIS yang digunakan.
     *
     * @return String data type.
     */
    public String getDeskripsi() {
        if (this.deskripsi == null || this.deskripsi.equals(" ") || this.deskripsi.equals("")) {
            return this.object.getDescription();
        } else {
            return deskripsi;
        }
    }

    /**
     * Get field name dari OBIS yang digunakan.
     *
     * @return String data type.
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     * Get group A value dari OBIS yang digunakan.
     *
     * @return Integer data type.
     */
    public int getA() {
        return a;
    }

    /**
     * Get group B value dari OBIS yang digunakan.
     *
     * @return Integer data type.
     */
    public int getB() {
        return b;
    }

    /**
     * Get group C value dari OBIS yang digunakan.
     *
     * @return Integer data type.
     */
    public int getC() {
        return c;
    }

    /**
     * Get group D value dari OBIS yang digunakan.
     *
     * @return Integer data type.
     */
    public int getD() {
        return d;
    }

    /**
     * Get group E value dari OBIS yang digunakan.
     *
     * @return Integer data type.
     */
    public int getE() {
        return e;
    }

    /**
     * Get group F value dari OBIS yang digunakan.
     *
     * @return Integer data type.
     */
    public int getF() {
        return f;
    }

    /**
     * Static method untuk mendapatan jumlah attribute dari object type.
     *
     * @param objectType Object type dari OBIS yang digunakan.
     * @return Integer data type.
     * @see ObjectType
     */
    public static int getAttributeIndexToRead(ObjectType objectType) {
        switch (objectType) {
            case DATA:
                return 2;
            case REGISTER:
                return 3;
            case EXTENDED_REGISTER:
                return 5;
            case DEMAND_REGISTER:
                return 9;
            case CLOCK:
                return 9;
            default:
                return 0;
        }
    }

    private String setDeskripsi() {
        return ObisDescriptor.getDeskripsi(logicalName);
    }

    private String setFieldName() {
        return ObisFieldName.getFieldName(logicalName);
    }

    /**
     * Create new GXDLMSObject
     *
     * @param logicalName New object logical name.
     * @param objectType New object object_type.
     * @return New GXDLMSObject instance with version 0
     */
    static GXDLMSObject createNewObject(String logicalName, ObjectType objectType) {
        return createNewObject(logicalName, objectType, 0);
    }

    /**
     * Create new GXDLMSObject
     *
     * @param logicalName New object logical name.
     * @param objectType New object object_type.
     * @param version New object version.
     * @return New GXDLMSObject instance
     */
    static GXDLMSObject createNewObject(String logicalName, ObjectType objectType, int version) {
        GXDLMSObject object = new GXDLMSObject();
        object.setLogicalName(logicalName);
        object.setDescription(ObisDescriptor.getDeskripsi(logicalName));
        object.setObjectType(objectType);
        object.setVersion(version);
        return object;
    }

    /**
     * Get COSEM object for load profile reading;
     *
     * @return GXDLMSProfileGeneric object
     */
    public static GXDLMSProfileGeneric getLoadProfileObject() {
        GXDLMSProfileGeneric profileGeneric = new GXDLMSProfileGeneric();
        profileGeneric.setVersion(1);
        profileGeneric.setDescription("Load Profile");
        profileGeneric.setObjectType(ObjectType.PROFILE_GENERIC);
        switch (Meter.meterType) {
            case EDMI_Mk10M:
                profileGeneric.setLogicalName("0.0.99.1.0.255");
                break;
            default:
                profileGeneric.setLogicalName("1.0.99.1.0.255");
                break;
        }
        return profileGeneric;
    }

    /**
     * Get COSEM object for load profile reading;
     *
     * @return GXDLMSProfileGeneric object
     */
    public static GXDLMSProfileGeneric getEndOfBillingObject() {
        GXDLMSProfileGeneric profileGeneric = new GXDLMSProfileGeneric();
        profileGeneric.setVersion(1);
        profileGeneric.setDescription("End of Billing");
        profileGeneric.setObjectType(ObjectType.PROFILE_GENERIC);
        switch (Meter.meterType) {
            case EDMI_Mk10M:
                profileGeneric.setLogicalName("0.0.98.1.0.255");
                break;
            case WASION_iMeter310:
            case WASION_iMeter318:
                profileGeneric.setLogicalName("1.0.98.1.1.255");
                break;
            default:
                profileGeneric.setLogicalName("1.0.98.1.0.255");
                break;
        }
        return profileGeneric;
    }

    /**
     * Get logical name list of event object.
     *
     * @return daftar COSEM object event.
     */
    public static List<GXDLMSObject> getEventObject() {
        List<GXDLMSObject> objects = new ArrayList<>();
        objects.add(createNewObject("1.97.99.98.10.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.11.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.12.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.13.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.15.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.16.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.17.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.18.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.19.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.21.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.22.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.23.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.24.255", ObjectType.PROFILE_GENERIC, 1));
        return objects;
    }

    /**
     * Get logical name list of alarm object.
     *
     * @return daftar COSEM object alarm.
     */
    public static List<GXDLMSObject> getAlarmObject() {
        List<GXDLMSObject> objects = new ArrayList<>();
        objects.add(createNewObject("1.97.99.98.0.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.1.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.2.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.3.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.4.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.5.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.6.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.7.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.8.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.9.255", ObjectType.PROFILE_GENERIC, 1));
        return objects;
    }

    /**
     * Get logical name list of tamper object.
     *
     * @return daftar COSEM object tamper.
     */
    public static List<GXDLMSObject> getTamperObject() {
        List<GXDLMSObject> objects = new ArrayList<>();
        objects.add(createNewObject("1.97.99.98.25.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.25.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.25.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.26.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.27.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.28.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.29.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.30.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.31.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.32.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.33.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.34.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.35.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.36.255", ObjectType.PROFILE_GENERIC, 1));
        objects.add(createNewObject("1.97.99.98.37.255", ObjectType.PROFILE_GENERIC, 1));
        return objects;
    }

    /**
     * Get logical name list of object.
     *
     * @return daftar COSEM object.
     */
    public static List<GXDLMSObject> getCustomObject() {
        List<GXDLMSObject> objects = new ArrayList<>();
        objects.add(createNewObject("0.0.96.1.0.255", ObjectType.forValue(1), 0));
        objects.add(createNewObject("0.0.15.0.0.255", ObjectType.forValue(22), 0));
        objects.add(createNewObject("0.0.1.0.0.255", ObjectType.forValue(8), 0));
        objects.add(createNewObject("1.0.1.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.1.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.1.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.2.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.2.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.2.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.15.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.15.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.15.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.16.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.16.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.16.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.3.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.3.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.3.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.4.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.4.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.4.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.133.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.133.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.133.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.134.9.1.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.134.9.2.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.134.9.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.1.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.2.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.15.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.16.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.3.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.4.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.133.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.134.8.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.9.6.1.255", ObjectType.forValue(4), 0));
        objects.add(createNewObject("1.0.9.6.2.255", ObjectType.forValue(4), 0));
        objects.add(createNewObject("1.0.9.6.0.255", ObjectType.forValue(4), 0));
        objects.add(createNewObject("1.0.10.6.1.255", ObjectType.forValue(4), 0));
        objects.add(createNewObject("1.0.10.6.2.255", ObjectType.forValue(4), 0));
        objects.add(createNewObject("1.0.10.6.0.255", ObjectType.forValue(4), 0));
        objects.add(createNewObject("1.0.9.2.0.255", ObjectType.forValue(3), 0));
        objects.add(createNewObject("1.0.10.2.0.255", ObjectType.forValue(3), 0));
        return objects;
    }

    /**
     * Get logical name list of meter off object.
     *
     * @return daftar COSEM object.
     */
    public static List<GXDLMSObject> getMeterOffObject() {
        List<GXDLMSObject> objects = new ArrayList<>();
        objects.add(createNewObject("0.0.96.7.19.255", ObjectType.REGISTER, 0));
        objects.add(createNewObject("0.0.96.7.21.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.96.7.20.255", ObjectType.REGISTER, 0));
        objects.add(createNewObject("1.0.99.97.0.255", ObjectType.PROFILE_GENERIC, 1));
        return objects;
    }

    /**
     * Get logical name list of meter info object.
     *
     * @return daftar COSEM object.
     */
    public static List<GXDLMSObject> getMeterInfoObject() {
        List<GXDLMSObject> objects = new ArrayList<>();
        objects.add(createNewObject("0.0.1.0.0.255", ObjectType.CLOCK, 0));
        objects.add(createNewObject("1.0.14.7.0.255", ObjectType.REGISTER, 0));
        objects.add(createNewObject("0.0.10.0.0.255", ObjectType.SCRIPT_TABLE, 0));
        objects.add(createNewObject("0.0.10.0.128.255", ObjectType.SCRIPT_TABLE, 0));
        objects.add(createNewObject("0.0.10.0.129.255", ObjectType.SCRIPT_TABLE, 0));
        objects.add(createNewObject("0.0.10.0.130.255", ObjectType.SCRIPT_TABLE, 0));
        objects.add(createNewObject("0.0.15.0.0.255", ObjectType.ACTION_SCHEDULE, 0));
        objects.add(createNewObject("0.0.96.1.0.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.96.1.1.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.96.1.2.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.96.1.3.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.96.5.96.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.96.5.97.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.96.6.13.255", ObjectType.REGISTER, 0));
        objects.add(createNewObject("0.0.96.6.3.255", ObjectType.REGISTER, 0));
        objects.add(createNewObject("0.0.96.96.0.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.1.96.20.46.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.1.96.7.0.255", ObjectType.DATA, 0));
        objects.add(createNewObject("1.0.0.2.0.255", ObjectType.DATA, 0));
        objects.add(createNewObject("1.0.0.2.8.255", ObjectType.DATA, 0));
        objects.add(createNewObject("1.0.0.3.0.255", ObjectType.REGISTER, 0));
        objects.add(createNewObject("1.2.0.2.0.255", ObjectType.DATA, 0));
        objects.add(createNewObject("1.2.0.2.8.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.1.0.2.0.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.1.96.6.3.255", ObjectType.REGISTER, 0));
        objects.add(createNewObject("0.2.0.2.0.255", ObjectType.DATA, 0));
        objects.add(createNewObject("0.0.0.2.0.255", ObjectType.DATA, 0));
        return objects;
    }
}
