/*
 * Tab Solutions
 */
package gurux.dlms.client.Global;

import gurux.dlms.enums.Unit;

/**
 * Class untuk mendefinisikan satuan, ukuran nama satuan serta definisi satuan
 * dalam standard internasional. Reference Blue Book Table 4 (Enumerated values
 * for physical.
 *
 * @author Tab Solutions team
 * @see Unit
 */
public class PhysicalUnit {

    /**
     * Satuan dari unit yang digunakan.
     */
    private final String satuan;
    /**
     * Ukuran dari unit yang digunakan.
     */
    private final String ukuran;
    /**
     * Nama satuan dari unit yang digunakan.
     */
    private final String namaSatuan;
    /**
     * Standard internasional dari unit yang digunakan.
     */
    private final String sIDefinition;
    /**
     * Unit object. Reference Blue Book Table 4 (Enumerated values for physical
     * units)
     *
     * @see Unit
     */
    private final Unit unit;

    /**
     * Constructor class
     *
     * @param satuan Satuan dari unit yang digunakan.
     * @param ukuran Ukuran dari unit yang digunakan.
     * @param namaSatuan Nama satuan dari unit yang digunakan.
     * @param sIDefinition Standard internasional dari unit yang digunakan.
     * @param unit Unit object. Reference Blue Book Table 4 (Enumerated values
     * for physical units)
     *
     * @see Unit
     */
    PhysicalUnit(String satuan, String ukuran, String namaSatuan, String sIDefinition, Unit unit) {
        this.satuan = satuan;
        this.ukuran = ukuran;
        this.namaSatuan = namaSatuan;
        this.sIDefinition = sIDefinition;
        this.unit = unit;
    }

    /**
     * Get unit enumeration for physical unit in integer data type. Reference
     * Blue Book Table 4 (Enumerated values for physical units)
     *
     * @return Integer data type.
     */
    public int getUnitEnumeration() {
        return unit.getValue();
    }

    /**
     * Get satuan dalam bentuk String.
     *
     * @return String data type.
     */
    public String getSatuan() {
        if (satuan.contains("?")) {
            return namaSatuan;
        } else {
            return satuan;
        }
    }

    /**
     * Get ukuran dalam bentuk String.
     *
     * @return String data type.
     */
    public String getUkuran() {
        return ukuran;
    }

    /**
     * Get nama satuan dalam bentuk String.
     *
     * @return String data type.
     */
    public String getNamaSatuan() {
        return namaSatuan;
    }

    /**
     * Get SI definition dalam bentuk String.
     *
     * @return String data type.
     */
    public String getSIDefinition() {
        return sIDefinition;
    }

    /**
     * Get unit dalam bentuk Unit object.
     *
     * @return Unit object type.
     * @see Unit
     */
    public Unit getUnit() {
        return unit;
    }

    /**
     * Static method untuk menset variabel berdasarkan enumerationnya. Reference
     * Blue Book Table 4 (Enumerated values for physical units).
     *
     * @param unitEnumeration bilangan enumeration dari unit yang digunakan.
     * @return PhysicalUnit object type.
     */
    public static PhysicalUnit getPhysicalUnit(int unitEnumeration) {
        return getPhysicalUnit(Unit.forValue(unitEnumeration));
    }

    /**
     * Static method untuk menset variabel berdasarkan unit objectnya. Reference
     * Blue Book Table 4 (Enumerated values for physical units).
     *
     * @param unit Unit yang digunakan
     * @return PhysicalUnit object type.
     * @see Unit
     */
    public static PhysicalUnit getPhysicalUnit(Unit unit) {
        String satuan = "";
        String ukuran = "";
        String namaSatuan = "";
        String sIDefinition = "";
        String x = String.valueOf((char) 215);
        switch (unit) {
            case YEAR:
                satuan = "a";
                ukuran = "Waktu";
                namaSatuan = "Tahun";
                break;
            case MONTH:
                satuan = "mo";
                ukuran = "Waktu";
                namaSatuan = "Bulan";
                break;
            case WEEK:
                satuan = "wk";
                ukuran = "Waktu";
                namaSatuan = "Minggu";
                sIDefinition = "7" + x + PhysicalUnit.getPhysicalUnit(Unit.DAY).sIDefinition;
                break;
            case DAY:
                satuan = "d";
                ukuran = "Waktu";
                namaSatuan = "Hari";
                sIDefinition = "24" + x + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition;
                break;
            case HOUR:
                satuan = "h";
                ukuran = "Waktu";
                namaSatuan = "Jam";
                sIDefinition = "60" + x + PhysicalUnit.getPhysicalUnit(Unit.MINUTE).sIDefinition;
                break;
            case MINUTE:
                satuan = "min";
                ukuran = "Waktu";
                namaSatuan = "Menit";
                sIDefinition = "60" + PhysicalUnit.getPhysicalUnit(Unit.SECOND).sIDefinition;
                break;
            case SECOND:
                satuan = "s";
                ukuran = "Waktu";
                namaSatuan = "Detik";
                sIDefinition = "s";
                break;
            case PHASE_ANGLE_DEGREE:
                satuan = (char) 176 + "";
                ukuran = "Sudut Phasa";
                namaSatuan = "Derajat";
                sIDefinition = "rad" + x + "180/" + Math.PI;
                break;
            case TEMPERATURE:
                satuan = (char) 176 + "C";
                ukuran = "Temperature (T)";
                namaSatuan = "Derajat Celsius";
                sIDefinition = "K-273.15";
                break;
            case LOCAL_CURRENCY:
                satuan = "currency";
                ukuran = "(local) currency";
                break;
            case LENGTH:
                satuan = "m";
                ukuran = "Panjang (l)";
                namaSatuan = "meter";
                sIDefinition = "m";
                break;
            case SPEED:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.LENGTH).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.SECOND).satuan;
                ukuran = "Kecepatan (v)";
                namaSatuan = "meter per detik";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.LENGTH).sIDefinition + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.SECOND).sIDefinition;
                break;
            case VOLUME_CUBIC_METER:
                satuan = "m�";
                ukuran = "Volume (V), rV, meter constant or pulse value (volume)";
                namaSatuan = "meter kubik";
                sIDefinition = "m�";
                break;
            case CORRECTED_VOLUME:
                satuan = "m�";
                ukuran = "corrected volume";
                namaSatuan = "meter kubik";
                sIDefinition = "m�";
                break;
            case VOLUME_FLUX_HOUR:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "volume flux";
                namaSatuan = "meter kubik per jam";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).sIDefinition + "/("
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition + ")";
                break;
            case CORRECTED_VOLUME_FLUX_HOUR:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "corrected volume flux";
                namaSatuan = "meter kubik per jam";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).sIDefinition + "/("
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition + ")";
                break;
            case VOLUME_FLUXDAY:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.DAY).satuan;
                ukuran = "volume flux";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).sIDefinition + "/("
                        + PhysicalUnit.getPhysicalUnit(Unit.DAY).sIDefinition + ")";
                break;
            case CORRECTED_VOLUME_FLUX_DAY:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.DAY).satuan;
                ukuran = "corrected volume flux";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).sIDefinition + "/("
                        + PhysicalUnit.getPhysicalUnit(Unit.DAY).sIDefinition + ")";
                break;
            case VOLUME_LITER:
                satuan = "l";
                ukuran = "Volume";
                namaSatuan = "Liter";
                sIDefinition = "10-� " + PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).sIDefinition;
                break;
            case MASS_KG:
                satuan = "kg";
                ukuran = "Massa (m)";
                namaSatuan = "Kilogram";
                sIDefinition = "kg";
                break;
            case FORCE:
                satuan = "N";
                ukuran = "force (F)";
                namaSatuan = "Newton";
                break;
            case ENERGY:
                satuan = "Nm";
                ukuran = "Energy";
                namaSatuan = "newton meter";
                sIDefinition = "J = Nm = Ws";
                break;
            case PRESSURE_PASCAL:
                satuan = "Pa";
                ukuran = "pressure (p)";
                namaSatuan = "Pascal";
                sIDefinition = "N/m�";
                break;
            case PRESSURE_BAR:
                satuan = "bar";
                ukuran = "pressure (p)";
                namaSatuan = "bar";
                sIDefinition = "10? N/m�";
                break;
            case ENERGY_JOULE:
                satuan = "J";
                ukuran = "Energy";
                namaSatuan = "Joule";
                sIDefinition = "J = Nm = Ws";
                break;
            case THERMAL_POWER:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.ENERGY_JOULE).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "Energy";
                namaSatuan = "joule per hour";
                sIDefinition = "(" + PhysicalUnit.getPhysicalUnit(Unit.ENERGY_JOULE).sIDefinition + ")/"
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition;
                break;
            case ACTIVE_POWER:
                satuan = "W";
                ukuran = "Daya aktif (P)";
                namaSatuan = "watt";
                sIDefinition = "W = (" + PhysicalUnit.getPhysicalUnit(Unit.ENERGY_JOULE).sIDefinition + ")/"
                        + PhysicalUnit.getPhysicalUnit(Unit.SECOND).sIDefinition;
                break;
            case APPARENT_POWER:
                satuan = "VA";
                ukuran = "Daya semu (S)";
                namaSatuan = "volt-ampere";
                break;
            case REACTIVE_POWER:
                satuan = "var";
                ukuran = "Daya reaktif (Q)";
                namaSatuan = "var";
                break;
            case ACTIVE_ENERGY:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.ACTIVE_POWER).satuan
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "Energi aktif, rW, active energy meter constant or pulse value";
                namaSatuan = "watt-hour";
                sIDefinition = "(" + PhysicalUnit.getPhysicalUnit(Unit.ACTIVE_POWER).sIDefinition
                        + ")" + x + "(" + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition + ")";
                break;
            case APPARENT_ENERGY:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.APPARENT_POWER).satuan
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "Energi semu, rS, apparent energy meter constant or pulse value";
                namaSatuan = "volt-ampere-hour";
                sIDefinition = "VA" + x + "(" + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition + ")";
                break;
            case REACTIVE_ENERGY:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.REACTIVE_POWER).satuan
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "Energi reaktif, rB, reactive energy meter constant or pulse value";
                namaSatuan = "var-hour";
                sIDefinition = "var" + x + "(" + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition + ")";
                break;
            case CURRENT:
                satuan = "A";
                ukuran = "Arus (I)";
                namaSatuan = "ampere";
                sIDefinition = "A";
                break;
            case ELECTRICAL_CHARGE:
                satuan = "C";
                ukuran = "Electrical charge (Q)";
                namaSatuan = "coulomb";
                sIDefinition = "C = As";
                break;
            case VOLTAGE:
                satuan = "V";
                ukuran = "Tegangan (U), Voltage";
                namaSatuan = "volt";
                sIDefinition = "V";
                break;
            case ELECTRICAL_FIELD_STRENGTH:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.VOLTAGE).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.LENGTH).satuan;
                ukuran = "electric field strength (E)";
                namaSatuan = "volt per meter";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.VOLTAGE).sIDefinition + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.LENGTH).sIDefinition;
                break;
            case CAPACITY:
                satuan = "F";
                ukuran = "Kapasitas (C), Capacitance";
                namaSatuan = "farad";
                sIDefinition = "(" + PhysicalUnit.getPhysicalUnit(Unit.ELECTRICAL_CHARGE).sIDefinition + ")/"
                        + PhysicalUnit.getPhysicalUnit(Unit.VOLTAGE).sIDefinition;
                break;
            case RESISTANCE:
                satuan = "?";
                ukuran = "Hambatan (R), Resistance";
                namaSatuan = "ohm";
                sIDefinition = "? = " + PhysicalUnit.getPhysicalUnit(Unit.VOLTAGE).sIDefinition + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.CURRENT).sIDefinition;
                break;
            case RESISTIVITY:
                satuan = "?m�/m";
                ukuran = "Resistivity";
                sIDefinition = "?m";
                break;
            case MAGNETIC_FLUX:
                satuan = "Wb";
                ukuran = "Magnetic flux (" + (char) 248 + ")";
                namaSatuan = "weber";
                sIDefinition = "Wb = Vs";
                break;
            case INDUCTION:
                satuan = "T";
                ukuran = "Magnetic flux density (B)";
                namaSatuan = "tesla";
                sIDefinition = "Wb/m�";
                break;
            case MAGNETIC:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.CURRENT).satuan + "/m";
                ukuran = "Magnetic field strength (H)";
                namaSatuan = "ampere per meter";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.CURRENT).sIDefinition + "/m";
                break;
            case INDUCTIVITY:
                satuan = "H";
                ukuran = "Inductance (L)";
                namaSatuan = "henry";
                sIDefinition = "H = (" + PhysicalUnit.getPhysicalUnit(Unit.MAGNETIC_FLUX).sIDefinition
                        + ")/" + PhysicalUnit.getPhysicalUnit(Unit.CURRENT).sIDefinition;
                break;
            case FREQUENCY:
                satuan = "Hz";
                ukuran = "Frequency";
                namaSatuan = "hertz";
                sIDefinition = "1/s";
                break;
            case ACTIVE:
                satuan = "1/" + PhysicalUnit.getPhysicalUnit(Unit.ACTIVE_ENERGY).getSatuan();
                ukuran = "Rw, active energy meter constant or pulse value";
                break;
            case REACTIVE:
                satuan = "1/" + PhysicalUnit.getPhysicalUnit(Unit.REACTIVE_ENERGY).getSatuan();
                ukuran = "Rb, reactive energy meter constant or pulse value";
                break;
            case APPARENT:
                satuan = "1/" + PhysicalUnit.getPhysicalUnit(Unit.APPARENT_ENERGY).getSatuan();
                ukuran = "Rs, apparent energy meter constant or pulse value";
                break;
            case V260:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.VOLTAGE).satuan + "�"
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "volt-squared hour, rU2h , volt-squared hour meter constant or pulse value";
                namaSatuan = "volt-squared-hours";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.VOLTAGE).sIDefinition + "�("
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition + ")";
                break;
            case A260:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.CURRENT).satuan + "�"
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).satuan;
                ukuran = "ampere-squared hour, rI2h , ampere-squared hour meter constant or pulse value";
                namaSatuan = "ampere-squared-hours";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.CURRENT).sIDefinition + "�("
                        + PhysicalUnit.getPhysicalUnit(Unit.HOUR).sIDefinition + ")";
                break;
            case MASS_KG_PER_SECOND:
                satuan = PhysicalUnit.getPhysicalUnit(Unit.MASS_KG).satuan + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.SECOND).satuan;
                ukuran = "mass flux";
                namaSatuan = "kilogram per second";
                sIDefinition = PhysicalUnit.getPhysicalUnit(Unit.MASS_KG).sIDefinition + "/"
                        + PhysicalUnit.getPhysicalUnit(Unit.SECOND).sIDefinition;
                break;
            case CONDUCTANCE:
                satuan = "S, mho";
                ukuran = "conductance";
                namaSatuan = "siemens";
                sIDefinition = "1/?";
                break;
            case KELVIN:
                satuan = "K";
                ukuran = "Temperature (T)";
                namaSatuan = "kelvin";
                break;
            case V2H:
                satuan = "1/(" + PhysicalUnit.getPhysicalUnit(Unit.V260).getSatuan() + ")";
                ukuran = "RU2h , volt-squared hour meter constant or pulse value";
                break;
            case A2H:
                satuan = "1/(" + PhysicalUnit.getPhysicalUnit(Unit.A260).getSatuan() + ")";
                ukuran = "RI2h , ampere-squared hour meter constant or pulse value";
                break;
            case CUBIC_METER_RV:
                satuan = "1/(" + PhysicalUnit.getPhysicalUnit(Unit.VOLUME_CUBIC_METER).getSatuan() + ")";
                ukuran = "Rv , meter constant or pulse value (volume)";
                break;
            case PERCENTAGE:
                ukuran = "percentage";
                namaSatuan = "%";
                break;
            case AMPERE_HOURS:
                satuan = "Ah";
                ukuran = "ampere-hours";
                namaSatuan = "ampere-hour";
                break;
            case ENERGY_PER_VOLUME:
                satuan = "Wh/m�";
                ukuran = "energy per volume";
                namaSatuan = "3,6" + x + "10�J/m�";
                break;
            case WOBBE:
                satuan = "J/m�";
                ukuran = "calorific value, wobbe";
                break;
            case MOLE_PERCENT:
                satuan = "Mol %";
                ukuran = "molar fraction of gas composition";
                namaSatuan = "mole percent";
                sIDefinition = "(Basic gas composition unit)";
                break;
            case MASS_DENSITY:
                satuan = "g/m�";
                ukuran = "mass density, quantity of material";
                sIDefinition = "(Gas analysis, accompanying elements)";
                break;
            case PASCAL_SECOND:
                satuan = "Pa s";
                ukuran = "Dynamic viscosity";
                namaSatuan = "pascal second";
                sIDefinition = "(Characteristic of gas stream)";
                break;
            case JOULE_KILOGRAM:
                satuan = "J/kg";
                ukuran = "spesific energy.\n\tNOTE : The amount of energy per unit of mass of a substance";
                namaSatuan = "Joule / kilogram";
                String o = String.valueOf((char) 183);
                sIDefinition = "m�" + o + "kg" + o + "s-�/kg = m�" + o + "s-�";
                break;
            case SIGNAL_STRENGTH:
                satuan = "dBm";
                ukuran = "signal strength, dB miliwatt (e.g. of GSM radio systems)";
                break;
            case SIGNAL_STRENGTH_MICROVOLT:
                satuan = "db" + (char) 181 + "V";
                ukuran = "signal strength, dB microvolt";
                break;
            case SIGNAL_STRENGTH_LOGARITHMIC:
                satuan = "db";
                ukuran = "logarithmic unit that expresses the ratio between two values of "
                        + "a physical quantity";
                break;
            case OTHER_UNIT:
                ukuran = "other unit";
                break;
            case NO_UNIT:
                ukuran = "no unit, unitless, count";
                break;
        }
        return new PhysicalUnit(satuan, ukuran, namaSatuan, sIDefinition, unit);
    }

    /**
     * Overriding method toString dari class Object.
     *
     * @return String data type.
     */
    @Override
    public String toString() {
        String hasil = unit.name() + "{\n"
                + "\tUnit Enumeration : " + unit.getValue() + ".\n"
                + "\tNama Unit : " + unit.toString() + ".\n";
        if (!satuan.equals("")) {
            hasil += "\tSatuan : " + satuan + ".\n";
        }
        if (!ukuran.equals("")) {
            hasil += "\tUkuran : " + ukuran + ".\n";
        }
        if (!namaSatuan.equals("")) {
            hasil += "\tNama Satuan : " + namaSatuan + ".\n";
        }
        if (!sIDefinition.equals("")) {
            hasil += "\tSI Definition : " + sIDefinition + ".\n";
        }
        hasil += "}";
        return hasil;
    }
    
    public static String secondToDuration(int second){
        int jam = (int) second/3600;
        int menit = (int) (second - (jam * 3600))/60;
        int detik = (int) (second - ((jam * 3600) + (menit * 60)));
        return jam + ":" + menit + ":" + detik;
    }
}
