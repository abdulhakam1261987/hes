package gurux.dlms.client.Global;

import gurux.dlms.enums.Unit;
import java.text.DecimalFormatSymbols;

/**
 * Class untuk pendefinisian skala dan unit register object.
 *
 * @author Tab Solutions team
 */
public class SkaladanUnit {

    /**
     * Skala dalam format double
     */
    private double skala;
    /**
     * Unit object. Reference Blue Book Table 4 (Enumerated values for physical
     * units)
     *
     * @see Unit
     */
    private Unit unit;

    /**
     * Default constructor
     */
    public SkaladanUnit() {
    }

    /**
     * Constructor class
     *
     * @param val Hasil baca attribut scaler_unit.
     */
    public SkaladanUnit(String val) {
        String nilaiAsli = String.valueOf(val);
        int spasiKe = 0;
        String scaller = "";
        String unitt = "";
        boolean readSkala = false;
        boolean readUnit = false;
        int upperunit = 0;
        for (int i = 0; i < nilaiAsli.length(); i++) {
            String karakter = nilaiAsli.substring(i, i + 1);
            if (karakter.equals(" ") && spasiKe == 0) {
                spasiKe++;
                readSkala = true;
            } else if (karakter.equals(" ") && spasiKe == 1) {
                spasiKe++;
                readSkala = false;
            } else if (karakter.equals(" ") && spasiKe == 2) {
                spasiKe++;
                readUnit = true;
            } else if (karakter.equals(" ") && spasiKe == 3) {
                break;
            }
            if (readSkala) {
                scaller = scaller + karakter;
            }
            if (readUnit) {
                if (karakter.equals(" ")) {
                    continue;
                }
                if (karakter.equals(karakter.toUpperCase()) && upperunit > 0) {
                    unitt = unitt + "_" + karakter;
                } else {
                    if (karakter.equals(karakter.toUpperCase())) {
                        unitt = unitt + karakter;
                        upperunit++;
                    } else {
                        unitt = unitt + karakter;
                    }
                }
            }
        }
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        int a = dfs.getDecimalSeparator();
        if (a == 46) {
            this.skala = Double.parseDouble(scaller);
        } else {
            scaller = scaller.replace(dfs.getDecimalSeparator(), '.');
            this.skala = Double.parseDouble(scaller);
        }
        try {
            this.unit = Unit.valueOf(unitt.trim().toUpperCase().replace(" ", "_"));
        } catch (Exception e) {
            if (unitt.equals("NOUNIT")) {
                this.unit = Unit.NO_UNIT;
            } else {
                this.unit = Unit.NONE;
            }
        }
    }

    /**
     * Get skala in double format.
     *
     * @return double type number
     */
    public double getSkala() {
        return skala;
    }

    /**
     * Set skala.
     *
     * @param skala Skala yang digunakan dalam format double.
     */
    public void setSkala(double skala) {
        this.skala = skala;
    }

    /**
     * Get unit in Unit object type.
     *
     * @return Unit object type.
     *
     * @see Unit
     */
    public Unit getUnit() {
        return unit;
    }

    /**
     * Set unit.
     *
     * @param unit Unit object type.
     *
     * @see Unit
     */
    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
