package gurux.dlms.client;

import gurux.dlms.communication.Communicator;
import gurux.dlms.communication.Reader;
import gurux.dlms.communication.Writer;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.object.meter.DCUTIDEMeterListObj;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.object.meter.MeterType;
import gurux.net.GXNet;
import gurux.serial.GXSerial;

/**
 * Test class untuk mencoba komunikasi dengan meter.
 *
 * @author Tab Solutions Team
 */
public class TestClass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Meter.setMeter(MeterType.OTHER);
        Meter.authentication = Authentication.LOW;
        Meter.clientAddress = 17;
        Meter.logicalAddress = 0;
        Meter.physicalAddress = 1;
        Meter.password = "TIDE";
//        Meter.isIEC = true;
        Meter.interfaceType = InterfaceType.HDLC;
//        Meter.serialPort = "COM3";
//        Meter.authenticationKey = "1111111111111111";
        Meter.traceReadingLog = true;
        Meter.hostname = "211.108.34.184";
        Meter.port = 4061;
//        Meter.addressSize = 4;
//        Meter.invocation_counter = 1;
        System.out.println(Meter.tostring());
        try {
            if (Meter.isIEC) {
                Meter.media = new GXSerial();
                GXSerial serial = (GXSerial) Meter.media;
                serial.setPortName(Meter.serialPort);
            } else {
                GXNet net;
                if (Meter.media == null) {
                    Meter.media = new GXNet();
                }
                net = (GXNet) Meter.media;
                net.setHostName(Meter.hostname);
                net.setPort(Meter.port);
            }
            Communicator.setCommunicator(Meter.media);
            Meter.media.open();
            Communicator.connect();
            if (Communicator.isConnected()) {
//                Communicator.readAssociationView();
                Reader reader = new Reader();
                Writer writer = new Writer();
//                reader.readNomorMeter();
//                reader.readMeterTime();
//                writer.imageTransfer();
                reader.readTideMeterList();
                writer.addDCUTIDEMeterList(new DCUTIDEMeterListObj("Mk7MI", "7190000001", "192.168.205.101", 1480));
                reader.readTideMeterList();
//                reader.readData("0.1.96.1.130.255");
//                writer.executeKeyTransfer(SecurityKeyType.AUTHENTICATION_KEY, "1111111111111111", "0000000000000000");
//                writer.updateFirmware();
//                reader.readEventProfileGeneric("0.0.99.98.0.255");
//                reader.readActivityCalendar();
//                Calendar calendar = Calendar.getInstance();
//                calendar.add(Calendar.HOUR_OF_DAY, -1);
//                writer.setActivatePassiveCalendarDate(calendar);
//                reader.readActivityCalendar();
//                reader.readData("1.0.0.2.0.255");
//                reader.readActivityCalendar();
//                writer.addDayID(0);
//                reader.readTOU();
//                reader.readClock();
//                reader.readWaktuRekamBilling(ObjectType.DATA);
//                writer.writeWaktuRekamBilling(1, 0, 0);
//                reader.readWaktuRekamBilling(ObjectType.DATA);
                Communicator.disconnect();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
