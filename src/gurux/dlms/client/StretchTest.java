/*
 * 
 */
package gurux.dlms.client;

import gurux.dlms.communication.Communicator;
import gurux.dlms.communication.Reader;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.object.meter.MeterType;
import gurux.net.GXNet;
import gurux.serial.GXSerial;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class StretchTest {

    public static void main(String[] args) {
        Calendar startTime = Calendar.getInstance();
        HashMap<Integer, String> hm = new HashMap<>();
        int jumlahMeter = 1000, jumlahSuksesBaca = 0;
        for (int i = 0; i < jumlahMeter; i++) {
            Meter.setMeter(MeterType.OTHER);
            Meter.hostname = "127.0.0.1";
            Meter.port = 10000 + i;
            try {
                if (Meter.isIEC) {
                    Meter.media = new GXSerial();
                    GXSerial serial = (GXSerial) Meter.media;
                    serial.setPortName(Meter.serialPort);
                } else {
                    GXNet net;
                    if (Meter.media == null) {
                        Meter.media = new GXNet();
                    }
                    net = (GXNet) Meter.media;
                    net.setHostName(Meter.hostname);
                    net.setPort(Meter.port);
                }
                Communicator.setCommunicator(Meter.media);
                Meter.media.open();
                System.out.println(Meter.tostring());
                Communicator.connect();
                if (Communicator.isConnected()) {
                    Reader reader = new Reader();
                    reader.readNomorMeter();
                    Communicator.disconnect();
                    jumlahSuksesBaca++;
                }
            } catch (Exception ex) {
//                System.out.println("===========================================================================");
//                System.out.println("Failed at port " + (10000 + i));
                hm.put(10000 + i, ex.getMessage());
//                System.out.println("===========================================================================");
            }
        }
        Calendar endTime = Calendar.getInstance();
        Calendar totalTime = Calendar.getInstance();
        totalTime.setTimeInMillis(endTime.getTimeInMillis() - startTime.getTimeInMillis());
        System.out.println("Waktu mulai pembacaan = " + startTime.getTime());
        System.out.println("Waktu selesai pembacaan = " + endTime.getTime());
        System.out.println("Jumlah record dibaca = " + jumlahSuksesBaca);
        double totalWaktuPembacaan = (double) totalTime.getTimeInMillis() / 1000;
        System.out.println("Total waktu pembacaan = " + totalWaktuPembacaan + " detik");
        double rataWaktuBaca = 0;
        if (jumlahSuksesBaca > 0) {
            rataWaktuBaca = (double) (totalTime.getTimeInMillis() / jumlahSuksesBaca) / 1000;
        }
        System.out.println("Rata-rata waktu baca per record = " + rataWaktuBaca + " detik");
        System.out.println("Jumlah gagal baca = " + hm.size());
        if (hm.size() > 0) {
            System.out.println("Daftar port gagal baca:");
            for (Map.Entry<Integer, String> entry : hm.entrySet()) {
                System.out.println("\t" + entry.getKey() + ":\t" + entry.getValue());
            }
        }
    }
}
