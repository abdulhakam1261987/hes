/*
 * Tab Solutions
 */
package gurux.dlms.client;

import gurux.dlms.client.Global.util.ObisDescriptor;

/**
 * Class untuk melakukan uji coba.
 *
 * @author Tab Solutions
 */
public class UjiCoba {

    /**
     * Main method.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String ln = "1.1.11.24.5.255";
        System.out.println(ln);
        System.out.println(ObisDescriptor.getDeskripsi(ln));
    }
}
