/*
 * 
 */
package gurux.dlms.ui;

import gurux.dlms.client.Global.Obis;
import gurux.dlms.communication.Communicator;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.object.meter.MeterObject;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.net.GXNet;
import gurux.serial.GXSerial;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class StretchThread implements Runnable {

    private final MeterObject meterObject;

    private final String modeBaca;

    private Thread t;

    public StretchThread(MeterObject meterObject, String modeBaca) {
        this.meterObject = meterObject;
        this.modeBaca = modeBaca;
    }

    @Override
    public void run() {
        System.out.println("Running " + meterObject.getHostname() + ":" + meterObject.getPort() + " Thread.");
        try {
            GXNet net;
            if (meterObject.getMedia() == null) {
                meterObject.setMedia(new GXNet());
            }
            net = (GXNet) meterObject.getMedia();
            net.setHostName(meterObject.getHostname());
            net.setPort(meterObject.getPort());
            StretchCom com = new StretchCom();
            com.setCommunicator(meterObject.getMedia(), meterObject.getClient());
            meterObject.getMedia().open();
            System.out.println(meterObject.toString());
            com.connect();
            if (com.isConnected()) {
                if (this.modeBaca == "nomor meter") {
                    GXDLMSData object = new GXDLMSData("0.0.96.1.0.255");
                    com.readData(object);
                    StretchTest.jumlahRecord++;
                } else {
                    GXDLMSProfileGeneric profileGeneric = new GXDLMSProfileGeneric();
                    profileGeneric.setVersion(1);
                    profileGeneric.setDescription("Load Profile");
                    profileGeneric.setObjectType(ObjectType.PROFILE_GENERIC);
                    switch (this.meterObject.getMeterType()) {
                        case EDMI_Mk10M:
                            profileGeneric.setLogicalName("0.0.99.1.0.255");
                            break;
                        default:
                            profileGeneric.setLogicalName("1.0.99.1.0.255");
                            break;
                    }
                    com.readProfileGeneric(profileGeneric);
                }
                com.close();
            }
        } catch (InterruptedException e) {
            System.out.println("Thread @" + meterObject.getHostname() + ":" + meterObject.getPort() + " interrupted.");
        } catch (Exception e) {
            System.out.println("Catching " + e.toString());
        }
        System.out.println("Thread " + meterObject.getHostname() + ":" + meterObject.getPort() + " exiting.");
    }

    public void start() {
        System.out.println("Starting thread @" + meterObject.getHostname() + ":" + meterObject.getPort());
        if (t == null) {
            t = new Thread(this, meterObject.getMeterType().getText());
            t.start();
        }
    }
}
