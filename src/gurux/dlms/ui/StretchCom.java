/*
 * 
 */
package gurux.dlms.ui;

import gurux.common.GXCommon;
import gurux.common.IGXMedia;
import gurux.common.ReceiveParameters;
import gurux.dlms.GXByteBuffer;
import gurux.dlms.GXDLMSClient;
import gurux.dlms.GXDLMSConverter;
import gurux.dlms.GXDLMSException;
import gurux.dlms.GXDLMSTranslator;
import gurux.dlms.GXReplyData;
import gurux.dlms.GXSimpleEntry;
import gurux.dlms.TranslatorOutputType;
import gurux.dlms.client.Global.GlobalVar;
import gurux.dlms.client.Global.Obis;
import gurux.dlms.client.Global.PhysicalUnit;
import gurux.dlms.client.Global.SkaladanUnit;
import gurux.dlms.client.Global.util.ObisDescriptor;
import gurux.dlms.client.Global.util.PrintUtil;
import gurux.dlms.communication.Communicator;
import gurux.dlms.enums.Authentication;
import gurux.dlms.enums.Conformance;
import gurux.dlms.enums.DataType;
import gurux.dlms.enums.ErrorCode;
import gurux.dlms.enums.InterfaceType;
import gurux.dlms.enums.ObjectType;
import gurux.dlms.enums.Unit;
import gurux.dlms.object.meter.Meter;
import gurux.dlms.objects.GXDLMSCaptureObject;
import gurux.dlms.objects.GXDLMSClock;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSDemandRegister;
import gurux.dlms.objects.GXDLMSExtendedRegister;
import gurux.dlms.objects.GXDLMSObject;
import gurux.dlms.objects.GXDLMSObjectCollection;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.dlms.objects.GXDLMSRegister;
import gurux.dlms.objects.IGXDLMSBase;
import gurux.dlms.secure.GXDLMSSecureClient;
import gurux.net.GXNet;
import java.lang.reflect.Array;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class StretchCom {

    /**
     * Media komunikasi
     */
    IGXMedia media;

    /**
     * DLMS Meter Setting
     */
    GXDLMSClient dlms;

    /**
     * replyBuff as ByteBuffer
     */
    java.nio.ByteBuffer replyBuff;

    /**
     * Meter connection reminder.
     */
    private static boolean connected = false;

    /**
     * Logger generator
     */
    static final Logger logger = Logger.getLogger(StretchCom.class.getName());

    /**
     * Communicator Class Constructor
     *
     * @param comMedia Object IGXMedia.
     * @param client client
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    public void setCommunicator(IGXMedia comMedia, GXDLMSSecureClient client) throws Exception {
        media = comMedia;
        dlms = client;
        if (dlms.getInterfaceType() == InterfaceType.WRAPPER) {
            replyBuff = java.nio.ByteBuffer.allocate(8 + 1024);
        } else {
            replyBuff = java.nio.ByteBuffer.allocate(100);
        }
    }

    /**
     * Close meter connection
     */
    void close() throws Exception {
        if (media != null && media.isOpen()) {
            GXReplyData reply = new GXReplyData();
            try {
                readDataBlock(dlms.releaseRequest(), reply);
            } catch (Exception e) {
                // All meters don't support release.
                logger.log(Level.WARNING, "All meters don't support release.");
                logger.log(Level.WARNING, e.toString());
            }
            reply.clear();
            readDLMSPacket(dlms.disconnectRequest(), reply);
            media.close();
            connected = false;
        }
    }

    /**
     * @return String dari waktu saat ini.
     */
    String now() {
        return new SimpleDateFormat("HH:mm:ss.SSS")
                .format(java.util.Calendar.getInstance().getTime());
    }

    /**
     * Read DLMS packet
     *
     * @param data Array byte 2 dimensi.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void readDLMSPacket(byte[][] data) throws Exception {
        GXReplyData reply = new GXReplyData();
        for (byte[] it : data) {
            reply.clear();
            readDLMSPacket(it, reply);
        }
    }

    /**
     * Handle received notify messages.
     *
     * @param reply Received data.
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    private void handleNotifyMessages(final GXReplyData reply)
            throws Exception {
        List<Map.Entry<GXDLMSObject, Integer>> items = new ArrayList<>();
        Object value = dlms.parseReport(reply, items);
        // If Event notification or Information report.
        if (value == null) {
            items.forEach((it) -> {
                logger.log(Level.INFO, "{0} Value:{1}", new Object[]{it.getKey().toString(),
                    it.getKey().getValues()[it.getValue() - 1]});
            });

        } else // Show data notification.
        {
            if (value instanceof List<?>) {
                for (Object it : (List<?>) value) {
                    logger.log(Level.WARNING, "Value:{0}", String.valueOf(it));
                }
            } else {
                logger.log(Level.WARNING, "Value:{0}", String.valueOf(value));
            }

        }
        reply.clear();
    }

    /**
     * @param data array data
     * @param reply GXReplyData
     *
     * Read DLMS Data from the device. If access is denied return null.
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void readDLMSPacket(byte[] data, GXReplyData reply)
            throws Exception {
        if (!reply.getStreaming() && (data == null || data.length == 0)) {
            return;
        }
        GXReplyData notify = new GXReplyData();
        reply.setError((short) 0);
        Object eop = (byte) 0x7E;
        // In network connection terminator is not used.
        if (dlms.getInterfaceType() == InterfaceType.WRAPPER
                && media instanceof GXNet) {
            eop = null;
        }
        Integer pos = 0;
        boolean succeeded = false;
        ReceiveParameters<byte[]> p
                = new ReceiveParameters<>(byte[].class);
        p.setEop(eop);
        if (dlms.getInterfaceType() == InterfaceType.WRAPPER) {
            p.setCount(8);
        } else {
            p.setCount(5);
        }
        p.setWaitTime(15000);
        GXByteBuffer rd;
        synchronized (media.getSynchronous()) {
            while (!succeeded) {
                if (!reply.isStreaming()) {
//                    System.out.println(Level.INFO.toString() + ": " + now() + ";[SENT]: "
//                            + GXCommon.bytesToHex(data));
                    media.send(data, null);
                }
                if (p.getEop() == null) {
                    p.setCount(1);
                }
                succeeded = media.receive(p);
                if (!succeeded) {
                    // Try to read again...
                    if (pos++ == 3) {
                        logger.log(Level.SEVERE, "Failed to receive reply from the device in given time.");
                        throw new RuntimeException();
                    } else {
                        logger.log(Level.WARNING, "Data send failed. Try to resend {0}/3", pos.toString());
                    }
                }
            }
            rd = new GXByteBuffer(p.getReply());
            int msgPos = 0;
            // Loop until whole DLMS packet is received.
            try {
                while (!dlms.getData(rd, reply, notify)) {
                    p.setReply(null);
                    if (notify.getData().getData() != null) {
                        // Handle notify.
                        if (!notify.isMoreData()) {
                            // Show received push message as XML.
                            GXDLMSTranslator t = new GXDLMSTranslator(
                                    TranslatorOutputType.SIMPLE_XML);
                            String xml = t.dataToXml(notify.getData());
                            System.out.println("xml = " + xml);
                            notify.clear();
                            msgPos = rd.position();
                        }
                        continue;
                    }

                    if (p.getEop() == null) {
                        p.setCount(dlms.getFrameSize(rd));
                    }
                    while (!media.receive(p)) {
                        // If echo.
                        if (reply.isEcho()) {
                            media.send(data, null);
                        }
                        // Try to read again...
                        if (++pos == 3) {
                            logger.log(Level.SEVERE, "Data send failed. Try to resend {0}/3", pos.toString());
                            throw new Exception(
                                    "Failed to receive reply from the device in given time.");
                        } else {
                            logger.log(Level.WARNING, "Data send failed. Try to resend {0}/3", pos.toString());
                        }
                    }
                    rd.position(msgPos);
                    rd.set(p.getReply());
                }
            } catch (Exception e) {
//                System.out.println(Level.INFO.toString() + ": " + now() + ";[RECV]: "
//                        + rd.toString());
                throw e;
            }
        }
//        System.out.println(Level.INFO.toString() + ": " + now() + ";[RECV]: "
//                + rd.toString());
        if (reply.getError() != 0) {
            if (reply.getError() == ErrorCode.REJECTED.getValue()) {
                Thread.sleep(1000);
                readDLMSPacket(data, reply);
            } else {
                throw new GXDLMSException(reply.getError());
            }
        }
    }

    /**
     * @param data array byte 2 dimensi.
     * @param reply GXReplyData.
     *
     * Read DLMS Data from the device. If access is denied return null.
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void readDataBlock(byte[][] data, GXReplyData reply) throws Exception {
        if (data != null) {
            for (byte[] it : data) {
                reply.clear();
                readDataBlock(it, reply);
            }
        }
    }

    /**
     * Reads next data block.
     *
     * @param data Array byte.
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void readDataBlock(byte[] data, GXReplyData reply) throws Exception {
        if (data != null && data.length != 0) {
            readDLMSPacket(data, reply);
            while (reply.isMoreData()) {
                if (reply.isStreaming()) {
                    data = null;
                } else {
                    data = dlms.receiverReady(reply);
                }
                readDLMSPacket(data, reply);
            }
        }
    }

    /**
     * Reads selected DLMS object with selected attribute index.
     *
     * @param item COSEM Object.
     * @param attributeIndex Attribute Index yang akan dibaca.
     * @return Hasil baca dalam bentuk object.
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    Object read(GXDLMSObject item, int attributeIndex) throws Exception {
        byte[] data = dlms.read(item.getName(), item.getObjectType(),
                attributeIndex)[0];
        GXReplyData reply = new GXReplyData();
        readDataBlock(data, reply);
        // Update data type on read.
        if (item.getDataType(attributeIndex) == DataType.NONE) {
            item.setDataType(attributeIndex, reply.getValueType());
        }
        return dlms.updateValue(item, attributeIndex, reply.getValue());
    }

    /**
     * Read list of attributes.
     *
     * @param list List attribute.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void readList(List<Map.Entry<GXDLMSObject, Integer>> list)
            throws Exception {
        if (!list.isEmpty()) {
            byte[][] data = dlms.readList(list);
            GXReplyData reply = new GXReplyData();
            List<Object> values = new ArrayList<>(list.size());
            for (byte[] it : data) {
                readDataBlock(it, reply);
                // Value is null if data is send in multiple frames.
                if (reply.getValue() != null) {
                    values.addAll((List<?>) reply.getValue());
                }
                reply.clear();
            }
            if (values.size() != list.size()) {
                throw new Exception(
                        "Invalid reply. Read items count do not match.");
            }
            dlms.updateValues(list, values);
        }
    }

    /**
     * Writes a value to DLMS object with the selected attribute index.
     *
     * @param item COSEM Object.
     * @param attributeIndex Attribute index yang akan ditulis.
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void writeObject(GXDLMSObject item, int attributeIndex)
            throws Exception {
        byte[][] data = dlms.write(item, attributeIndex);
        readDLMSPacket(data);
    }

    /**
     * @param pg Object GXDLMSProfileGeneric yang akan dibaca kolomnya.
     * @return Daftar Kolom.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    List<Map.Entry<GXDLMSObject, GXDLMSCaptureObject>>
            GetColumns(GXDLMSProfileGeneric pg) throws Exception {
        Object entries = read(pg, 7);
        logger.log(Level.INFO, "Reading Profile Generic: {0} {1} entries:{2}",
                new Object[]{pg.getLogicalName(), pg.getDescription(), entries.toString()});
        GXReplyData reply = new GXReplyData();
        byte[] data = dlms.read(pg.getName(), pg.getObjectType(), 3)[0];
        readDataBlock(data, reply);
        dlms.updateValue((GXDLMSObject) pg, 3, reply.getValue());
        return pg.getCaptureObjects();
    }

    /**
     * Read Profile Generic's data by entry start and count.
     *
     * @param pg Object GXDLMSProfileGeneric yang akan dibaca.
     * @param index Index awal.
     * @param count Jumlah index yang ingin dibaca.
     * @return Hasil baca dalam bentuk array object.
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    Object[] readRowsByEntry(GXDLMSProfileGeneric pg, int index,
            int count) throws Exception {
        byte[][] data = dlms.readRowsByEntry(pg, index, count);
        GXReplyData reply = new GXReplyData();
        readDataBlock(data, reply);
        return (Object[]) dlms.updateValue(pg, 2, reply.getValue());
    }

    /**
     * Read Profile Generic's data by range (start and end time).
     *
     * @param pg Object GXDLMSProfileGeneric yang akan dibaca.
     * @param start Tanggal awal baca.
     * @param end Tanggal akhir baca
     * @return Hasil baca dalam bentuk array object
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    Object[] readRowsByRange(final GXDLMSProfileGeneric pg,
            final Date start, final Date end) throws Exception {
        GXReplyData reply = new GXReplyData();
        byte[][] data = dlms.readRowsByRange(pg, start, end);
        readDataBlock(data, reply);
        return (Object[]) dlms.updateValue(pg, 2, reply.getValue());
    }

    /**
     * Read Scalers and units from the register object.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void readScalerAndUnits() throws Exception {
        GXDLMSObjectCollection objs = dlms.getObjects()
                .getObjects(new ObjectType[]{ObjectType.REGISTER,
            ObjectType.DEMAND_REGISTER,
            ObjectType.EXTENDED_REGISTER});
        try {
            if (dlms.getNegotiatedConformance()
                    .contains(Conformance.MULTIPLE_REFERENCES)) {
                List<Map.Entry<GXDLMSObject, Integer>> list = new ArrayList<>();
                for (GXDLMSObject it : objs) {
                    if (it instanceof GXDLMSRegister) {
                        list.add(new GXSimpleEntry<>(it, 3));
                    }
                    if (it instanceof GXDLMSDemandRegister) {
                        list.add(new GXSimpleEntry<>(it, 4));
                    }
                }
                readList(list);
            }
        } catch (Exception e) {
            // Some meters are set multiple references, but don't support it.
            dlms.getNegotiatedConformance()
                    .remove(Conformance.MULTIPLE_REFERENCES);
        }
        if (!dlms.getNegotiatedConformance()
                .contains(Conformance.MULTIPLE_REFERENCES)) {
            objs.forEach((it) -> {
                try {
                    if (it instanceof GXDLMSRegister) {
                        read(it, 3);
                    } else if (it instanceof GXDLMSDemandRegister) {
                        read(it, 4);
                    }
                } catch (Exception e) {
                    logger.log(Level.WARNING, "Actaric SL7000 can return error here. Continue reading.");
                    logger.log(Level.SEVERE, e.toString());
                }
            });
        }
    }

    /**
     * Read profile generic columns from the meter.
     */
    void getProfileGenericColumns() {
        GXDLMSObjectCollection profileGenerics
                = dlms.getObjects().getObjects(ObjectType.PROFILE_GENERIC);
        for (GXDLMSObject it : profileGenerics) {
            logger.log(Level.INFO, "Profile Generic {0}Columns:", it.getName());
            GXDLMSProfileGeneric pg = (GXDLMSProfileGeneric) it;
            // Read columns.
            try {
                read(pg, 3);
                boolean first = true;
                StringBuilder sb = new StringBuilder();
                for (Map.Entry<GXDLMSObject, GXDLMSCaptureObject> col : pg
                        .getCaptureObjects()) {
                    if (!first) {
                        sb.append(" | ");
                    }
                    sb.append(col.getKey().getName());
                    sb.append(" ");
                    String desc = col.getKey().getDescription();
                    if (desc != null) {
                        sb.append(desc);
                    }
                    first = false;
                }
                System.out.println(sb.toString());
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Err! Failed to read columns:{0}", ex.getMessage());
                logger.log(Level.INFO, "Continue reading.");
            }
        }
    }

    /**
     * Read all data from the meter except profile generic (Historical) data.
     */
    void getReadOut() {
        for (GXDLMSObject it : dlms.getObjects()) {
            if (!(it instanceof IGXDLMSBase)) {
                // If interface is not implemented.
                logger.log(Level.WARNING, "Unknown Interface: {0}", it.getObjectType().toString());
                continue;
            }

            if (it instanceof GXDLMSProfileGeneric) {
                // Profile generic are read later
                // because it might take so long time
                // and this is only a example.
                continue;
            }
            logger.log(Level.INFO, "-------- Reading {0} {1} {2}", new Object[]{it.getClass().getSimpleName(), it.getName().toString(), it.getDescription()});
            for (int pos : ((IGXDLMSBase) it).getAttributeIndexToRead(true)) {
                try {
                    Object val = read(it, pos);
                    showValue(pos, val);
                } catch (Exception ex) {
                    logger.log(Level.SEVERE, "Error! Index: {0} {1}",
                            new Object[]{pos, ex.getMessage()});
                    logger.log(Level.SEVERE, ex.toString());
                    logger.log(Level.INFO, "Continue reading.");
                }
            }
        }
    }

    /**
     * Showing Value
     *
     * @param pos Attribute index.
     * @param value Object hasil baca
     */
    static void showValue(final int pos, final Object value) {
        Object val = value;
        if (val instanceof byte[]) {
            val = GXCommon.bytesToHex((byte[]) val);
        } else if (val instanceof Double) {
            NumberFormat formatter = NumberFormat.getNumberInstance();
            val = formatter.format(val);
        } else if (val instanceof List) {
            StringBuilder sb = new StringBuilder();
            for (Object tmp : (List<?>) val) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                if (tmp instanceof byte[]) {
                    sb.append(GXCommon.bytesToHex((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        } else if (val != null && val.getClass().isArray()) {
            StringBuilder sb = new StringBuilder();
            for (int pos2 = 0; pos2 != Array.getLength(val); ++pos2) {
                if (sb.length() != 0) {
                    sb.append(", ");
                }
                Object tmp = Array.get(val, pos2);
                if (tmp instanceof byte[]) {
                    sb.append(GXCommon.bytesToHex((byte[]) tmp));
                } else {
                    sb.append(String.valueOf(tmp));
                }
            }
            val = sb.toString();
        }
        System.out.println(String.valueOf(val) + "\t");
    }

    /**
     * Read profile generic (Historical) data.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void getProfileGenerics() throws Exception {
        Object[] cells;
        GXDLMSObjectCollection profileGenerics
                = dlms.getObjects().getObjects(ObjectType.PROFILE_GENERIC);
        for (GXDLMSObject it : profileGenerics) {
            logger.log(Level.INFO, "-------- Reading {0} {1} {2}", new Object[]{it.getClass().getSimpleName(),
                it.getName().toString(), it.getDescription()});
            long entriesInUse = ((Number) read(it, 7)).longValue();
            long entries = ((Number) read(it, 8)).longValue();
            logger.log(Level.INFO, "Entries: {0}/{1}", new Object[]{String.valueOf(entriesInUse),
                String.valueOf(entries)});
            GXDLMSProfileGeneric pg = (GXDLMSProfileGeneric) it;
            // If there are no columns.
            if (entriesInUse == 0 || pg.getCaptureObjects().isEmpty()) {
                if (entriesInUse == 0) {
                    logger.log(Level.WARNING, "Entries in use is 0");
                }
                if (pg.getCaptureObjects().isEmpty()) {
                    logger.log(Level.WARNING, "Capture objects is empty");
                }
                logger.log(Level.INFO, "Continue reading");
                continue;
            }
            ///////////////////////////////////////////////////////////////////
            // Read first item.
            try {
                logger.log(Level.INFO, "Read first item");
                cells = readRowsByEntry(pg, 1, 1);
                for (Object rows : cells) {
                    for (Object cell : (Object[]) rows) {
                        if (cell instanceof byte[]) {
                            System.out.println(GXCommon.bytesToHex((byte[]) cell) + " | ");
                        } else {
                            System.out.println(cell + " | ");
                        }
                    }
                    System.out.println("");
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Error! Failed to read first row: {0}", ex.getMessage());
                logger.log(Level.INFO, "Continue reading if device returns access denied error");
                // Continue reading if device returns access denied error.
            }
            ///////////////////////////////////////////////////////////////////
            // Read last day.
            try {
                logger.log(Level.INFO, "Read last day");
                java.util.Calendar start = java.util.Calendar
                        .getInstance(java.util.TimeZone.getTimeZone("UTC"));
                start.set(java.util.Calendar.HOUR_OF_DAY, 0); // set hour to
                // midnight
                start.set(java.util.Calendar.MINUTE, 0); // set minute in
                // hour
                start.set(java.util.Calendar.SECOND, 0); // set second in
                // minute
                start.set(java.util.Calendar.MILLISECOND, 0);
                start.add(java.util.Calendar.DATE, -1);

                java.util.Calendar end = java.util.Calendar.getInstance();
                end.set(java.util.Calendar.MINUTE, 0); // set minute in hour
                end.set(java.util.Calendar.SECOND, 0); // set second in
                // minute
                end.set(java.util.Calendar.MILLISECOND, 0);
                cells = readRowsByRange((GXDLMSProfileGeneric) it,
                        start.getTime(), end.getTime());
                for (Object rows : cells) {
                    for (Object cell : (Object[]) rows) {
                        if (cell instanceof byte[]) {
                            System.out.print(
                                    GXCommon.bytesToHex((byte[]) cell) + " | ");
                        } else {
                            System.out.println(cell + " | ");
                        }
                    }
                    System.out.println("");
                }
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Error! Failed to read last day: {0}", ex.getMessage());
                logger.log(Level.INFO, "Continue reading if device returns access denied error");
            }
        }
    }

    /**
     * Read association view.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    public void readAssociationView() throws Exception {
        logger.log(Level.INFO, "Read Association View");
        System.out.println("=============================Get Obis List=================================");
        GXReplyData reply = new GXReplyData();
        // Get Association view from the meter.
        readDataBlock(dlms.getObjectsRequest(), reply);
        GXDLMSObjectCollection objects
                = dlms.parseObjects(reply.getData(), true);
        // Get description of the objects.
        GXDLMSConverter converter = new GXDLMSConverter();
        converter.updateOBISCodeInformation(objects);
        if (objects.size() > 0) {
            for (GXDLMSObject object : objects) {
                if (GlobalVar.isObisAvailable(object.getLogicalName())) {
                    if (GlobalVar.getObisByLn(object.getLogicalName()).getObject().getObjectType().
                            equals(object.getObjectType())) {
                        break;
                    } else {
                        System.out.println("breaking " + object.getLogicalName() + " object");
                    }
                } else {
                    GlobalVar.getObisList().put(object.getLogicalName(), new Obis(object));
                }
            }
            GlobalVar.cetakDaftarObis(true);
        }
        System.out.println("===========================================================================");
    }

    /**
     * Read all objects from the meter. This is only example. Usually there is
     * no need to read all data from the meter.
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    void readAll() throws Exception {
        try {
            System.out.println("=============================Association View==========================");
            readAssociationView();
            // Read all attributes from all objects.
            getReadOut();
            // Read historical data.
            getProfileGenerics();
        } finally {
            close();
        }
    }

    /*
================================================================================================
                                          Start Own Code
================================================================================================
     */
    /**
     * Menyambung komunikasi dengan meter.
     */
    public void connect() {
        logger.log(Level.INFO, "Start Login Process");
        try {
            GXReplyData reply = new GXReplyData();
            byte[] data = dlms.snrmRequest();
            if (data.length != 0) {
                logger.log(Level.INFO, "Sending SNRM");
                readDLMSPacket(data, reply);
                dlms.parseUAResponse(reply.getData());
                int size = (int) ((((Number) dlms.getLimits().getMaxInfoTX())
                        .intValue() & 0xFFFFFFFFL) + 40);
                replyBuff = java.nio.ByteBuffer.allocate(size);
            }
            reply.clear();
            logger.log(Level.INFO, "Execute Application Association");
            readDataBlock(dlms.aarqRequest(), reply);
            dlms.parseAareResponse(reply.getData());
            reply.clear();
            if (dlms.getAuthentication().getValue() > Authentication.LOW
                    .getValue()) {
                for (byte[] it : dlms.getApplicationAssociationRequest()) {
                    readDLMSPacket(it, reply);
                }
                dlms.parseApplicationAssociationResponse(reply.getData());
            }
            GXDLMSData dataNomorMeter = new GXDLMSData("0.0.96.1.0.255");
            dataNomorMeter.setVersion(0);
            read(dataNomorMeter, 2);
            logger.log(Level.INFO, "Meter with {0} serial number connected @{1} at {2}", new Object[]{
                PrintUtil.lihatNilai(2, dataNomorMeter.getValue()), media.getName(), new Date().toString()});
            System.out.println("===========================================================================");
            connected = true;
        } catch (Exception e) {
            try {
                logger.log(Level.SEVERE, "Cannot connect to Meter @{0} cause {1}", new Object[]{media.getName(),
                    e.toString()});
                close();
                System.out.println("===========================================================================");
                connected = false;
            } catch (Exception ex) {
                Logger.getLogger(Communicator.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("===========================================================================");
                connected = false;
            }
        }
    }

    /**
     * Memutus sambungan komunikasi dengan meter.
     */
    public void disconnect() {
        try {
            System.out.println(Level.INFO + ": Disconnecting");
//            logger.log(Level.INFO, "Disconnecting");
            close();
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
        } finally {
            System.out.println("Read end at " + new Date().toString());
//            logger.log(Level.INFO, "Read end at {0}", new Date().toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Get connection status.
     *
     * @return Boolean
     */
    public boolean isConnected() {
        return connected;
    }

    /**
     * Read association view, scaller, unit dan profile generic column
     *
     * @throws Exception Penanganan kesalahan jika terjadi error.
     */
    public void readAssociationViewScallerPG() throws Exception {
        readAssociationView();
        System.out.println("=============================Get Scaler and Units==========================");
//             Read Scalers and units from the register objects.
        readScalerAndUnits();
//             Read Profile Generic columns.
        System.out.println("=============================Get Profile Generic Column====================");
        getProfileGenericColumns();
    }

    /**
     * Function untuk memaintenance koneksi ke meter agar tetap hidup walaupun
     * tidak melakukan pembacaan.
     */
    public void keepAlive() {
        GXReplyData reply = new GXReplyData();
        try {
            readDLMSPacket(dlms.keepAlive(), reply);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }

    /**
     * Membaca object Data.
     *
     * @param object GXDLMSData yang ingin dibaca.
     */
    public void readData(GXDLMSData object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
        try {
            read(object, 2);
            PrintUtil.printdata(object);
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }
    
    /**
     * Membaca profile generic Object.
     *
     * @param object Object GXDLMSProfileGeneric yang akan dibaca.
     */
    public void readProfileGeneric(GXDLMSProfileGeneric object) {
        logger.log(Level.INFO, PrintUtil.READING_LOG, new Object[]{
            object.getObjectType(), object.getLogicalName(), object.getDescription()});
            addToCollection(object);
        logger.log(Level.INFO, "Read capture object ({0} index 3)", object.getLogicalName());
        try {
            read(object, 3);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Err! Failed to read columns:{0}", ex.toString());
            System.out.println("===========================================================================");
            return;
        }
        try {
            GXDLMSProfileGeneric pg = new GXDLMSProfileGeneric();
            pg.setLogicalName(object.getLogicalName());
            pg.setObjectType(ObjectType.PROFILE_GENERIC);
            if (Meter.isLoadProfileEDMIMk7MI(object.getLogicalName())) {
                GXDLMSObject dt = new GXDLMSObject();
                dt.setLogicalName("0.0.1.0.0.255");
                dt.setDescription("Capture Time");
                dt.setObjectType(ObjectType.CLOCK);
                dt.setUIDataType(2, DataType.DATETIME);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
                dt = new GXDLMSObject();
                dt.setLogicalName("1.0.0.8.4.255");
                dt.setDescription("Data");
                dt.setObjectType(ObjectType.DATA);
                dlms.getObjects().add(dt);
                pg.addCaptureObject(dt, 2, 0);
            }
            object.getCaptureObjects().forEach((itco) -> {
                pg.addCaptureObject(itco.getKey(), itco.getValue().getAttributeIndex(),
                        itco.getValue().getDataIndex());
            });
            logger.log(Level.INFO, "Jumlah Channel {0} : {1}", new Object[]{object.getDescription(),
                pg.getCaptureObjects().size()});
            long entriesInUse = ((Number) read(pg, 7)).longValue();
            long entries = ((Number) read(pg, 8)).longValue();
            logger.log(Level.INFO, "Entries: {0}/{1}", new Object[]{String.valueOf(entriesInUse),
                String.valueOf(entries)});
            if (entriesInUse == 0 || pg.getCaptureObjects().isEmpty()) {
                if (entriesInUse == 0) {
                    logger.log(Level.WARNING, "Entries in use is 0");
                }
                if (pg.getCaptureObjects().isEmpty()) {
                    logger.log(Level.WARNING, "Capture objects is empty");
                }
                System.out.println("===========================================================================");
                return;
            }
            StretchTest.jumlahRecord += entriesInUse;
            read(pg, 2);
            Object[] cells = pg.getBuffer();
            for (Object rows : cells) {
                System.out.println("---------------------------------------------------------------------------");
                int a = 0;
                for (Object cell : (Object[]) rows) {
                    System.out.print(pg.getCaptureObjects().get(a).getKey().getDescription() + "("
                            + pg.getCaptureObjects().get(a).getKey().getLogicalName() + ") = ");
                    String hasil;
                    if (cell instanceof byte[]) {
                        hasil = new String((byte[]) cell);
                    } else {
                        hasil = cell.toString();
                    }
                    if (pg.getCaptureObjects().get(a).getKey().getObjectType().equals(ObjectType.REGISTER)) {
                        if (PhysicalUnit.getPhysicalUnit(
                                ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                        pg.getCaptureObjects().get(a).getKey().
                                                getLogicalName())).getUnit()).getSatuan().equals(
                                                        PhysicalUnit.getPhysicalUnit(Unit.SECOND).getSatuan())) {
                            System.out.println(PhysicalUnit.secondToDuration(Integer.parseInt(hasil)));
                            
                        } else {
                            System.out.println(hasil + " " + PhysicalUnit.getPhysicalUnit(
                                ((GXDLMSRegister) dlms.getObjects().findByLN(ObjectType.REGISTER,
                                        pg.getCaptureObjects().get(a).getKey().
                                                getLogicalName())).getUnit()).getSatuan());
                        }
                    } else {
                        System.out.println(hasil);
                    }
                    a++;
                }
            }
            System.out.println("===========================================================================");
        } catch (GXDLMSException ex) {
            PrintUtil.printGXDLMSException(ex);
            System.out.println("===========================================================================");
        } catch (Exception ex) {
            logger.log(Level.SEVERE, PrintUtil.FAIL_LOG, ex.toString());
            System.out.println("===========================================================================");
        }
    }
    
    /**
     * Menambahkan capture object profile generic ke dalam Object Collection
     * GXDLMSClient sekaligus scaller dan unitnya. Perhatian : Jangan digunakan
     * bila sudah melakukan pembacaan Association View kecuali meter EDMI MK7MI.
     *
     * @param generic Object profile generic yang sedang dibaca.
     */
    void addToCollection(GXDLMSProfileGeneric generic) {
        logger.log(Level.INFO, "add unit measurement and record unit and scale to collection");
        List<Double> doubles = new ArrayList<>();
        if (Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName())) {
            try {
                doubles = readEDMIMk7MILPScaleList();
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Gagal mengambil skala EDMI Mk7MI karena : {0}", ex.toString());
            }
        }
        try {
            read(generic, 3);
            int count = 0;
            for (Map.Entry<GXDLMSObject, GXDLMSCaptureObject> col : generic
                    .getCaptureObjects()) {
                switch (col.getKey().getObjectType()) {
                    case DEMAND_REGISTER: {
                        GXDLMSDemandRegister demandRegister = new GXDLMSDemandRegister(
                                col.getKey().getLogicalName());
                        demandRegister.setVersion(col.getKey().getVersion());
                        demandRegister.setDescription(ObisDescriptor.getDeskripsi(
                                demandRegister.getLogicalName()));
                        Object val = read(demandRegister, 4);
                        SkaladanUnit su = new SkaladanUnit(PrintUtil.lihatNilai(4, val));
                        demandRegister.setScaler(readLPScale(
                                Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName()), count, doubles,
                                su.getSkala()));
                        demandRegister.setUnit(su.getUnit());
                        dlms.getObjects().add(demandRegister);
                        System.out.println("add " + demandRegister.getLogicalName()
                                + "(" + demandRegister.getDescription() + ")"
                                + " version " + demandRegister.getVersion()
                                + " scaller " + demandRegister.getScaler()
                                + " unit " + demandRegister.getUnit()
                                + " as " + demandRegister.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case REGISTER: {
                        GXDLMSRegister register = new GXDLMSRegister(col.getKey().getLogicalName());
                        register.setVersion(col.getKey().getVersion());
                        register.setDescription(ObisDescriptor.getDeskripsi(register.getLogicalName()));
                        Object val = read(register, 3);
                        SkaladanUnit su = new SkaladanUnit(PrintUtil.lihatNilai(3, val));
                        register.setScaler(readLPScale(Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName()),
                                count, doubles, su.getSkala()));
                        register.setUnit(su.getUnit());
                        dlms.getObjects().add(register);
                        System.out.println("add " + register.getLogicalName()
                                + "(" + register.getDescription() + ")"
                                + " version " + register.getVersion()
                                + " scaller " + register.getScaler()
                                + " unit " + register.getUnit()
                                + " as " + register.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case EXTENDED_REGISTER: {
                        GXDLMSExtendedRegister extendedRegister
                                = new GXDLMSExtendedRegister(col.getKey().getLogicalName());
                        extendedRegister.setVersion(col.getKey().getVersion());
                        extendedRegister.setDescription(ObisDescriptor.getDeskripsi(
                                extendedRegister.getLogicalName()));
                        Object val = read(extendedRegister, 3);
                        SkaladanUnit su = new SkaladanUnit(PrintUtil.lihatNilai(3, val));
                        extendedRegister.setScaler(readLPScale(
                                Meter.isLoadProfileEDMIMk7MI(generic.getLogicalName()), count, doubles,
                                su.getSkala()));
                        extendedRegister.setUnit(su.getUnit());
                        dlms.getObjects().add(extendedRegister);
                        System.out.println("add " + extendedRegister.getLogicalName()
                                + "(" + extendedRegister.getDescription() + ")"
                                + " version " + extendedRegister.getVersion()
                                + " scaller " + extendedRegister.getScaler()
                                + " unit " + extendedRegister.getUnit()
                                + " as " + extendedRegister.getObjectType()
                                + " to collection ");
                        break;
                    }
                    case CLOCK:
                        GXDLMSClock clock = new GXDLMSClock(col.getKey().getLogicalName());
                        clock.setDescription(ObisDescriptor.getDeskripsi(clock.getLogicalName()));
                        clock.setVersion(col.getKey().getVersion());
                        for (int i = 2; i < 10; i++) {
                            read(clock, i);
                        }
                        dlms.getObjects().add(clock);
                        System.out.println("add " + clock.getLogicalName()
                                + "(" + clock.getDescription() + ")"
                                + " version " + clock.getVersion()
                                + " as " + clock.getObjectType()
                                + " to collection ");
                        break;
                    case DATA: {
                        GXDLMSData dt = new GXDLMSData(col.getKey().getLogicalName());
                        dt.setDescription(ObisDescriptor.getDeskripsi(dt.getLogicalName()));
                        dlms.getObjects().add(dt);
                        System.out.println("add " + dt.getLogicalName()
                                + "(" + dt.getDescription() + ")"
                                + " version " + dt.getVersion()
                                + " as " + dt.getObjectType()
                                + " to collection ");
                        break;
                    }
                    default: {
                        GXDLMSObject dt = new GXDLMSObject();
                        dt.setLogicalName(col.getKey().getLogicalName());
                        dt.setObjectType(col.getKey().getObjectType());
                        dt.setDescription(ObisDescriptor.getDeskripsi(dt.getLogicalName()));
                        dlms.getObjects().add(dt);
                        System.out.println("add " + dt.getLogicalName()
                                + "(" + dt.getDescription() + ")"
                                + " version " + dt.getVersion()
                                + " as " + dt.getObjectType()
                                + " to collection ");
                        break;
                    }
                }
                count++;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error adding to collection {0}", e.toString());
        }
    }
    
    /**
     * Mendapatkan daftar skala untuk masing-masing capture object EDMI Mk7MI.
     *
     * @return List skala dalam variabel bertipe Double.
     */
    List<Double> readEDMIMk7MILPScaleList() throws Exception {
        logger.log(Level.WARNING, "Read EDMI Mk7MI Load Profile Scaller (0.0.136.0.1.255 index 2)");
        List<Double> doubles = new ArrayList<>();
        GXDLMSData data = new GXDLMSData("0.0.136.0.1.255");
        data.setDescription("EDMI Scale");
        data.setVersion(0);
        data.setObjectType(ObjectType.DATA);
        read(data, 2);
        for (Object tmp : (List<?>) data.getValue()) {
            if (tmp instanceof List) {
                String tmp2;
                if (tmp instanceof byte[]) {
                    tmp2 = new String((byte[]) tmp);
                } else {
                    tmp2 = String.valueOf(tmp);
                }
                String[] strings = tmp2.replace("[", "").replace("]", "").split(",");
                doubles.add(Math.pow(10, Integer.parseInt(strings[1].trim())));
            }
        }
        return doubles;
    }
    
    /**
     * Memberikan skala untuk capture object. skalaObject jika bukan EDMI Mk7MI.
     *
     * @param isLoadProfileEDMIMk7MI True jika membaca Load Profile EDMI Mk7MI
     * karena Load Profile EDMI Mk7MI capture Object dan Buffernya tidak
     * Kongruen.
     * @param count Urutan capture object.
     * @param doubles List skala EDMI Mk7MI. bisa didapat dengan menggunakan
     * method getEDMIMk7MILPScaleList().
     * @param skalaObject Skala object aslinya.
     *
     * @return Skala dalam variabel bertipe Double
     */
    double readLPScale(boolean isLoadProfileEDMIMk7MI, int count, List<Double> doubles, double skalaObject) {
        if (isLoadProfileEDMIMk7MI && (count - 2 >= 0)) {
            return doubles.get(count);
        } else {
            return skalaObject;
        }
    }
}
