/*
 * 
 */

package gurux.dlms.ui;

import gurux.dlms.enums.ObjectType;
import gurux.dlms.object.meter.MeterObject;
import gurux.dlms.objects.GXDLMSData;
import gurux.dlms.objects.GXDLMSProfileGeneric;
import gurux.net.GXNet;

/**
 * Class Definition
 *
 * @author Tab Solutions
 */
public class StretchSequensial {
    
    public static void read(MeterObject meterObject, String modeBaca){
        System.out.println("Running " + meterObject.getHostname() + ":" + meterObject.getPort() + ".");
        try {
            GXNet net;
            if (meterObject.getMedia() == null) {
                meterObject.setMedia(new GXNet());
            }
            net = (GXNet) meterObject.getMedia();
            net.setHostName(meterObject.getHostname());
            net.setPort(meterObject.getPort());
            StretchCom com = new StretchCom();
            com.setCommunicator(meterObject.getMedia(), meterObject.getClient());
            meterObject.getMedia().open();
            System.out.println(meterObject.toString());
            com.connect();
            if (com.isConnected()) {
                if (modeBaca == "nomor meter") {
                    GXDLMSData object = new GXDLMSData("0.0.96.1.0.255");
                    com.readData(object);
                    StretchTest.jumlahRecord++;
                } else {
                    GXDLMSProfileGeneric profileGeneric = new GXDLMSProfileGeneric();
                    profileGeneric.setVersion(1);
                    profileGeneric.setDescription("Load Profile");
                    profileGeneric.setObjectType(ObjectType.PROFILE_GENERIC);
                    switch (meterObject.getMeterType()) {
                        case EDMI_Mk10M:
                            profileGeneric.setLogicalName("0.0.99.1.0.255");
                            break;
                        default:
                            profileGeneric.setLogicalName("1.0.99.1.0.255");
                            break;
                    }
                    com.readProfileGeneric(profileGeneric);
                }
                com.close();
            }
        } catch (InterruptedException e) {
            System.out.println("Meter @" + meterObject.getHostname() + ":" + meterObject.getPort() + " interrupted.");
        } catch (Exception e) {
            System.out.println("Catching " + e.toString());
        }
        System.out.println("Meter " + meterObject.getHostname() + ":" + meterObject.getPort() + " exiting.");
    }
    
}
